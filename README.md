# Yateli

Yateli est une initiative étudiante visant à proposer des produits atypiques du monde entier. Notre mot d’ordre? **Encourager l’artisanat international** et donner la possibilité aux consommateurs de retrouver confiance dans les produits, de leur fabrication jusqu’à leur utilisation au quotidien. 

Grâce à nos multiples partenariats autour du monde, des centaines d’articles sont à votre disposition et attendent seulement à être adoptés. Nos artisans tiennent à ce que leurs créations soient **respectueuses de l’environnement** et contribuent au développement durable.

Chez Yateli, nous sommes conscients que le monde est une chance et un bien précieux à entretenir. Nous voulons ainsi rompre avec la commercialisation de masse et le désintérêt porté à la production **éco-responsable**.

Par l’innovation et l’intelligence de nos artisans, redonnons le sourire à notre planète et entrons en possession de biens et d’accessoires nouveaux. 

**Forgeons ensemble l’économie participative et environnementale de demain**.

 Yateli, Prenez part à l’écoculture !
