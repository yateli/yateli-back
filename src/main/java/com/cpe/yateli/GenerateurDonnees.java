package com.cpe.yateli;

import com.cpe.yateli.model.*;
import com.cpe.yateli.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;
import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

@Configuration
@Profile("generateur-donnees")
public class GenerateurDonnees {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PanierRepository panierRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ArtisanRepository artisanRepository;

    @Autowired
    private ProduitRepository produitRepository;

    @Autowired
    private LocalisationRepository localisationRepository;

    @Autowired
    private ProduitQuantitePanierRepository produitQuantitePanierRepository;

    @Autowired
    private LivraisonRepository livraisonRepository;

    @Autowired
    private BonAchatRepository bonAchatRepository;

    @Autowired
    private InterrogationRepository interrogationRepository;

    @Autowired
    private ReponseRepository reponseRepository;

    @PostConstruct
    public void initEntities() {
        if(userRepository.findAll().size() < 1){
            this.initRoles();
            this.initTags();
            this.initArtisan();
            this.initProduits();
            this.initUsers();
            this.initPanier();
            this.remplirPanier();
            this.initLivraisons();
            this.initBonAchats();
            this.initInterrogations();
        }
    }

    private void initRoles(){
        Role role1 = Role.builder()
                .nom("USER")
                .users(new ArrayList<>())
                .build();

        Role role2 = Role.builder()
                .nom("ADMIN")
                .users(new ArrayList<>())
                .build();

        Role role3 = Role.builder()
                .nom("ARTISAN")
                .users(new ArrayList<>())
                .build();

        roleRepository.save(Arrays.asList(role1,role2,role3));
    }

    private void initTags(){
        Tag envie1 = Tag.builder()
                .nom("JAPON")
                .produits(new HashSet<>())
                .users(new HashSet<>())
                .build();

        Tag manga = Tag.builder()
                .nom("MANGA")
                .produits(new HashSet<>())
                .users(new HashSet<>())
                .build();

        Tag envie2 = Tag.builder()
                .nom("RUSSIE")
                .produits(new HashSet<>())
                .users(new HashSet<>())
                .build();

        Tag envie3 = Tag.builder()
                .nom("POLOGNE")
                .produits(new HashSet<>())
                .users(new HashSet<>())
                .build();

        Tag envie4 = Tag.builder()
                .nom("INDE")
                .produits(new HashSet<>())
                .users(new HashSet<>())
                .build();

        Tag envie5 = Tag.builder()
                .nom("BRESIL")
                .produits(new HashSet<>())
                .users(new HashSet<>())
                .build();

        Tag envie6 = Tag.builder()
                .nom("PEROU")
                .produits(new HashSet<>())
                .users(new HashSet<>())
                .build();

        tagRepository.save(Arrays.asList(envie1,envie2,envie3,envie4,envie5,envie6,manga));
    }

    private void initArtisan(){

        // JAPON
        Localisation localisationArtisan = Localisation.builder()
                .pays(Pays.Japon)
                .latitude(35.0116)
                .longitude(135.7680)
                .build();

        // RUSSIE
        Localisation localisationArtisan1 = Localisation.builder()
                .pays(Pays.Russie)
                .latitude(57.866847)
                .longitude(37.338097)
                .build();

        // INDE
        Localisation localisationArtisan2 = Localisation.builder()
                .pays(Pays.Inde)
                .latitude(11.920518)
                .longitude(79.822198)
                .build();

        // BRESIL
        Localisation localisationArtisan3 = Localisation.builder()
                .pays(Pays.Bresil)
                .latitude(-10.770788)
                .longitude(-48.297319)
                .build();

        // POLOGNE
        Localisation localisationArtisan4 = Localisation.builder()
                .pays(Pays.Pologne)
                .latitude(52.559647)
                .longitude(19.749959)
                .build();

        // PEROU
        Localisation localisationArtisan5 = Localisation.builder()
                .pays(Pays.Perou)
                .latitude(-10.677362)
                .longitude(-77.789784)
                .build();

        Role roleArtisan = roleRepository.findRoleByNom("ARTISAN");

        User user = User.builder()
                .prenom("Wang")
                .mail("wang@test.ch")
                .password(passwordEncoder.encode("test"))
                .dateCreation(new Timestamp(new Date().getTime()))
                .role(roleArtisan)
                .estAbonne(false)
                .estActive(true)
                .build();

        User user1 = User.builder()
                .prenom("Vladislav")
                .mail("vladislav@test.ru")
                .password(passwordEncoder.encode("test"))
                .dateCreation(new Timestamp(new Date().getTime()))
                .role(roleArtisan)
                .estAbonne(false)
                .estActive(true)
                .build();

        User user2 = User.builder()
                .prenom("priash")
                .mail("priash@test.in")
                .password(passwordEncoder.encode("test"))
                .dateCreation(new Timestamp(new Date().getTime()))
                .role(roleArtisan)
                .estAbonne(false)
                .estActive(true)
                .build();

        User user3 = User.builder()
                .prenom("antonio")
                .mail("antonio@test.br")
                .password(passwordEncoder.encode("test"))
                .dateCreation(new Timestamp(new Date().getTime()))
                .role(roleArtisan)
                .estAbonne(false)
                .estActive(true)
                .build();

        User user4 = User.builder()
                .prenom("alicia")
                .mail("alicia@test.pl")
                .password(passwordEncoder.encode("test"))
                .dateCreation(new Timestamp(new Date().getTime()))
                .role(roleArtisan)
                .estAbonne(false)
                .estActive(true)
                .build();

        User user5 = User.builder()
                .prenom("andrea")
                .mail("andrea@test.pe")
                .password(passwordEncoder.encode("test"))
                .dateCreation(new Timestamp(new Date().getTime()))
                .role(roleArtisan)
                .estAbonne(false)
                .estActive(true)
                .build();

        roleArtisan.getUsers().add(user);
        roleArtisan.getUsers().add(user1);
        roleArtisan.getUsers().add(user2);
        roleArtisan.getUsers().add(user3);
        roleArtisan.getUsers().add(user4);
        roleArtisan.getUsers().add(user5);

        Artisan artisan = Artisan.builder()
                .user(user)
                .nationalite(Pays.Chine)
                .photoArtisan(null)
                .telephone("0608144214")
                .emplacement(localisationArtisan)
                .estActif(true)
                .build();

        Artisan artisan1 = Artisan.builder()
                .user(user1)
                .nationalite(Pays.Russie)
                .photoArtisan(null)
                .telephone("0608144214")
                .emplacement(localisationArtisan1)
                .estActif(true)
                .build();

        Artisan artisan2 = Artisan.builder()
                .user(user2)
                .nationalite(Pays.Inde)
                .photoArtisan(null)
                .telephone("0608144214")
                .emplacement(localisationArtisan2)
                .estActif(true)
                .build();

        Artisan artisan3 = Artisan.builder()
                .user(user3)
                .nationalite(Pays.Bresil)
                .photoArtisan(null)
                .telephone("0608144214")
                .emplacement(localisationArtisan3)
                .estActif(true)
                .build();

        Artisan artisan4 = Artisan.builder()
                .user(user4)
                .nationalite(Pays.Pologne)
                .photoArtisan(null)
                .telephone("0608144214")
                .emplacement(localisationArtisan4)
                .estActif(true)
                .build();

        Artisan artisan5 = Artisan.builder()
                .user(user5)
                .nationalite(Pays.Perou)
                .photoArtisan(null)
                .telephone("0608144214")
                .emplacement(localisationArtisan5)
                .estActif(true)
                .build();

        userRepository.save(user);
        userRepository.save(user1);
        userRepository.save(user2);
        userRepository.save(user3);
        userRepository.save(user4);
        userRepository.save(user5);
        roleRepository.save(roleArtisan);
        artisanRepository.save(artisan);
        artisanRepository.save(artisan1);
        artisanRepository.save(artisan2);
        artisanRepository.save(artisan3);
        artisanRepository.save(artisan4);
        artisanRepository.save(artisan5);
    }

    private void initProduits(){
        Produit produit1 = Produit.builder()
                .nom("Shuriken Artisanal")
                .reference("SHU-ART-WANG")
                .description("Forgé à la main par un grand maître Japonais")
                .date_ajout(new Timestamp(new Date().getTime()))
                .image(null)
                .poids(2.5f)
                .pointsFidelite(8)
                .prixHT(43.6)
                .tauxTaxe(0.2f)
                .quantiteStock(34)
                .tags(new HashSet<>())
                .build();
        produit1.calculTaxe();

        Produit produit2 = Produit.builder()
                .nom("Manga Fait Main")
                .reference("MANGA-MAIN-WANG")
                .description("Ecrit avec la plume d'un grand homme du Japon")
                .date_ajout(new Timestamp(new Date().getTime()))
                .image(null)
                .poids(0.8f)
                .pointsFidelite(4)
                .prixHT(16.4)
                .tauxTaxe(0.2f)
                .quantiteStock(20)
                .tags(new HashSet<>())
                .build();
        produit2.calculTaxe();

        Produit produit3 = Produit.builder()
                .nom("Poupee russe")
                .reference("POUPEE-RUSSE-WANG")
                .description("description")
                .date_ajout(new Timestamp(new Date().getTime()))
                .image(null)
                .poids(0.8f)
                .pointsFidelite(4)
                .prixHT(16.4)
                .tauxTaxe(0.2f)
                .quantiteStock(20)
                .tags(new HashSet<>())
                .build();
        produit3.calculTaxe();

        Produit produit4 = Produit.builder()
                .nom("Vase Fait Main")
                .reference("VASE-MAIN-WANG")
                .description("description")
                .date_ajout(new Timestamp(new Date().getTime()))
                .image(null)
                .poids(0.8f)
                .pointsFidelite(4)
                .prixHT(16.4)
                .tauxTaxe(0.2f)
                .quantiteStock(20)
                .tags(new HashSet<>())
                .build();
        produit4.calculTaxe();

        Produit produit5 = Produit.builder()
                .nom("chapka siberienne")
                .reference("CHAPKA-MAIN-VLADISLAV")
                .description("Chapka en Fourrure réélle De Renard pour Hiver fait main en siberie.")
                .date_ajout(new Timestamp(new Date().getTime()))
                .image(null)
                .poids(0.8f)
                .pointsFidelite(4)
                .prixHT(45.5)
                .tauxTaxe(0.2f)
                .quantiteStock(20)
                .tags(new HashSet<>())
                .build();
        produit5.calculTaxe();

        Produit produit6 = Produit.builder()
                .nom("Pavlovo Possad (écharpe)")
                .reference("PALVOVO-MAIN-VLADISLAV")
                .description("Pavlovo Possad écharpe artisanal typique de russie.")
                .date_ajout(new Timestamp(new Date().getTime()))
                .image(null)
                .poids(0.8f)
                .pointsFidelite(4)
                .prixHT(24.5)
                .tauxTaxe(0.2f)
                .quantiteStock(20)
                .tags(new HashSet<>())
                .build();
        produit6.calculTaxe();

        Produit produit7 = Produit.builder()
                .nom("etole Batik")
                .reference("BATIK-MAIN-PRIASH")
                .description("Etole Batik traditionnel d'origine indienne.")
                .date_ajout(new Timestamp(new Date().getTime()))
                .image(null)
                .poids(0.8f)
                .pointsFidelite(4)
                .prixHT(17.5)
                .tauxTaxe(0.2f)
                .quantiteStock(20)
                .tags(new HashSet<>())
                .build();
        produit7.calculTaxe();

        Produit produit8 = Produit.builder()
                .nom("Bonnet Anita alpaga perou")
                .reference("BONNETANITA-MAIN-ANDREA")
                .description("Etole Batik traditionnel d'origine indienne.")
                .date_ajout(new Timestamp(new Date().getTime()))
                .image(null)
                .poids(0.8f)
                .pointsFidelite(4)
                .prixHT(24.5)
                .tauxTaxe(0.2f)
                .quantiteStock(20)
                .tags(new HashSet<>())
                .build();
        produit8.calculTaxe();

        Produit produit9 = Produit.builder()
                .nom("Moufles Totora perou")
                .reference("TOTORA-MAIN-ANDREA")
                .description("Moufles Totora fait main avec du coton venant des alpage peruvien, 100% naturel.")
                .date_ajout(new Timestamp(new Date().getTime()))
                .image(null)
                .poids(0.8f)
                .pointsFidelite(4)
                .prixHT(9.5)
                .tauxTaxe(0.2f)
                .quantiteStock(20)
                .tags(new HashSet<>())
                .build();
        produit9.calculTaxe();

        Produit produit10 = Produit.builder()
                .nom("Bango Bahia")
                .reference("BANGOBAHIA-MAIN-ANTONIO")
                .description("Bango bahia est un instrument de musique fabriqué au Brésil selon les bonnes moeurs du commerce equitable.")
                .date_ajout(new Timestamp(new Date().getTime()))
                .image(null)
                .poids(0.8f)
                .pointsFidelite(4)
                .prixHT(9.5)
                .tauxTaxe(0.2f)
                .quantiteStock(20)
                .tags(new HashSet<>())
                .build();
        produit10.calculTaxe();

        Produit produit11 = Produit.builder()
                .nom("Poupee russe")
                .reference("POUPEE-RUSSE-VLADISLAV")
                .description("poupee russe traditionnel 100% fait main.")
                .date_ajout(new Timestamp(new Date().getTime()))
                .image(null)
                .poids(0.8f)
                .pointsFidelite(4)
                .prixHT(21.4)
                .tauxTaxe(0.2f)
                .quantiteStock(20)
                .tags(new HashSet<>())
                .build();
        produit11.calculTaxe();

        Produit produit12 = Produit.builder()
                .nom("chapka polonaise")
                .reference("CHAPKA-MAIN-ALICIA")
                .description("Chapka en Fourrure d'ours brun.")
                .date_ajout(new Timestamp(new Date().getTime()))
                .image(null)
                .poids(0.8f)
                .pointsFidelite(4)
                .prixHT(35.5)
                .tauxTaxe(0.2f)
                .quantiteStock(20)
                .tags(new HashSet<>())
                .build();
        produit12.calculTaxe();

        Artisan artisan = artisanRepository.findByUser(userRepository.findUserByMail("wang@test.ch"));
        produit1.setArtisan(artisan);
        produit2.setArtisan(artisan);
        produit3.setArtisan(artisan);
        produit4.setArtisan(artisan);

        Artisan artisan1 = artisanRepository.findByUser(userRepository.findUserByMail("vladislav@test.ru"));
        produit11.setArtisan(artisan1);
        produit5.setArtisan(artisan1);
        produit6.setArtisan(artisan1);

        Artisan artisan2 = artisanRepository.findByUser(userRepository.findUserByMail("antonio@test.br"));
        produit10.setArtisan(artisan2);

        Artisan artisan3 = artisanRepository.findByUser(userRepository.findUserByMail("priash@test.in"));
        produit7.setArtisan(artisan3);

        Artisan artisan4 = artisanRepository.findByUser(userRepository.findUserByMail("alicia@test.pl"));
        produit12.setArtisan(artisan4);

        Artisan artisan5 = artisanRepository.findByUser(userRepository.findUserByMail("andrea@test.pe"));
        produit8.setArtisan(artisan5);

        Tag tag = tagRepository.findTagByNom("JAPON");
        produit1.ajoutTag(tag);
        produit2.ajoutTag(tag);
        produit4.ajoutTag(tag);

        Tag tag1 = tagRepository.findTagByNom("RUSSIE");
        produit11.ajoutTag(tag1);
        produit5.ajoutTag(tag1);
        produit6.ajoutTag(tag1);

        Tag tag2 = tagRepository.findTagByNom("BRESIL");
        produit10.ajoutTag(tag2);

        Tag tag3 = tagRepository.findTagByNom("INDE");
        produit7.ajoutTag(tag3);

        Tag tag4 = tagRepository.findTagByNom("POLOGNE");
        produit11.ajoutTag(tag4);

        Tag tag5 = tagRepository.findTagByNom("PEROU");
        produit8.ajoutTag(tag5);

        produitRepository.save(Arrays.asList(produit1,produit2,produit3,produit4,produit5,produit6,produit7,produit8,produit9,produit10,produit11,produit12));
    }

    private void initUsers(){
        Role roleUser = roleRepository.findRoleByNom("USER");
        Role roleAdmin = roleRepository.findRoleByNom("ADMIN");

        User userTest = User.builder()
                .prenom("testUser")
                .mail("testUser@test.fr")
                .dateCreation(new Timestamp(new Date().getTime()))
                .password(passwordEncoder.encode("test"))
                .role(roleUser)
                .pointsFidelite(0)
                .listeEnvies(new HashSet<>())
                .bonAchats(null)
                .estAbonne(false)
                .estActive(true)
                .build();
        roleUser.getUsers().add(userTest);

        List<Tag> listeEnvies = tagRepository.findTagsByNomIn(new HashSet<>(Arrays.asList("JAPON","RUSSIE")));
        for(Tag envie : listeEnvies){
            userTest.ajoutEnvie(envie);
        }

        userRepository.save(userTest);

        User mailHani = User.builder()
                .prenom("Hani")
                .mail("hani.denden@live.fr")
                .dateCreation(new Timestamp(new Date().getTime()))
                .password(passwordEncoder.encode("hani"))
                .role(roleRepository.findRoleByNom("USER"))
                .pointsFidelite(0)
                .bonAchats(new ArrayList<>())
                .listeEnvies(new HashSet<>())
                .estAbonne(false)
                .estActive(true)
                .build();
        roleUser.getUsers().add(mailHani);

        User userAdmin = User.builder()
                .prenom("testAdmin")
                .mail("testAdmin@test.fr")
                .dateCreation(new Timestamp(new Date().getTime()))
                .password(passwordEncoder.encode("test"))
                .role(roleAdmin)
                .panier(null)
                .pointsFidelite(0)
                .bonAchats(new ArrayList<>())
                .listeEnvies(null)
                .estAbonne(false)
                .estActive(true)
                .build();
        roleAdmin.getUsers().add(userAdmin);

        userRepository.save(Arrays.asList(userTest,mailHani,userAdmin));
        roleRepository.save(Arrays.asList(roleUser,roleAdmin));
    }

    private void initPanier(){
        User userTest = userRepository.findUserByMail("testUser@test.fr");

        Panier panier = Panier.builder()
                .date_creation(new Timestamp(new Date().getTime()))
                .user(userTest)
                .pointsFidelitePanier(0)
                .poidsPanier(0f)
                .produits(null)
                .build();

        userTest.setPanier(panier);

        User userHani = userRepository.findUserByMail("hani.denden@live.fr");

        Panier panierHani = Panier.builder()
                .date_creation(new Timestamp(new Date().getTime()))
                .user(userHani)
                .pointsFidelitePanier(0)
                .poidsPanier(0f)
                .produits(null)
                .build();

        userHani.setPanier(panierHani);

        userRepository.save(Arrays.asList(userTest,userHani));
    }

    private void remplirPanier(){
        Panier panierUserTest = panierRepository.findByUser(userRepository.findUserByMail("testUser@test.fr"));
        Panier panierHani = panierRepository.findByUser(userRepository.findUserByMail("hani.denden@live.fr"));

        Produit produit1 = produitRepository.findProduitByNom("Shuriken Artisanal");
        Produit produit2 = produitRepository.findProduitByNom("Manga Fait Main");

        ProduitQuantitePanier produitQuantitePanier = ProduitQuantitePanier.builder()
                .produit(produit1)
                .panier(panierUserTest)
                .quantite(2)
                .build();

        ProduitQuantitePanier produitQuantitePanier2 = ProduitQuantitePanier.builder()
                .produit(produit2)
                .panier(panierUserTest)
                .quantite(1)
                .build();

        ProduitQuantitePanier produitQuantitePanierHani = ProduitQuantitePanier.builder()
                .produit(produit1)
                .panier(panierHani)
                .quantite(2)
                .build();

        ProduitQuantitePanier produitQuantitePanierHani2 = ProduitQuantitePanier.builder()
                .produit(produit2)
                .panier(panierHani)
                .quantite(1)
                .build();

        produitQuantitePanierRepository.save(Arrays.asList(produitQuantitePanier,produitQuantitePanier2,produitQuantitePanierHani,produitQuantitePanierHani2));

        panierUserTest.ajoutProduitsAuPanier(produitQuantitePanier,produitQuantitePanier2);
        panierHani.ajoutProduitsAuPanier(produitQuantitePanierHani,produitQuantitePanierHani2);

        panierRepository.save(Arrays.asList(panierUserTest,panierHani));
    }

    private void initLivraisons(){
        Livraison livraison1 = Livraison.builder()
                .societe(Societe.CHRONOPOST)
                .poidsMin(0.1f)
                .poidsMax(2f)
                .prixMin(1d)
                .prixMax(2d)
                .build();

        Livraison livraison2 = Livraison.builder()
                .societe(Societe.CHRONOPOST)
                .poidsMin(2f)
                .poidsMax(5f)
                .prixMin(3d)
                .prixMax(7d)
                .build();

        Livraison livraison3 = Livraison.builder()
                .societe(Societe.CHRONOPOST)
                .poidsMin(5f)
                .poidsMax(12f)
                .prixMin(8d)
                .prixMax(16d)
                .build();

        Livraison livraison4 = Livraison.builder()
                .societe(Societe.CHRONOPOST)
                .poidsMin(12f)
                .poidsMax(30f)
                .prixMin(17d)
                .prixMax(50d)
                .build();

        Livraison livraison5 = Livraison.builder()
                .societe(Societe.CHRONOPOST)
                .poidsMin(30f)
                .poidsMax(60f)
                .prixMin(51d)
                .prixMax(90d)
                .build();

        Livraison livraison6 = Livraison.builder()
                .societe(Societe.CHRONOPOST)
                .poidsMin(60f)
                .poidsMax(100f)
                .prixMin(91d)
                .prixMax(120d)
                .build();

        livraisonRepository.save(Arrays.asList(livraison1,livraison2,livraison3,livraison4,livraison5,livraison6));
    }

    private void initBonAchats() {
        User userTest = userRepository.findUserByMail("testUser@test.fr");

        BonAchat bonAchat1 = BonAchat.builder()
                .date_creation(new Timestamp(new Date().getTime()))
                .valeur(10d)
                .user(userTest)
                .build();
        userTest.getBonAchats().add(bonAchat1);

        BonAchat bonAchat2 = BonAchat.builder()
                .date_creation(new Timestamp(new Date().getTime()))
                .valeur(10d)
                .user(userTest)
                .build();
        userTest.getBonAchats().add(bonAchat2);

        BonAchat bonAchat3 = BonAchat.builder()
                .date_creation(new Timestamp(new Date().getTime()))
                .valeur(15d)
                .user(userTest)
                .build();
        userTest.getBonAchats().add(bonAchat3);

        userRepository.save(userTest);
        bonAchatRepository.save(Arrays.asList(bonAchat1,bonAchat2,bonAchat3));
    }

    private void initInterrogations() {
        Tag tagJapon = tagRepository.findTagByNom("JAPON");
        Tag tagManga = tagRepository.findTagByNom("MANGA");

        Reponse reponseOui = Reponse.builder()
                .interrogation(null)
                .texte("Oui")
                .tags(new HashSet<>(Arrays.asList(tagJapon,tagManga)))
                .build();

        Reponse reponseNon = Reponse.builder()
                .interrogation(null)
                .texte("Non")
                .tags(new HashSet<>(Collections.singletonList(tagJapon)))
                .build();

        reponseRepository.save(Arrays.asList(reponseOui,reponseNon));

        Interrogation interrogation = Interrogation.builder()
                .question("Etes-vous fan des mangas ?")
                .reponses(Arrays.asList(reponseOui,reponseNon))
                .build();

        interrogationRepository.save(interrogation);

        reponseOui.setInterrogation(interrogation);
        reponseNon.setInterrogation(interrogation);

        reponseRepository.save(Arrays.asList(reponseOui,reponseNon));
    }

}
