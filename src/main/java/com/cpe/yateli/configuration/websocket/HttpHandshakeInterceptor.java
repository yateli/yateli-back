package com.cpe.yateli.configuration.websocket;

import com.cpe.yateli.model.User;
import com.cpe.yateli.repository.UserRepository;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.usersConnectes.UserConnecte;
import com.cpe.yateli.service.usersConnectes.UserConnecteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class HttpHandshakeInterceptor implements ChannelInterceptor {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserConnecteRepository userConnecteRepository;

    @Autowired
    private EvenementService evenementService;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {

        final StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

        if(accessor != null && StompCommand.CONNECT == accessor.getCommand()){
            final String login = accessor.getFirstNativeHeader("login");
            final String token = accessor.getFirstNativeHeader("token");

            UserConnecte userConnecte = userConnecteRepository.findOne(login);

            if(userConnecte != null && !userConnecte.getTokenWebsocket().equals(token)){
                userConnecteRepository.delete(userConnecte);
                String logCreated = "Connexion Websocket refusée pour user " + userConnecte.getLogin() + " car non authentifié avec token";
                log.info(logCreated);
                User user = userRepository.findUserByMail(userConnecte.getLogin());
                evenementService.enregistrerEvenement(user, logCreated);
                return null;
            }

            final UsernamePasswordAuthenticationToken user = new UsernamePasswordAuthenticationToken(login, null, Collections.singletonList((GrantedAuthority) () -> "USER"));
            accessor.setUser(user);

        }

        return message;

    }

    @Override
    public void postSend(Message<?> message, MessageChannel channel, boolean sent) {

    }

    @Override
    public void afterSendCompletion(Message<?> message, MessageChannel channel, boolean sent, Exception ex) {

    }

    @Override
    public boolean preReceive(MessageChannel channel) {
        return true;
    }

    @Override
    public Message<?> postReceive(Message<?> message, MessageChannel channel) {
        return message;
    }

    @Override
    public void afterReceiveCompletion(Message<?> message, MessageChannel channel, Exception ex) {

    }

}
