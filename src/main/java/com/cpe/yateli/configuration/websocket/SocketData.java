package com.cpe.yateli.configuration.websocket;

import com.cpe.yateli.model.Alerte;
import com.google.gson.annotations.Expose;
import lombok.*;

@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SocketData {

    @Expose
    private Alerte alerte;

}
