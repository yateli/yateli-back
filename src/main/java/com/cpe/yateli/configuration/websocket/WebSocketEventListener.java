package com.cpe.yateli.configuration.websocket;

import com.cpe.yateli.model.User;
import com.cpe.yateli.repository.UserRepository;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.usersConnectes.UserConnecte;
import com.cpe.yateli.service.usersConnectes.UserConnecteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.Date;

@Component
public class WebSocketEventListener {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserConnecteRepository userConnecteRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EvenementService evenementService;

    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event){

        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(event.getMessage());
        if(headers.getUser() != null){
            UserConnecte userConnecte = userConnecteRepository.findOne(headers.getUser().getName());

            long date = new Date().getTime();
            if(userConnecte != null){
                userConnecte.setTokenWebsocket(null);
                userConnecte.setTimestampConnexion(date);
                userConnecte.setWebSocketActif(true);
                userConnecteRepository.save(userConnecte);
                String logCreated = "Nouvelle personne connectée à " + String.valueOf(date) + " : " + userConnecte.getLogin();
                log.info(logCreated);
                User user = userRepository.findUserByMail(userConnecte.getLogin());
                evenementService.enregistrerEvenement(user, logCreated);
            }

        }

    }

    @EventListener
    public void handleWebsocketDisconnectListener(SessionDisconnectEvent event) {

        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(event.getMessage());
        if(headers.getUser() != null){
            UserConnecte userConnecte = userConnecteRepository.findOne(headers.getUser().getName());
            if(userConnecte != null){
                userConnecteRepository.delete(userConnecte);
                String logCreated = "Session websocket du User " + userConnecte.getLogin() + " déconnectée à " + new Date().getTime();
                log.info(logCreated);
                User user = userRepository.findUserByMail(userConnecte.getLogin());
                evenementService.enregistrerEvenement(user, logCreated);
            }
        }

    }

}
