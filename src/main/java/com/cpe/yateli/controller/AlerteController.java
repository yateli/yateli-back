package com.cpe.yateli.controller;

import com.cpe.yateli.exception.alerte.AlerteExistePasException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.Alerte;
import com.cpe.yateli.model.dto.MessageDto;
import com.cpe.yateli.service.AlerteService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/notification")
@Api(value = "/api/notification", description = "Actions concernant les notifications")
public class AlerteController {

    @Autowired
    private AlerteService alerteService;

    @GetMapping(value = "/recupererNotifications", produces = { "application/json" })
    public ResponseEntity<List<Alerte>> recupererNotifications(@ApiIgnore Principal moi) throws UserNonTrouveException {

        List<Alerte> notifications = alerteService.recupererNotifications(moi);

        return ResponseEntity.ok().body(notifications);

    }

    @PutMapping(value = "/notificationVue/{id}", produces = { "application/json" })
    public ResponseEntity<MessageDto> notificationVue(@ApiIgnore Principal moi,
                                                     @PathVariable(value = "id") String id) throws UserNonTrouveException, AlerteExistePasException {

        MessageDto status = alerteService.notificationVue(moi, id);

        return ResponseEntity.ok().body(status);

    }

    @DeleteMapping(value = "/supprimerNotification/{id}", produces = { "application/json" })
    public ResponseEntity<MessageDto> supprimerNotification(@ApiIgnore Principal moi,
                                                            @PathVariable(value = "id") String id) throws UserNonTrouveException, AlerteExistePasException {

        MessageDto status = alerteService.supprimerNotification(moi, id);

        return ResponseEntity.ok().body(status);

    }

}
