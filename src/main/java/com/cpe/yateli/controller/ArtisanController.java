package com.cpe.yateli.controller;

import com.cpe.yateli.exception.artisan.*;
import com.cpe.yateli.exception.produit.ProduitABloquerException;
import com.cpe.yateli.exception.role.RoleExistePasException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.Artisan;
import com.cpe.yateli.model.Blocage;
import com.cpe.yateli.model.InfoFichier;
import com.cpe.yateli.model.dto.ArtisanDto;
import com.cpe.yateli.model.dto.MessageDto;
import com.cpe.yateli.service.ArtisanService;
import com.google.common.io.ByteStreams;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/artisan")
@Api(value = "/api/artisan", description = "Actions concernant les artisans")
public class ArtisanController {

    @Autowired
    private ArtisanService artisanService;

    @GetMapping(value = "/recupererImage/{idArtisan}", produces = { "application/json" })
    public ResponseEntity<byte[]> recupererImage(@PathVariable(value = "idArtisan") int idArtisan) throws IOException, ArtisanSansImageException, SQLException, ArtisanExistePasException {

        InfoFichier fichier = artisanService.recupererImage(idArtisan);

        if(fichier == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        byte[] imageRecuperee = ByteStreams.toByteArray(fichier.getFichier().getInputStream());
        byte[] encodedImageRecuperee = java.util.Base64.getEncoder().encode(imageRecuperee);
        MediaType typeFichier = MediaType.parseMediaType(fichier.getExtension());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "artisan." + fichier.getExtension().split("/")[1] +"\"")
                .contentType(typeFichier)
                .contentLength(encodedImageRecuperee.length)
                .body(encodedImageRecuperee);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN') || hasRole('ROLE_ARTISAN')")
    @PutMapping(value = "/modifier", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = { "application/json" })
    public ResponseEntity<Artisan> modifierArtisan(@Valid @RequestPart(value = "artisan") ArtisanDto artisan,
                                                   @RequestPart(value = "image", required = false) MultipartFile image) throws SQLException, IOException, ArtisanExistePasException, UserBloqueException {

        Artisan artisanModifie = artisanService.modifierArtisan(artisan, image);

        return ResponseEntity.ok().body(artisanModifie);

    }

    @PreAuthorize("hasRole('ROLE_USER')")
    @PostMapping(value = "/demande", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = { "application/json" })
    public ResponseEntity<Artisan> demandeArtisan(@Valid @RequestPart(value = "artisan") ArtisanDto artisan,
                                                  @RequestPart(value = "image", required = false) MultipartFile image,
                                                  @ApiIgnore Principal moi) throws UserNonTrouveException, SQLException, RoleExistePasException, IOException, UserBloqueException, ArtisanEnCoursAutorisationException {

        Artisan artisanCree = artisanService.demandeArtisan(artisan,image,moi);

        return ResponseEntity.ok().body(artisanCree);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/bloquerArtisan", produces = { "application/json" })
    public ResponseEntity<List<Blocage>> bloquerArtisan(@ApiIgnore Principal moi,
                                                        @RequestParam(value = "idArtisan") int idArtisan,
                                                        @RequestParam(value = "raison") String raison) throws UserNonTrouveException, ArtisanExistePasException, ProduitABloquerException {

        List<Blocage> blocages = artisanService.bloquerArtisan(moi, idArtisan, raison);

        return ResponseEntity.ok().body(blocages);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/autoriserArtisan", produces = { "application/json" })
    public ResponseEntity<Blocage> autoriserArtisan(@RequestParam(value = "idArtisan") int idArtisan) throws UserNonTrouveException, ArtisanExistePasException, ProduitABloquerException, ArtisanNonBloqueException {

        Blocage autorisation = artisanService.autoriserArtisan(idArtisan);

        return ResponseEntity.ok().body(autorisation);

    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/acceptation", produces = { "application/json" })
    public ResponseEntity<MessageDto> acceptationArtisan(@ApiIgnore Principal moi,
                                                         @RequestParam(value = "loginArtisan") String loginArtisan) throws UserNonTrouveException, ArtisanExistePasException, ArtisanDejaAutoriseException {

        String acceptation = artisanService.acceptationArtisan(moi, loginArtisan);

        return ResponseEntity.ok().body(new MessageDto(acceptation));

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/refuser", produces = { "application/json" })
    public ResponseEntity<MessageDto> refuserArtisan(@ApiIgnore Principal moi,
                                                     @RequestParam(value = "loginArtisan") String loginArtisan,
                                                     @RequestParam(value = "raison") String raison) throws UserNonTrouveException, ArtisanExistePasException, ArtisanDejaAutoriseException {

        String refus = artisanService.refuserArtisan(moi, loginArtisan, raison);

        return ResponseEntity.ok().body(new MessageDto(refus));

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/getArtisansAaccepter", produces = { "application/json" })
    public ResponseEntity<List<Artisan>> getArtisansAaccepter(){

        List<Artisan> artisans = artisanService.getArtisansAaccepter();

        return ResponseEntity.ok().body(artisans);

    }

    @GetMapping(value = "/getTousLesArtisans", produces = { "application/json" })
    public ResponseEntity<List<Artisan>> getTousLesArtisans(){

        List<Artisan> artisans = artisanService.getTousLesArtisans();

        if(artisans == null){
            return ResponseEntity.ok().body(new ArrayList<>());
        }

        return ResponseEntity.ok().body(artisans);

    }

    @PreAuthorize("hasRole('ROLE_ARTISAN')")
    @GetMapping(value = "/recupererArtisan", produces = { "application/json" })
    public ResponseEntity<Artisan> recupererArtisan(@ApiIgnore Principal moi){

        Artisan artisan = artisanService.getArtisan(moi.getName());

        if(artisan == null){
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().body(artisan);

    }

}
