package com.cpe.yateli.controller;

import com.cpe.yateli.exception.colisMystere.DateDecouverteColisMystereException;
import com.cpe.yateli.exception.user.NonAbonneException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.ColisMystere;
import com.cpe.yateli.service.ColisMystereService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;

@PreAuthorize("hasRole('ROLE_USER')")
@RestController
@RequestMapping("/api/colismystere")
public class ColisMystereController {

    @Autowired
    private ColisMystereService colisMystereService;

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @GetMapping(value = "/getColisMystere", produces = { "application/json" })
    public ResponseEntity<ColisMystere> getColisMystere(@ApiIgnore Principal moi) throws UserNonTrouveException, DateDecouverteColisMystereException, NonAbonneException, UserBloqueException {

        ColisMystere colisMystere = colisMystereService.getColisMystere(moi);

        return ResponseEntity.ok().body(colisMystere);

    }

}
