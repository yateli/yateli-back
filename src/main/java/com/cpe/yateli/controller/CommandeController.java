package com.cpe.yateli.controller;

import com.cpe.yateli.model.Commande;
import com.cpe.yateli.model.dto.CommandeDto;
import com.cpe.yateli.service.CommandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@PreAuthorize("hasRole('ROLE_USER')")
@RestController
@RequestMapping("/api/commande")
public class CommandeController {

    @Autowired
    private CommandeService commandeService;

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @PostMapping(value = "/passerCommande", produces = { "application/json" })
    public ResponseEntity<Commande> passerCommande(@ApiIgnore Principal moi,
                                                   @Valid @RequestBody CommandeDto commande) throws Exception {

        Commande commandePassee = commandeService.passerCommande(moi,commande);

        if(commandePassee == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok().body(commandePassee);

    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @GetMapping(value = "/recupererCommandes", produces = { "application/json" })
    public ResponseEntity<List<Commande>> recupererCommandes(@ApiIgnore Principal moi) throws Exception {

        List<Commande> commandes = commandeService.getCommandesUser(moi);

        if(commandes == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok().body(commandes);

    }

}
