package com.cpe.yateli.controller;

import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.service.EvenementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/evenement")
public class EvenementController {

    @Autowired
    private EvenementService evenementService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/recuperer", produces = { "application/json" })
    public ResponseEntity<List<String>> recupererEvenements(@RequestParam(value = "login") String login) throws UserNonTrouveException {

        List<String> logs = evenementService.recupererEvenementsUser(login);

        return ResponseEntity.ok().body(logs);

    }

}
