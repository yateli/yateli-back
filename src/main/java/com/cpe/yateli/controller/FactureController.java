package com.cpe.yateli.controller;

import com.cpe.yateli.exception.facture.FactureExistePasException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.view.FactureView;
import com.cpe.yateli.service.FactureService;
import com.google.common.io.ByteStreams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/facture")
public class FactureController {

    @Autowired
    private FactureService factureService;

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @GetMapping(value = "/getListFactureUser", produces = { "application/json" })
    public ResponseEntity<List<FactureView>> getListFactureUser(@ApiIgnore Principal moi) throws UserNonTrouveException, FactureExistePasException, UserBloqueException {

        List<FactureView> factureViews = factureService.getFacturesUser(moi);

        if(factureViews == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok().body(factureViews);

    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @GetMapping(value = "/getFacture/{idFacture}")
    public @ResponseBody ResponseEntity<byte[]> getFacture(@ApiIgnore Principal moi,
                                               @PathVariable(value = "idFacture") int idFacture) throws UserNonTrouveException, SQLException, FactureExistePasException, IOException, UserBloqueException {

        Resource resource = factureService.getFacture(moi, idFacture);

        if(resource == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Date date = new Date();
        String dateFactureRecupere = new SimpleDateFormat("ddMMyy").format(date);

        byte[] pdfGeneree = ByteStreams.toByteArray(resource.getInputStream());
        byte[] encodedPdf = java.util.Base64.getEncoder().encode(pdfGeneree);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "Facture" + dateFactureRecupere + ".pdf" +"\"")
                .contentType(MediaType.APPLICATION_PDF)
                .contentLength(encodedPdf.length)
                .body(encodedPdf);

    }

}
