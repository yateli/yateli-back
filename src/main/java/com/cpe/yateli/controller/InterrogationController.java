package com.cpe.yateli.controller;

import com.cpe.yateli.exception.interrogation.*;
import com.cpe.yateli.exception.tag.TagExistePasException;
import com.cpe.yateli.model.Interrogation;
import com.cpe.yateli.model.dto.InterrogationActivesDto;
import com.cpe.yateli.model.dto.InterrogationDto;
import com.cpe.yateli.model.dto.MessageDto;
import com.cpe.yateli.service.InterrogationService;
import org.apache.commons.codec.EncoderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/interrogation")
public class InterrogationController {

    @Autowired
    private InterrogationService interrogationService;

    @GetMapping(value = "/recupererQuestions", produces = { "application/json" })
    public ResponseEntity<List<Interrogation>> recupererQuestions(){

        List<Interrogation> questions = interrogationService.getInterrogationsActives();

        return ResponseEntity.ok().body(questions);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/recupererToutesQuestions", produces = { "application/json" })
    public ResponseEntity<List<Interrogation>> recupererToutesQuestions(){

        List<Interrogation> questions = interrogationService.getToutesInterrogations();

        return ResponseEntity.ok().body(questions);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/creerQuestion", produces = { "application/json" })
    public ResponseEntity<Interrogation> creerQuestion(@Valid @RequestBody InterrogationDto interrogationDto) throws QuestionExisteDejaException, EncoderException, TagExistePasException {

        Interrogation question = interrogationService.creerQuestion(interrogationDto);

        return ResponseEntity.ok().body(question);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/supprimerQuestion", produces = { "application/json" })
    public ResponseEntity<MessageDto> supprimerQuestion(@RequestParam(value = "idQuestion") Integer idQuestion) throws QuestionExistePasException {

        String message = interrogationService.supprimerQuestion(idQuestion);

        if(message == null){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.ok().body(new MessageDto(message));

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/activerQuestions", produces = { "application/json" })
    public ResponseEntity<MessageDto> activerQuestions(@Valid @RequestBody InterrogationActivesDto idQuestions) throws QuestionExistePasException, QuestionsActivesException, QuestionDejaActiveException {

        String message = interrogationService.activerQuestions(idQuestions);

        if(message == null){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.ok().body(new MessageDto(message));
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/desactiverQuestions", produces = { "application/json" })
    public ResponseEntity<MessageDto> desactiverQuestions(@Valid @RequestBody InterrogationActivesDto idQuestions) throws QuestionExistePasException, QuestionsActivesException, QuestionPasActiveException {

        String message = interrogationService.desactiverQuestions(idQuestions);

        if(message == null){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        return ResponseEntity.ok().body(new MessageDto(message));
    }

}
