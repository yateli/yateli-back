package com.cpe.yateli.controller;

import com.cpe.yateli.exception.jeuConcours.*;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.JeuConcours;
import com.cpe.yateli.model.User;
import com.cpe.yateli.model.dto.JeuConcoursDto;
import com.cpe.yateli.service.JeuConcoursService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/jeuConcours")
public class JeuConcoursController {

    @Autowired
    private JeuConcoursService jeuConcoursService;

    @ApiOperation(value = "Créer un jeu concours et inviter des utilisateurs à ce jeu",
                  notes = "Les timestamps de début et de fin sont à exprimer en millisecondes",
                  response = JeuConcours.class)
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/creer", produces = { "application/json" })
    public ResponseEntity<JeuConcours> creerJeuConcours(@Valid @RequestBody JeuConcoursDto jeuConcoursDto) throws JeuConcoursExisteDejaException {

        JeuConcours jeuConcours = jeuConcoursService.creerJeuConcours(jeuConcoursDto);

        return ResponseEntity.ok().body(jeuConcours);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/changerEtatJeuConcours", produces = { "application/json" })
    public ResponseEntity<JeuConcours> changerEtatJeuConcours(@RequestParam(value = "id") Integer id,
                                                              @RequestParam(value = "estFerme") boolean estFerme) throws JeuConcoursExistePasException, JeuConcoursDejaOuvertFermeException {

        JeuConcours jeuConcoursFerme = jeuConcoursService.changerEtatExceptionnelJeuConcours(id, estFerme);

        return ResponseEntity.ok().body(jeuConcoursFerme);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN') || hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @GetMapping(value = "/recupererTousLesConcours", produces = { "application/json" })
    public ResponseEntity<List<JeuConcours>> recupererTousLesConcours(@ApiIgnore Principal moi) throws UserNonTrouveException, UserBloqueException {

        List<JeuConcours> jeuConcours = jeuConcoursService.recupererTousLesConcours(moi);

        return ResponseEntity.ok().body(jeuConcours);

    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @PutMapping(value = "/participerJeuConcours", produces = { "application/json" })
    public ResponseEntity<JeuConcours> participerJeuConcours(@ApiIgnore Principal moi,
                                                             @RequestParam(value = "idJeuConcours") int idJeuConcours) throws UserNonTrouveException, PointsFideliteInsuffisantException, JeuConcoursExistePasException, JeuConcoursFermeException, JeuConcoursDejaInscritException, JeuConcoursFermeExceptionnelementException, UserBloqueException {

        JeuConcours jeuConcours = jeuConcoursService.participerJeuConcours(idJeuConcours, moi);

        return ResponseEntity.ok().body(jeuConcours);

    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @PutMapping(value = "/desinscrireJeuConcours", produces = { "application/json" })
    public ResponseEntity<User> desinscrireJeuConcours(@ApiIgnore Principal moi,
                                                       @RequestParam(value = "idJeuConcours") int idJeuConcours) throws UserNonTrouveException, JeuConcoursParticipePasException, JeuConcoursExistePasException, UserBloqueException {

        User user = jeuConcoursService.desinscrireJeuConcours(idJeuConcours, moi);

        return ResponseEntity.ok().body(user);

    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @GetMapping(value = "/recupererJeuConcours", produces = { "application/json" })
    public ResponseEntity<List<JeuConcours>> recupererJeuConcours(@ApiIgnore Principal moi) throws UserNonTrouveException, UserBloqueException {

        List<JeuConcours> jeuConcours = jeuConcoursService.recupererJeuConcours(moi);

        if(jeuConcours == null){
            return ResponseEntity.ok().body(new ArrayList<>());
        }

        return ResponseEntity.ok().body(jeuConcours);

    }

}
