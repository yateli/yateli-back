package com.cpe.yateli.controller;

import com.cpe.yateli.exception.commande.CommandeDejaPayeeException;
import com.cpe.yateli.exception.commande.CommandeNonTrouveeException;
import com.cpe.yateli.model.dto.MessageDto;
import com.cpe.yateli.model.view.FactureView;
import com.cpe.yateli.service.PaiementService;
import com.paypal.base.rest.PayPalRESTException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;

@RestController
@RequestMapping("/api/paiement")
public class PaiementController {

    @Autowired
    private PaiementService paiementService;

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @GetMapping(value = "/payerCommande/{idCommande}", produces = { "application/json" })
    public ResponseEntity<MessageDto> payerCommande(@ApiIgnore Principal moi,
                                                    @PathVariable(value = "idCommande") int idCommande) throws PayPalRESTException, CommandeNonTrouveeException, CommandeDejaPayeeException {

        String response = paiementService.payerCommande(moi,idCommande);

        return ResponseEntity.ok().body(new MessageDto(response));

    }

    @ApiOperation(value = "Validation de paiement Paypal",
            notes = "Etape nécessaire pour que la transaction soit effective",
            response = FactureView.class)
    @GetMapping(value = "/returnPaiement", produces = { "application/json" })
    public ResponseEntity<FactureView> returnPaiement(@RequestParam(value = "PayerID") String payerId,
                                  @RequestParam(value = "paymentId") String paymentId) throws Exception {

        FactureView factureGeneree = paiementService.effectuerPaiement(payerId,paymentId);

        return ResponseEntity.ok().body(factureGeneree);

    }

}
