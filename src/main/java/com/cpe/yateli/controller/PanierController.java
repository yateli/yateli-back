package com.cpe.yateli.controller;

import com.cpe.yateli.exception.panier.ProduitDejaPresentPanierException;
import com.cpe.yateli.exception.panier.ProduitInexistantException;
import com.cpe.yateli.exception.panier.ProduitQuantitePanierExistePasException;
import com.cpe.yateli.exception.panier.QuantiteIncorrecteException;
import com.cpe.yateli.exception.produit.StockProduitInsuffisantException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.Panier;
import com.cpe.yateli.service.PanierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;

@PreAuthorize("hasRole('ROLE_USER')")
@RestController
@RequestMapping("/api/panier")
public class PanierController {

    @Autowired
    private PanierService panierService;

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @PutMapping(value = "/ajoutProduit", produces = { "application/json" })
    public ResponseEntity<Panier> ajouterProduit(@ApiIgnore Principal moi,
                                                 @RequestParam(value = "reference") String reference,
                                                 @RequestParam(value = "quantite") int quantite) throws ProduitInexistantException, UserNonTrouveException, QuantiteIncorrecteException, StockProduitInsuffisantException, UserBloqueException {

        Panier panier = panierService.ajoutProduit(moi,reference,quantite);

        if(panier == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok().body(panier);

    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @PutMapping(value = "/modifierProduit", produces = { "application/json" })
    public ResponseEntity<Panier> modifierProduit(@ApiIgnore Principal moi,
                                                 @RequestParam(value = "reference") String reference,
                                                 @RequestParam(value = "quantite") int quantite) throws ProduitInexistantException, UserNonTrouveException, QuantiteIncorrecteException, StockProduitInsuffisantException, UserBloqueException {

        Panier panier = panierService.modifierQuantiteProduit(moi,reference,quantite);

        if(panier == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok().body(panier);

    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @PutMapping(value = "/supprimerProduit", produces = { "application/json" })
    public ResponseEntity<Panier> supprimerProduit(@ApiIgnore Principal moi,
                                                   @RequestParam(value = "reference") String reference,
                                                   @RequestParam(value = "quantite") int quantite) throws ProduitInexistantException, UserNonTrouveException, UserBloqueException {

        Panier panier = panierService.supprimerProduit(moi,reference,quantite);

        if(panier == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok().body(panier);

    }

}
