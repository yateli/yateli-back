package com.cpe.yateli.controller;

import com.cpe.yateli.model.Pays;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/pays")
public class PaysController {

    @GetMapping(value = "/recupererPays", produces = { "application/json" })
    public ResponseEntity<List<Pays>> recupererPays(){
        return ResponseEntity.ok().body(new ArrayList<>(Arrays.asList(Pays.values())));
    }

}
