package com.cpe.yateli.controller;

import com.cpe.yateli.exception.phraseDuJour.PhraseDuJourException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.PhraseDuJour;
import com.cpe.yateli.model.dto.MessageDto;
import com.cpe.yateli.service.PhraseDuJourService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/phraseDuJour")
@Api(value = "/api/phraseDuJour", description = "Controlleur portant sur la phrase du jour affichée dans la barre du haut")
public class PhraseDuJourController {

    @Autowired
    private PhraseDuJourService phraseDuJourService;

    @ApiOperation(value = "Création Phrase Du Jour",
            notes = "Nouvelle phrase du jour créée par admin courant")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/creer", produces = { "application/json" })
    public ResponseEntity<PhraseDuJour> creerPhraseDuJour(@ApiIgnore Principal moi,
                                                          @RequestParam(value = "phrase") String phrase,
                                                          @RequestParam(value = "urlImage") String urlImage) throws UserNonTrouveException {

        PhraseDuJour phraseDuJour = phraseDuJourService.creerPhraseDuJour(moi, phrase, urlImage);

        return ResponseEntity.ok().body(phraseDuJour);

    }

    @ApiOperation(value = "Modification Phrase Du Jour",
            notes = "Cette action remplacera la dernière phrase du jour appartenant à l'admin effectuant la requête par la nouvelle phrase en paramètre")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/modifier", produces = { "application/json" })
    public ResponseEntity<PhraseDuJour> modifierPhraseDuJour(@ApiIgnore Principal moi,
                                                             @RequestParam(value = "phrase") String phrase,
                                                             @RequestParam(value = "urlImage") String urlImage) throws UserNonTrouveException, PhraseDuJourException {

        PhraseDuJour phraseDuJour = phraseDuJourService.modifierPhraseDuJour(moi, phrase, urlImage);

        return ResponseEntity.ok().body(phraseDuJour);

    }

    @ApiOperation(value = "Suppression Phrase Du Jour",
            notes = "Cette action supprimera la dernière phrase du jour appartenant à l'admin et la remplacera par la dernière phrase que l'admin avait créée")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/supprimer", produces = { "application/json" })
    public ResponseEntity<MessageDto> supprimerPhraseDuJour(@ApiIgnore Principal moi) throws UserNonTrouveException, PhraseDuJourException {

        String statut = phraseDuJourService.supprimerPhraseDuJour(moi);

        return ResponseEntity.ok().body(new MessageDto(statut));

    }

    @ApiOperation(value = "Récupération de toutes les Phrases Du Jour",
            notes = "Administrateur récupère toutes les phrases du jour")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/getToutesPhrasesDuJour", produces = { "application/json" })
    public ResponseEntity<List<PhraseDuJour>> getToutesLesPhrasesDuJour(@ApiIgnore Principal moi) throws PhraseDuJourException {

        List<PhraseDuJour> phraseDuJourList = phraseDuJourService.getToutesPhrasesDuJour();

        return ResponseEntity.ok().body(phraseDuJourList);

    }

    @ApiOperation(value = "Récupération Phrase Du Jour",
            notes = "Utilisateur courant récupère la dernière phrase du jour")
    @GetMapping(value = "/getPhraseDuJour", produces = { "application/json" })
    public ResponseEntity<PhraseDuJour> getPhraseDuJour() throws PhraseDuJourException {

        PhraseDuJour phraseDuJour = phraseDuJourService.getPhraseDuJour();

        return ResponseEntity.ok().body(phraseDuJour);

    }

}
