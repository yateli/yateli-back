package com.cpe.yateli.controller;

import com.cpe.yateli.exception.artisan.ArtisanExistePasException;
import com.cpe.yateli.exception.artisan.ArtisanNonAutoriseProduitException;
import com.cpe.yateli.exception.panier.ProduitInexistantException;
import com.cpe.yateli.exception.produit.ProduitABloquerException;
import com.cpe.yateli.exception.produit.ProduitSansImageException;
import com.cpe.yateli.exception.produit.ReferenceIncorrecteSuppressionException;
import com.cpe.yateli.exception.produit.ReferenceProduitExisteDejaException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.Blocage;
import com.cpe.yateli.model.InfoFichier;
import com.cpe.yateli.model.Produit;
import com.cpe.yateli.model.dto.*;
import com.cpe.yateli.model.view.ProduitView;
import com.cpe.yateli.service.ProduitService;
import com.google.common.io.ByteStreams;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/produit")
@Api(value = "/api/produit", description = "Controlleur permettant de récupérer, créer, supprimer ou modifier des produits")
public class ProduitController {

    @Autowired
    private ProduitService produitService;

    @GetMapping(value = "/recupererTout", produces = { "application/json" })
    public ResponseEntity<List<ProduitView>> recupererProduits(){

        List<ProduitView> produits = produitService.getTousLesProduits();

        if(produits == null){
            return ResponseEntity.ok().body(new ArrayList<>());
        }

        return ResponseEntity.ok().body(produits);

    }

    @GetMapping(value = "/recupererProduitsArtisan", produces = { "application/json" })
    public ResponseEntity<List<ProduitView>> recupererProduitsArtisan(@RequestParam(value = "idArtisan") Integer idArtisan){

        List<ProduitView> produits = produitService.getProduitsArtisan(idArtisan);

        if(produits == null){
            return ResponseEntity.ok().body(new ArrayList<>());
        }

        return ResponseEntity.ok().body(produits);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/recupererToutAdmin", produces = { "application/json" })
    public ResponseEntity<List<Produit>> recupererProduitsAvecBloques(){

        List<Produit> produits = produitService.getTousLesProduitsAvecBloques();

        if(produits == null){
            return ResponseEntity.ok().body(new ArrayList<>());
        }

        return ResponseEntity.ok().body(produits);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/bloquerProduits", produces = { "application/json" })
    public ResponseEntity<List<Blocage>> bloquerProduits(@ApiIgnore Principal moi,
                                                         @Valid @RequestBody BloquerProduitDto produits) throws ProduitABloquerException, UserNonTrouveException {

        List<Blocage> produitsBloques = produitService.bloquerProduits(moi, produits);

        return ResponseEntity.ok().body(produitsBloques);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/autoriserProduits", produces = { "application/json" })
    public ResponseEntity<List<Produit>> autoriserProduits(@Valid @RequestBody BloquerProduitDto produits) throws ProduitABloquerException, UserNonTrouveException {

        List<Produit> produitsAutorises = produitService.autoriserProduits(produits);

        return ResponseEntity.ok().body(produitsAutorises);

    }

    @GetMapping(value = "/recupererImage/{reference}", produces = { "application/json" })
    public ResponseEntity<byte[]> recupererImage(@PathVariable(value = "reference") String reference) throws ProduitInexistantException, IOException, SQLException, ProduitSansImageException {

        InfoFichier fichier = produitService.recupererImage(reference);

        if(fichier == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        byte[] imageRecuperee = ByteStreams.toByteArray(fichier.getFichier().getInputStream());
        byte[] encodedImageRecuperee = java.util.Base64.getEncoder().encode(imageRecuperee);
        MediaType typeFichier = MediaType.parseMediaType(fichier.getExtension());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + "produit." + fichier.getExtension().split("/")[1] +"\"")
                .contentType(typeFichier)
                .contentLength(encodedImageRecuperee.length)
                .body(encodedImageRecuperee);

    }

    @PreAuthorize("hasRole('ROLE_ARTISAN')")
    @PostMapping(value = "/creer", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = { "application/json" })
    public ResponseEntity<Produit> creerProduit(@Valid @RequestPart(value = "produit") ProduitArtisanDto produitDto,
                                                @RequestPart(value = "image", required = false) MultipartFile image) throws ArtisanExistePasException, ReferenceProduitExisteDejaException, IOException, SQLException, UserBloqueException {

        Produit produit = produitService.creerProduit(produitDto, image);

        return new ResponseEntity<>(produit, HttpStatus.CREATED);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/modifierAdmin", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = { "application/json" })
    public ResponseEntity<Produit> modifierProduit(@Valid @RequestPart(value = "produit") ProduitDto produitDto,
                                                   @RequestPart(value = "image", required = false) MultipartFile image) throws ProduitInexistantException, IOException, SQLException, UserBloqueException {

        Produit produit = produitService.modifierProduit(produitDto, image);

        return ResponseEntity.ok().body(produit);

    }

    @PreAuthorize("hasRole('ROLE_ARTISAN')")
    @PutMapping(value = "/modifierProduitParArtisan", consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = { "application/json" })
    public ResponseEntity<Produit> modifierProduitParArtisan(@Valid @RequestPart(value = "produit") ProduitArtisanDto produitDto,
                                                             @RequestPart(value = "image", required = false) MultipartFile image) throws ProduitInexistantException, IOException, SQLException, UserBloqueException {

        Produit produit = produitService.modifierProduitParArtisan(produitDto, image);

        return ResponseEntity.ok().body(produit);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN') || hasRole('ROLE_ARTISAN')")
    @DeleteMapping(value = "/supprimerProduit", produces = { "application/json" })
    public ResponseEntity<MessageDto> supprimerProduit(@ApiIgnore Principal moi,
                                                       @Valid @RequestBody ProduitSuppressionDto suppressionDto) throws ReferenceIncorrecteSuppressionException, UserNonTrouveException, ArtisanNonAutoriseProduitException, UserBloqueException {

        String response = produitService.supprimerProduit(moi, suppressionDto);

        return ResponseEntity.ok().body(new MessageDto(response));

    }

    @GetMapping(value = "/detailsProduit/{refProduit}", produces = { "application/json" })
    public ResponseEntity<ProduitView> recupererDetailsProduit(@PathVariable(value = "refProduit") String reference) throws ProduitInexistantException {

        ProduitView produitView = produitService.getDetailsProduit(reference);

        return ResponseEntity.ok().body(produitView);
    }

}
