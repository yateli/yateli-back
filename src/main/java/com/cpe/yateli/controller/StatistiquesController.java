package com.cpe.yateli.controller;

import com.cpe.yateli.exception.excel.AucuneDonneeExcelException;
import com.cpe.yateli.exception.excel.ErreurGenerationException;
import com.cpe.yateli.exception.tag.TagExisteDejaException;
import com.cpe.yateli.exception.tag.TagExistePasException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.InfoFichier;
import com.cpe.yateli.model.Tag;
import com.cpe.yateli.model.dto.MessageDto;
import com.cpe.yateli.service.StatistiquesService;
import com.cpe.yateli.service.TagService;
import com.google.common.io.ByteStreams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/statistiques")
public class StatistiquesController {

    @Autowired
    private StatistiquesService statistiquesService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/genererExcelReponses", produces = { "application/json" })
    public @ResponseBody ResponseEntity<byte[]> genererExcelReponses(@ApiIgnore Principal moi,
                                                                     @RequestParam(value = "nameFile") String nomFichier,
                                                                     @RequestParam(value = "minimumDate") Long timestampMinimum,
                                                                     @RequestParam(value = "login", required = false) String login) throws IOException, UserNonTrouveException, ErreurGenerationException, AucuneDonneeExcelException {

        InfoFichier fichier = statistiquesService.genererExcel(moi, nomFichier, timestampMinimum, login);

        if(fichier == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        byte[] fichierGenere = ByteStreams.toByteArray(fichier.getFichier().getInputStream());
        byte[] encodedFichierGenere = java.util.Base64.getEncoder().encode(fichierGenere);
        MediaType typeFichier = MediaType.parseMediaType(fichier.getExtension());

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + nomFichier + ".xlsx" +"\"")
                .contentType(typeFichier)
                .contentLength(encodedFichierGenere.length)
                .body(encodedFichierGenere);

    }

}
