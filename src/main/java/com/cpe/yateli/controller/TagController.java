package com.cpe.yateli.controller;

import com.cpe.yateli.exception.tag.TagExisteDejaException;
import com.cpe.yateli.exception.tag.TagExistePasException;
import com.cpe.yateli.model.Tag;
import com.cpe.yateli.model.dto.MessageDto;
import com.cpe.yateli.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tag")
public class TagController {

    @Autowired
    private TagService tagService;

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/creer", produces = { "application/json" })
    public ResponseEntity<Tag> creerTag(@RequestParam(value = "nom") String nom) throws TagExisteDejaException {

        Tag tag = tagService.creerTag(nom);

        return ResponseEntity.ok().body(tag);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping(value = "/modifier", produces = { "application/json" })
    public ResponseEntity<Tag> modifierTag(@RequestParam(value = "id") int id,
                                           @RequestParam(value = "nouveauNom") String nouveauNom) throws TagExistePasException {

        Tag tag = tagService.modifierTag(id, nouveauNom);

        return ResponseEntity.ok().body(tag);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/supprimer", produces = { "application/json" })
    public ResponseEntity<MessageDto> supprimerTag(@RequestParam(value = "id") int id) throws TagExistePasException {

        String response = tagService.supprimerTag(id);

        return ResponseEntity.ok().body(new MessageDto(response));

    }

    @GetMapping(value = "/recupererTousTags", produces = { "application/json" })
    public ResponseEntity<List<Tag>> recupereTousTags(){

        List<Tag> tags = tagService.getTousLesTags();

        return ResponseEntity.ok().body(tags);

    }

}
