package com.cpe.yateli.controller;

import com.cpe.yateli.exception.role.RoleExistePasException;
import com.cpe.yateli.exception.user.*;
import com.cpe.yateli.model.User;
import com.cpe.yateli.model.dto.MessageDto;
import com.cpe.yateli.model.dto.UserDto;
import com.cpe.yateli.model.dto.UserModificationDto;
import com.cpe.yateli.model.view.UserView;
import com.cpe.yateli.service.UserService;
import com.cpe.yateli.service.usersConnectes.UserConnecte;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/user")
@Api(value = "/api/user", description = "Actions concernant les utilisateurs")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "/creer", produces = { "application/json" })
    public ResponseEntity<User> creerUser(@Valid @RequestBody UserDto user) throws UserExisteDejaException, PhoneFormatIncorrectException, MailNonCorrectException, FormatMotDePasseIncorrecteException {

        User nouveauUser = userService.creerUser(user);

        if(nouveauUser == null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>(nouveauUser,HttpStatus.CREATED);

    }

    @PostMapping(value = "/activerCompte", produces = { "application/json" })
    public ResponseEntity<User> activerCompte(@RequestParam("login") String login,
                                              @RequestParam("password") String password) throws UserNonTrouveException, MotDePasseIncorrectException, UserDejaActiveException {

        User nouveauUser = userService.activerUser(login,password);

        if(nouveauUser == null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return ResponseEntity.ok().body(nouveauUser);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/desactiverCompte", produces = { "application/json" })
    public ResponseEntity<MessageDto> desactiverCompte(@RequestParam("login") String login) throws UserNonTrouveException {

        String response = userService.desactiverUser(login);

        if(response == null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return ResponseEntity.ok().body(new MessageDto(response));

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/reactiverCompte", produces = { "application/json" })
    public ResponseEntity<MessageDto> reactiverCompte(@RequestParam("login") String login) throws UserNonTrouveException {

        String response = userService.reactiverUser(login);

        if(response == null){
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        return ResponseEntity.ok().body(new MessageDto(response));

    }

    @PostMapping(value = "/motDePasseOublie", produces = { "application/json" })
    public ResponseEntity<MessageDto> motDePasseOublie(@RequestParam(value = "login") String login) throws UserNonTrouveException, UserBloqueException {

        String userARenouvele = userService.motDePasseOublie(login);

        return ResponseEntity.ok().body(new MessageDto(userARenouvele));

    }

    @PostMapping(value = "/verificationCode", produces = { "application/json" })
    public ResponseEntity<MessageDto> verificationCode(@RequestParam(value = "login") String login,
                                                       @RequestParam(value = "code") String code) throws UserNonTrouveException, CodeVerificationIncorrectException, UserBloqueException {

        String userARenouvele = userService.verificationCodeRecuperation(login,code);

        return ResponseEntity.ok().body(new MessageDto(userARenouvele));

    }

    @PostMapping(value = "/nouveauPasswordAvecCode", produces = { "application/json" })
    public ResponseEntity<MessageDto> nouveauPasswordAvecCode(@RequestParam(value = "code") String code,
                                                              @RequestParam(value = "nouveauPassword") String nouveauPassword) throws UserNonTrouveException, UserBloqueException {

        String response = userService.nouveauPasswordAvecCode(code, nouveauPassword);

        return ResponseEntity.ok().body(new MessageDto(response));

    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ADMIN') || hasRole('ROLE_ARTISAN')")
    @PutMapping(value = "/modifierUser", produces = { "application/json" })
    public ResponseEntity<User> modifierUser(@ApiIgnore Principal moi,
                                             @Valid @RequestBody UserModificationDto userDto) throws UserNonTrouveException, UserBloqueException {

        User userModifie = userService.modifierUser(moi, userDto);

        if(userModifie == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok().body(userModifie);

    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ADMIN') || hasRole('ROLE_ARTISAN')")
    @DeleteMapping(value = "/supprimerUser", produces = { "application/json" })
    public ResponseEntity<MessageDto> supprimerUser(@ApiIgnore Principal moi) throws UserNonTrouveException, SuppressionUserProblemeException, UserBloqueException {

        String response = userService.supprimerUser(moi);

        if(response == null || !response.equals("OK")){
            throw SuppressionUserProblemeException.creerAvec(HttpStatus.INTERNAL_SERVER_ERROR,moi.getName());
        }

        return ResponseEntity.ok().body(new MessageDto(response));

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/supprimerSpecificUser", produces = { "application/json" })
    public ResponseEntity<MessageDto> supprimerSpecificUser(@ApiIgnore Principal moi,
                                                            @RequestParam(value = "login") String loginUser) throws UserNonTrouveException, SuppressionUserProblemeException {

        String response = userService.supprimerSpecificUser(moi,loginUser);

        if(response == null || !response.equals("OK")){
            throw SuppressionUserProblemeException.creerAvec(HttpStatus.INTERNAL_SERVER_ERROR,moi.getName());
        }

        return ResponseEntity.ok().body(new MessageDto(response));
    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ADMIN') || hasRole('ROLE_ARTISAN')")
    @GetMapping(value = "/getUser", produces = { "application/json" })
    public ResponseEntity<UserView> getUser(@ApiIgnore Principal moi) throws UserNonTrouveException, UserBloqueException {

        UserView userConnecte = userService.getUser(moi);

        return ResponseEntity.ok().body(userConnecte);

    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ADMIN') || hasRole('ROLE_ARTISAN')")
    @GetMapping(value = "/getTokenWebsocket", produces = { "application/json" })
    public ResponseEntity<MessageDto> getTokenWebsocket(@ApiIgnore Principal moi) throws UserNonTrouveException, UserBloqueException {

        String token = userService.getTokenWebsocket(moi);

        if(token == null || token.isEmpty()){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok().body(new MessageDto(token));

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/getUsersConnectesSelonRole/{role}", produces = { "application/json" })
    public ResponseEntity<List<UserConnecte>> getUsersConnectesSelonRole(@ApiIgnore Principal moi,
                                                        @PathVariable(value = "role") String role) throws UserNonTrouveException {

        List<UserConnecte> usersConnectes = userService.getConnectedUsers(moi,role);

        if(usersConnectes == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok().body(usersConnectes);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(value = "/logoutSpecificUser", produces = { "application/json" })
    public ResponseEntity<MessageDto> logoutSpecificUser(@RequestParam(value = "login") String login,
                                                         @RequestParam(value = "raison") String raison) throws UserNonTrouveException, UserNonConnecteException {

        String response = userService.logoutSpecificUser(login, raison);

        if(response == null || response.isEmpty()){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return ResponseEntity.ok().body(new MessageDto(response));

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/getAllUsers", produces = { "application/json" })
    public ResponseEntity<List<User>> getAllUsers(){

        List<User> users = userService.getAllUsers();

        return ResponseEntity.ok().body(users);

    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/getUsersSelonRole/{role}", produces = { "application/json" })
    public ResponseEntity<List<User>> getUsersSelonRole(@PathVariable(value = "role") String role) throws RoleExistePasException {

        List<User> users = userService.getUsersSelonRole(role);

        return ResponseEntity.ok().body(users);

    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @PutMapping(value = "/abonnerUser", produces = { "application/json" })
    public ResponseEntity<User> abonnerUser(@ApiIgnore Principal moi) throws UserNonTrouveException, EstDejaAbonneException, UserBloqueException {

        User userAbonne = userService.abonnerUser(moi);

        return ResponseEntity.ok().body(userAbonne);

    }

    @PreAuthorize("hasRole('ROLE_USER') || hasRole('ROLE_ARTISAN')")
    @PutMapping(value = "/desabonnerUser", produces = { "application/json" })
    public ResponseEntity<User> desabonnerUser(@ApiIgnore Principal moi) throws UserNonTrouveException, UserBloqueException {

        User userDesabonne = userService.desabonnerUser(moi);

        return ResponseEntity.ok().body(userDesabonne);

    }


}
