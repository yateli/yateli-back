package com.cpe.yateli.exception;

public enum ErreurMessages {

    USER_EXISTE_DEJA("Un utilisateur existe déjà avec l'adresse mail %s"),
    USER_EXISTE_PAS("L'utilisateur %s n'existe pas"),
    MAIL_NON_CORRECTE("L'adresse %s ne respecte le format des adresses mail"),
    FORMAT_MOT_DE_PASSE_NON_CORRECTE("Le format du mot de passe renseigné est incorrecte"),
    PRODUIT_EXISTE_PAS("Le produit de référence %s n'existe pas"),
    PRODUIT_SANS_IMAGE("Le produit de référence %s n'a pas d'image"),
    AUCUN_PRODUIT_A_BLOQUER("Aucun produit à supprimer parmi les références renseignées"),
    LIVRAISON_EXISTE_PAS("Cette livraison n'existe pas"),
    ADRESSE_LIVRAISON_NULL_VIDE("L'adresse choisie n'est pas correcte"),
    FORMAT_CODE_POSTAL_NON_CORRECT("Le code postal %s ne respecte pas le format désiré"),
    COMMANDE_EXISTE_PAS("La commande d'ID %s n'existe pas"),
    ROLE_EXISTE_PAS("Le role %s n'existe pas"),
    USER_DEJA_ABONNE("Vous êtes déjà abonné"),
    COMMANDE_INFERIEUR_0("Pour pouvoir utiliser l'ensemble de ces réductions, vous devez ajouter %s€ à votre commande"),
    BON_ACHAT_NULL_OU_DEJA_UTILISE("Le bon d'achat %s n'existe pas ou a déjà été utilisé"),
    USER_NON_ABONNE("Le login %s n'est pas abonné au colis mystère"),
    PASSWORD_NOT_CORRECT("Mot de passe incorrect"),
    PDF_FACTURE_NULL("La facture ne s'est pas générée correctement"),
    POIDS_PANIER_0("Le poids de votre panier ne peut pas être de 0"),
    FACTURE_EXISTE_PAS("La facture %s appartenant au login %s n'existe pas"),
    CODE_VERIFICATION_INCORRECT("Le code de vérification saisi (%s) est incorrect"),
    SUPPRESSION_USER_PROBLEME("L'utilisateur %s ne s'est pas supprimé correctement"),
    PHRASE_DU_JOUR_EXISTE_PAS("Il n'y a pas de phrase du jour"),
    ARTISAN_EXISTE_PAS("L'artisan %s n'existe pas"),
    PRODUIT_QUANTITE_PANIER_EXISTE_PAS("Produit Quantite Panier n'existe pas pour le produit %s et le panier de %s"),
    HISTORIQUE_USER_EXISTE_PAS("L'historique de l'utilisateur %s n'existe pas"),
    COMMANDE_DEJA_PAYEE("La commande %s a déjà été payée"),
    PRODUIT_EXISTE_DEJA("Un produit de référence %s existe déjà"),
    REFERENCES_INCORRECTE_SUPPRESSION("Les produits de référence %s n'existent pas et n'ont pas été supprimés. Les autres produits ont été supprimés."),
    ARTISAN_NON_AUTORISES_REFERENCES("Les produits de référence %s ne peuvent pas être supprimés par %s"),
    QUANTITE_INCORRECTE("La quantité %s est incorrecte. Elle doit être supérieure à 0"),
    PRODUIT_DEJA_PRESENT_PANIER("Le produit de référence %s est déjà présent dans votre panier"),
    CONFIRM_PASSWORD_NON_EGAL("Les deux mots de passe ne correspondent pas"),
    USER_DEJA_ACTIVE("L'utilisateur %s est déjà activé"),
    TAG_EXISTE_DEJA("Le tag %s existe déjà"),
    TAG_EXISTE_PAS("Le tag %s n'existe pas"),
    PHONE_FORMAT_INCORRECT("Le format du numéro de téléphone %s est incorrect. Il devrait commencer par un +"),
    JEU_CONCOURS_EXISTE_DEJA("Le jeu concours %s existe déjà"),
    JEU_CONCOURS_EXISTE_PAS("Le jeu concours %s n'existe pas"),
    JEU_CONCOURS_FERME("Le jeu concours %s est fermé. Il était ouvert du %s au %s"),
    JEU_CONCOURS_DEJA_OUVERT_FERME("Le jeu concours %s est déjà %s"),
    POINT_FIDELITE_INSUFFISANT_CONCOURS("Points de fidélité insuffisant pour participer au concours %s. Vous avez %s points, il vous fallait %s points"),
    JEU_CONCOURS_PARTICIPE_PAS("L'utilisateur %s ne participe pas au concours %s pour l'instant"),
    JEU_CONCOURS_DEJA_INSCRIT("L'utilisateur %s est déjà inscrit au concours %s"),
    JEU_CONCOURS_FERME_EXCEPTIONNELEMENT("Le jeu concours %s a été exceptionnelement fermé par un administrateur"),
    STOCK_PRODUIT_INSUFFISANT("Impossible d'ajouter %s produit(s) car la quantité en stock est de %s"),
    STOCK_PRODUIT_POUR_COMMANDE_INSUFFISANT("Malheureusement, il est désormais impossible de commander %s %s car il n'en reste plus que %s en stock"),
    ARTISAN_SANS_IMAGE("L'artisan %s n'a pas d'image"),
    QUESTION_EXISTE_DEJA("La question \"%s\" existe déjà. Son ID est %s."),
    QUESTION_EXISTE_PAS("La question d'id \"%s\" n'existe pas."),
    QUESTIONS_ACTIVES_LIMITE("Erreur de limites des questions actives."),
    QUESTION_DEJA_ACTIVE("La question d'id \"%s\" est déjà active."),
    QUESTION_PAS_ACTIVE("La question d'id \"%s\" n'est pas active."),
    ARTISAN_NON_BLOQUE("L'artisan %s n'est pas bloqué"),
    USER_NON_CONNECTE("L'utilisateur %s n'est pas connecté"),
    ALERTE_EXISTE_PAS("L'alerte %s n'existe pas"),
    USER_BLOQUE("Vous avez été bloqué"),
    ARTISAN_EN_COURS_AUTORISATION("L'artisan \"%s\" est déjà en cours d'autorisation"),
    ARTISAN_DEJA_AUTORISE("L'artisan \"%s\" a déjà été autorisé par un administrateur"),
    GENERATION_EXCEL_ERREUR("L'application a rencontré un problème lors de la génération du fichier Excel"),
    AUCUNE_DONNEE_EXCEL("Il n'y a aucune donnée Excel à afficher concernant cette période et/ou cet utilisateur");

    private String message;

    ErreurMessages(String texte){
        this.message = texte;
    }

    public String getMessage(String... parametres){
        return String.format(this.message, parametres);
    }

    @Override
    public String toString(){
        return this.message;
    }

}
