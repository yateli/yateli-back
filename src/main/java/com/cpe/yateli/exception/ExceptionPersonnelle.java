package com.cpe.yateli.exception;

import org.springframework.http.HttpStatus;

public abstract class ExceptionPersonnelle extends Exception {

    private HttpStatus httpStatus;

    protected ExceptionPersonnelle(HttpStatus httpStatus){
        this.httpStatus = httpStatus;
    }

    protected HttpStatus getHttpStatus(){
        return this.httpStatus;
    }

    public abstract String getMessage();

}
