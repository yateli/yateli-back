package com.cpe.yateli.exception;

import com.cpe.yateli.model.ApiError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ApiError> handleException(Exception ex){
        HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;

        if(ex instanceof ExceptionPersonnelle){
            httpStatus = ((ExceptionPersonnelle) ex).getHttpStatus();
        }

        String logCreated = ex.getClass().getSimpleName() + " : " + ex.getMessage();
        log.error(logCreated);
        return new ResponseEntity<>(new ApiError(httpStatus.value(), ex.getClass().getSimpleName(), ex.getMessage()), httpStatus);
    }

}
