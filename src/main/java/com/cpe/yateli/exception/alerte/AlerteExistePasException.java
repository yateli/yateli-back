package com.cpe.yateli.exception.alerte;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class AlerteExistePasException extends ExceptionPersonnelle {
    private String id;

    public static AlerteExistePasException creerAvec(HttpStatus httpStatus, String id){
        return new AlerteExistePasException(httpStatus,id);
    }

    private AlerteExistePasException(HttpStatus httpStatus, String id) {
        super(httpStatus);
        this.id = id;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.ALERTE_EXISTE_PAS.getMessage(this.id);
    }
}
