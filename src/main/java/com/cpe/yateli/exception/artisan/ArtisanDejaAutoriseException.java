package com.cpe.yateli.exception.artisan;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class ArtisanDejaAutoriseException extends ExceptionPersonnelle {
    private String login;

    public static ArtisanDejaAutoriseException creerAvec(HttpStatus httpStatus, String login){
        return new ArtisanDejaAutoriseException(httpStatus, login);
    }

    private ArtisanDejaAutoriseException(HttpStatus httpStatus, String login) {
        super(httpStatus);
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.ARTISAN_DEJA_AUTORISE.getMessage(this.login);
    }
}
