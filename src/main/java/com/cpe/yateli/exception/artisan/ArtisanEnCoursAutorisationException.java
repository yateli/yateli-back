package com.cpe.yateli.exception.artisan;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class ArtisanEnCoursAutorisationException extends ExceptionPersonnelle {
    private String login;

    public static ArtisanEnCoursAutorisationException creerAvec(HttpStatus httpStatus, String login){
        return new ArtisanEnCoursAutorisationException(httpStatus, login);
    }

    private ArtisanEnCoursAutorisationException(HttpStatus httpStatus, String login) {
        super(httpStatus);
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.ARTISAN_EN_COURS_AUTORISATION.getMessage(this.login);
    }
}
