package com.cpe.yateli.exception.artisan;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class ArtisanExistePasException extends ExceptionPersonnelle {
    private String login;

    public static ArtisanExistePasException creerAvec(HttpStatus httpStatus, String login){
        return new ArtisanExistePasException(httpStatus, login);
    }

    private ArtisanExistePasException(HttpStatus httpStatus, String login) {
        super(httpStatus);
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.ARTISAN_EXISTE_PAS.getMessage(this.login);
    }
}
