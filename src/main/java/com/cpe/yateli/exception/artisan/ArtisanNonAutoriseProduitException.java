package com.cpe.yateli.exception.artisan;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

import java.util.List;

public class ArtisanNonAutoriseProduitException extends ExceptionPersonnelle {
    private String artisanNonAutorise;
    private List<String> referencesNonAutorises;

    public static ArtisanNonAutoriseProduitException creerAvec(HttpStatus httpStatus, String artisanNonAutorise, List<String> referencesProduit){
        return new ArtisanNonAutoriseProduitException(httpStatus, artisanNonAutorise, referencesProduit);
    }

    private ArtisanNonAutoriseProduitException(HttpStatus httpStatus, String artisanNonAutorise, List<String> referencesProduit) {
        super(httpStatus);
        this.artisanNonAutorise = artisanNonAutorise;
        this.referencesNonAutorises = referencesProduit;
    }

    @Override
    public String getMessage() {
        StringBuilder stringBuilder = new StringBuilder();
        for(String reference : this.referencesNonAutorises){
            stringBuilder.append(reference);
            stringBuilder.append(",");
        }
        return ErreurMessages.ARTISAN_NON_AUTORISES_REFERENCES.getMessage(stringBuilder.toString(), this.artisanNonAutorise);
    }
}
