package com.cpe.yateli.exception.artisan;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class ArtisanNonBloqueException extends ExceptionPersonnelle {
    private String login;

    public static ArtisanNonBloqueException creerAvec(HttpStatus httpStatus, String login){
        return new ArtisanNonBloqueException(httpStatus, login);
    }

    private ArtisanNonBloqueException(HttpStatus httpStatus, String login) {
        super(httpStatus);
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.ARTISAN_NON_BLOQUE.getMessage(this.login);
    }
}
