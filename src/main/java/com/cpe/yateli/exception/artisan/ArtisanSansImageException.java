package com.cpe.yateli.exception.artisan;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class ArtisanSansImageException extends ExceptionPersonnelle {
    private String login;

    public static ArtisanSansImageException creerAvec(HttpStatus httpStatus, String login){
        return new ArtisanSansImageException(httpStatus, login);
    }

    private ArtisanSansImageException(HttpStatus httpStatus, String login) {
        super(httpStatus);
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.ARTISAN_SANS_IMAGE.getMessage(this.login);
    }
}
