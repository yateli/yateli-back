package com.cpe.yateli.exception.bonAchat;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class BonAchatNullOuDejaUtiliseException extends ExceptionPersonnelle {
    private String codeBonAchat;

    public static BonAchatNullOuDejaUtiliseException creerAvec(HttpStatus httpStatus,String codeBonAchat){
        return new BonAchatNullOuDejaUtiliseException(httpStatus,codeBonAchat);
    }

    private BonAchatNullOuDejaUtiliseException(HttpStatus httpStatus,String codeBonAchat){
        super(httpStatus);
        this.codeBonAchat = codeBonAchat;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.BON_ACHAT_NULL_OU_DEJA_UTILISE.getMessage(this.codeBonAchat);
    }
}
