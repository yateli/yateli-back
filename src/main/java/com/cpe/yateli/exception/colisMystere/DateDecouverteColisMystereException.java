package com.cpe.yateli.exception.colisMystere;

import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class DateDecouverteColisMystereException extends ExceptionPersonnelle {
    private String dateDecouverte;

    public static DateDecouverteColisMystereException creerAvec(HttpStatus httpStatus,String dateDecouverte){
        return new DateDecouverteColisMystereException(httpStatus,dateDecouverte);
    }

    private DateDecouverteColisMystereException(HttpStatus httpStatus,String dateDecouverte) {
        super(httpStatus);
        this.dateDecouverte = dateDecouverte;
    }

    @Override
    public String getMessage() {
        return this.dateDecouverte;
    }
}
