package com.cpe.yateli.exception.commande;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class AddressNullException extends ExceptionPersonnelle {

    public static AddressNullException creerAvec(HttpStatus httpStatus){
        return new AddressNullException(httpStatus);
    }

    private AddressNullException(HttpStatus httpStatus) {
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return ErreurMessages.ADRESSE_LIVRAISON_NULL_VIDE.toString();
    }
}
