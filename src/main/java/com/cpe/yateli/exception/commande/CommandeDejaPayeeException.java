package com.cpe.yateli.exception.commande;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class CommandeDejaPayeeException extends ExceptionPersonnelle {
    private int idCommande;

    public static CommandeDejaPayeeException creerAvec(HttpStatus httpStatus, int idCommande){
        return new CommandeDejaPayeeException(httpStatus, idCommande);
    }

    private CommandeDejaPayeeException(HttpStatus httpStatus, int idCommande) {
        super(httpStatus);
        this.idCommande = idCommande;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.COMMANDE_DEJA_PAYEE.getMessage(String.valueOf(this.idCommande));
    }
}
