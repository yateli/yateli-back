package com.cpe.yateli.exception.commande;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

import java.math.BigDecimal;

public class CommandeInferieur0Exception extends ExceptionPersonnelle {
    private Double montantAPayer;

    public static CommandeInferieur0Exception creerAvec(HttpStatus httpStatus, Double montantAPayer){
        return new CommandeInferieur0Exception(httpStatus,montantAPayer);
    }

    private CommandeInferieur0Exception(HttpStatus httpStatus,Double montantAPayer) {
        super(httpStatus);
        this.montantAPayer = montantAPayer;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.COMMANDE_INFERIEUR_0.getMessage(new BigDecimal(this.montantAPayer).setScale(0,BigDecimal.ROUND_UP).toString());
    }
}
