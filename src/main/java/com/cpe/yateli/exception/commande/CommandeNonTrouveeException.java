package com.cpe.yateli.exception.commande;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class CommandeNonTrouveeException extends ExceptionPersonnelle {
    private String idCommande;

    public static CommandeNonTrouveeException creerAvec(HttpStatus httpStatus,String idCommande){
        return new CommandeNonTrouveeException(httpStatus,idCommande);
    }

    private CommandeNonTrouveeException(HttpStatus httpStatus, String idCommande) {
        super(httpStatus);
        this.idCommande = idCommande;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.COMMANDE_EXISTE_PAS.getMessage(this.idCommande);
    }
}
