package com.cpe.yateli.exception.commande;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class FormatCodePostalNonCorrectException extends ExceptionPersonnelle {
    private String codePostal;

    public static FormatCodePostalNonCorrectException creerAvec(HttpStatus httpStatus, String codePostal){
        return new FormatCodePostalNonCorrectException(httpStatus,codePostal);
    }

    private FormatCodePostalNonCorrectException(HttpStatus httpStatus,String codePostal) {
        super(httpStatus);
        this.codePostal = codePostal;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.FORMAT_CODE_POSTAL_NON_CORRECT.getMessage(this.codePostal);
    }
}
