package com.cpe.yateli.exception.commande;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class NonLivraisonException extends ExceptionPersonnelle {

    public static NonLivraisonException creerAvec(HttpStatus httpStatus){
        return new NonLivraisonException(httpStatus);
    }

    private NonLivraisonException(HttpStatus httpStatus) {
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return ErreurMessages.LIVRAISON_EXISTE_PAS.toString();
    }

}
