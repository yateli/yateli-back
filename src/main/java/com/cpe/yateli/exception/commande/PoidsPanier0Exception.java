package com.cpe.yateli.exception.commande;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class PoidsPanier0Exception extends ExceptionPersonnelle {

    public static PoidsPanier0Exception creerAvec(HttpStatus httpStatus){
        return new PoidsPanier0Exception(httpStatus);
    }

    private PoidsPanier0Exception(HttpStatus httpStatus) {
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return ErreurMessages.POIDS_PANIER_0.toString();
    }

}
