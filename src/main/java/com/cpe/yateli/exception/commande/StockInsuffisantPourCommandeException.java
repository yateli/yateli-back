package com.cpe.yateli.exception.commande;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class StockInsuffisantPourCommandeException extends ExceptionPersonnelle {
    private int stockProduit, quantiteCommandee;
    private String nomProduit;

    public static StockInsuffisantPourCommandeException creerAvec(HttpStatus httpStatus, String nomProduit, int stockProduit, int quantiteCommandee){
        return new StockInsuffisantPourCommandeException(httpStatus, nomProduit, stockProduit, quantiteCommandee);
    }

    private StockInsuffisantPourCommandeException(HttpStatus httpStatus, String nomProduit, int stockProduit, int quantiteCommandee) {
        super(httpStatus);
        this.nomProduit = nomProduit;
        this.stockProduit = stockProduit;
        this.quantiteCommandee = quantiteCommandee;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.STOCK_PRODUIT_POUR_COMMANDE_INSUFFISANT.getMessage(String.valueOf(this.quantiteCommandee), this.nomProduit, String.valueOf(this.stockProduit));
    }
}
