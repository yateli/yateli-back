package com.cpe.yateli.exception.excel;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class AucuneDonneeExcelException extends ExceptionPersonnelle {
    public static AucuneDonneeExcelException creerAvec(HttpStatus httpStatus){
        return new AucuneDonneeExcelException(httpStatus);
    }

    private AucuneDonneeExcelException(HttpStatus httpStatus) {
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return ErreurMessages.AUCUNE_DONNEE_EXCEL.toString();
    }
}
