package com.cpe.yateli.exception.excel;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class ErreurGenerationException extends ExceptionPersonnelle {
    public static ErreurGenerationException creerAvec(HttpStatus httpStatus){
        return new ErreurGenerationException(httpStatus);
    }

    private ErreurGenerationException(HttpStatus httpStatus) {
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return ErreurMessages.GENERATION_EXCEL_ERREUR.toString();
    }
}
