package com.cpe.yateli.exception.facture;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class FactureExistePasException extends ExceptionPersonnelle {
    private String idFacture;
    private String login;

    public static FactureExistePasException creerAvec(HttpStatus httpStatus, String idFacture, String login){
        return new FactureExistePasException(httpStatus,idFacture,login);
    }

    private FactureExistePasException(HttpStatus httpStatus, String idFacture, String login) {
        super(httpStatus);
        this.idFacture = idFacture;
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.FACTURE_EXISTE_PAS.getMessage(this.idFacture,this.login);
    }
}
