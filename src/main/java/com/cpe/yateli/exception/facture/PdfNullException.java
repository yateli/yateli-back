package com.cpe.yateli.exception.facture;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class PdfNullException extends ExceptionPersonnelle {
    private String login;

    public static PdfNullException creerAvec(HttpStatus httpStatus, String login){
        return new PdfNullException(httpStatus,login);
    }

    private PdfNullException(HttpStatus httpStatus, String login) {
        super(httpStatus);
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.PDF_FACTURE_NULL.toString();
    }
}
