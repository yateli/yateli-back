package com.cpe.yateli.exception.interrogation;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class QuestionDejaActiveException extends ExceptionPersonnelle {

    private Integer id;

    public static QuestionDejaActiveException creerAvec(HttpStatus httpStatus, Integer id){
        return new QuestionDejaActiveException(httpStatus,id);
    }

    protected QuestionDejaActiveException(HttpStatus httpStatus, Integer id) {
        super(httpStatus);
        this.id = id;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.QUESTION_DEJA_ACTIVE.getMessage(String.valueOf(this.id));
    }
}
