package com.cpe.yateli.exception.interrogation;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class QuestionExisteDejaException extends ExceptionPersonnelle {

    private String question;
    private int id;

    public static QuestionExisteDejaException creerAvec(HttpStatus httpStatus, int id, String question){
        return new QuestionExisteDejaException(httpStatus,id,question);
    }

    QuestionExisteDejaException(HttpStatus httpStatus, int id, String question) {
        super(httpStatus);
        this.id = id;
        this.question = question;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.QUESTION_EXISTE_DEJA.getMessage(this.question, String.valueOf(this.id));
    }

}
