package com.cpe.yateli.exception.interrogation;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class QuestionExistePasException extends ExceptionPersonnelle {

    private Integer id;

    public static QuestionExistePasException creerAvec(HttpStatus httpStatus, Integer id){
        return new QuestionExistePasException(httpStatus,id);
    }

    protected QuestionExistePasException(HttpStatus httpStatus, Integer id) {
        super(httpStatus);
        this.id = id;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.QUESTION_EXISTE_PAS.getMessage(String.valueOf(this.id));
    }
}
