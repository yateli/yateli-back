package com.cpe.yateli.exception.interrogation;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class QuestionPasActiveException extends ExceptionPersonnelle {
    private Integer id;

    public static QuestionPasActiveException creerAvec(HttpStatus httpStatus, Integer id){
        return new QuestionPasActiveException(httpStatus,id);
    }

    protected QuestionPasActiveException(HttpStatus httpStatus, Integer id) {
        super(httpStatus);
        this.id = id;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.QUESTION_PAS_ACTIVE.getMessage(String.valueOf(this.id));
    }
}
