package com.cpe.yateli.exception.interrogation;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class QuestionsActivesException extends ExceptionPersonnelle {

    protected QuestionsActivesException(HttpStatus httpStatus) {
        super(httpStatus);
    }

    public static QuestionsActivesException creerAvec(HttpStatus httpStatus){
        return new QuestionsActivesException(httpStatus);
    }

    @Override
    public String getMessage() {
        return ErreurMessages.QUESTIONS_ACTIVES_LIMITE.getMessage();
    }
}
