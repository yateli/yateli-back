package com.cpe.yateli.exception.jeuConcours;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class JeuConcoursDejaInscritException extends ExceptionPersonnelle {
    private String nomConcours, login;

    public static JeuConcoursDejaInscritException creerAvec(HttpStatus httpStatus, String nomConcours, String login){
        return new JeuConcoursDejaInscritException(httpStatus, nomConcours, login);
    }

    protected JeuConcoursDejaInscritException(HttpStatus httpStatus, String nomConcours, String login) {
        super(httpStatus);
        this.nomConcours = nomConcours;
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.JEU_CONCOURS_DEJA_INSCRIT.getMessage(this.login, this.nomConcours);
    }
}
