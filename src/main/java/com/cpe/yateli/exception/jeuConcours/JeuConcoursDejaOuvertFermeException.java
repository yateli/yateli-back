package com.cpe.yateli.exception.jeuConcours;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class JeuConcoursDejaOuvertFermeException extends ExceptionPersonnelle {
    private String nomConcours;
    private String etat;

    public static JeuConcoursDejaOuvertFermeException creerAvec(HttpStatus httpStatus, String nomConcours, String etat){
        return new JeuConcoursDejaOuvertFermeException(httpStatus, nomConcours, etat);
    }

    private JeuConcoursDejaOuvertFermeException(HttpStatus httpStatus, String nomConcours, String etat) {
        super(httpStatus);
        this.nomConcours = nomConcours;
        this.etat = etat;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.JEU_CONCOURS_DEJA_OUVERT_FERME.getMessage(this.nomConcours, this.etat);
    }
}
