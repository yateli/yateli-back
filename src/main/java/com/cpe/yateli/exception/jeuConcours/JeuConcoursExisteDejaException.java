package com.cpe.yateli.exception.jeuConcours;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class JeuConcoursExisteDejaException extends ExceptionPersonnelle {
    private String nomConcours;

    public static JeuConcoursExisteDejaException creerAvec(HttpStatus httpStatus, String nomConcours){
        return new JeuConcoursExisteDejaException(httpStatus, nomConcours);
    }

    protected JeuConcoursExisteDejaException(HttpStatus httpStatus, String nomConcours) {
        super(httpStatus);
        this.nomConcours = nomConcours;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.JEU_CONCOURS_EXISTE_DEJA.getMessage(this.nomConcours);
    }
}
