package com.cpe.yateli.exception.jeuConcours;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class JeuConcoursExistePasException extends ExceptionPersonnelle {
    private String nomConcours;

    public static JeuConcoursExistePasException creerAvec(HttpStatus httpStatus, String nomConcours){
        return new JeuConcoursExistePasException(httpStatus, nomConcours);
    }

    private JeuConcoursExistePasException(HttpStatus httpStatus, String nomConcours) {
        super(httpStatus);
        this.nomConcours = nomConcours;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.JEU_CONCOURS_EXISTE_PAS.getMessage(this.nomConcours);
    }
}
