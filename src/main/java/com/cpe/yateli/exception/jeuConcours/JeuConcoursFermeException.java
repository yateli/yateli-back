package com.cpe.yateli.exception.jeuConcours;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

import java.text.SimpleDateFormat;

public class JeuConcoursFermeException extends ExceptionPersonnelle {
    private String nomJeuConcours, debut, fin;

    public static JeuConcoursFermeException creerAvec(HttpStatus httpStatus, String nomJeuConcours, long timestampDebut, long timestampFin){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMMM yyyy HH:mm");
        return new JeuConcoursFermeException(httpStatus, nomJeuConcours, simpleDateFormat.format(timestampDebut), simpleDateFormat.format(timestampFin));
    }

    private JeuConcoursFermeException(HttpStatus httpStatus, String nomJeuConcours, String debut, String fin) {
        super(httpStatus);
        this.nomJeuConcours = nomJeuConcours;
        this.debut = debut;
        this.fin = fin;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.JEU_CONCOURS_FERME.getMessage(this.nomJeuConcours, this.debut, this.fin);
    }
}
