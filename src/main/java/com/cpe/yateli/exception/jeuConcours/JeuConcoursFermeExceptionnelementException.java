package com.cpe.yateli.exception.jeuConcours;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class JeuConcoursFermeExceptionnelementException extends ExceptionPersonnelle {
    private String nomConcours;

    public static JeuConcoursFermeExceptionnelementException creerAvec(HttpStatus httpStatus, String nomConcours){
        return new JeuConcoursFermeExceptionnelementException(httpStatus, nomConcours);
    }

    private JeuConcoursFermeExceptionnelementException(HttpStatus httpStatus, String nomConcours) {
        super(httpStatus);
        this.nomConcours = nomConcours;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.JEU_CONCOURS_FERME_EXCEPTIONNELEMENT.getMessage(this.nomConcours);
    }
}
