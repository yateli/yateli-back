package com.cpe.yateli.exception.jeuConcours;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class JeuConcoursParticipePasException extends ExceptionPersonnelle {
    private String nomConcours, login;

    public static JeuConcoursParticipePasException creerAvec(HttpStatus httpStatus, String nomConcours, String login){
        return new JeuConcoursParticipePasException(httpStatus, nomConcours, login);
    }

    protected JeuConcoursParticipePasException(HttpStatus httpStatus, String nomConcours, String login) {
        super(httpStatus);
        this.nomConcours = nomConcours;
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.JEU_CONCOURS_PARTICIPE_PAS.getMessage(this.login, this.nomConcours);
    }
}
