package com.cpe.yateli.exception.jeuConcours;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class PointsFideliteInsuffisantException extends ExceptionPersonnelle {
    private String nomJeuConcours;
    private int pointActuel, pointConcours;

    public static PointsFideliteInsuffisantException creerAvec(HttpStatus httpStatus, String nomJeuConcours, int pointActuel, int pointConcours){
        return new PointsFideliteInsuffisantException(httpStatus, nomJeuConcours, pointActuel, pointConcours);
    }

    private PointsFideliteInsuffisantException(HttpStatus httpStatus, String nomJeuConcours, int pointActuel, int pointConcours) {
        super(httpStatus);
        this.nomJeuConcours = nomJeuConcours;
        this.pointActuel = pointActuel;
        this.pointConcours = pointConcours;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.POINT_FIDELITE_INSUFFISANT_CONCOURS.getMessage(this.nomJeuConcours, String.valueOf(this.pointActuel), String.valueOf(this.pointConcours));
    }
}
