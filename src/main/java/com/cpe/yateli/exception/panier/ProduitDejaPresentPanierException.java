package com.cpe.yateli.exception.panier;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class ProduitDejaPresentPanierException extends ExceptionPersonnelle {
    private String reference;

    public static ProduitDejaPresentPanierException creerAvec(HttpStatus httpStatus, String referenceProduit){
        return new ProduitDejaPresentPanierException(httpStatus, referenceProduit);
    }

    private ProduitDejaPresentPanierException(HttpStatus httpStatus, String referenceProduit) {
        super(httpStatus);
        this.reference = referenceProduit;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.PRODUIT_DEJA_PRESENT_PANIER.getMessage(this.reference);
    }
}
