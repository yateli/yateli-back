package com.cpe.yateli.exception.panier;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class ProduitInexistantException extends ExceptionPersonnelle {

    private String reference;

    public static ProduitInexistantException creerAvec(HttpStatus httpStatus, String reference){
        return new ProduitInexistantException(httpStatus,reference);
    }

    ProduitInexistantException(HttpStatus httpStatus, String reference) {
        super(httpStatus);
        this.reference = reference;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.PRODUIT_EXISTE_PAS.getMessage(this.reference);
    }

}
