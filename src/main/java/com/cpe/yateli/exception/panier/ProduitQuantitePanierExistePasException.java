package com.cpe.yateli.exception.panier;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class ProduitQuantitePanierExistePasException extends ExceptionPersonnelle {
    private String produit;
    private String login;

    public static ProduitQuantitePanierExistePasException creerAvec(HttpStatus httpStatus, String produit, String login){
        return new ProduitQuantitePanierExistePasException(httpStatus, produit, login);
    }

    private ProduitQuantitePanierExistePasException(HttpStatus httpStatus, String produit, String login) {
        super(httpStatus);
        this.produit = produit;
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.PRODUIT_QUANTITE_PANIER_EXISTE_PAS.getMessage(this.produit, this.login);
    }
}
