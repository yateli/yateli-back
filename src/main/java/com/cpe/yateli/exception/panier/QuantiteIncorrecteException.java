package com.cpe.yateli.exception.panier;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class QuantiteIncorrecteException extends ExceptionPersonnelle {
    private int quantite;

    public static QuantiteIncorrecteException creerAvec(HttpStatus httpStatus, int quantite){
        return new QuantiteIncorrecteException(httpStatus, quantite);
    }

    private QuantiteIncorrecteException(HttpStatus httpStatus, int quantite) {
        super(httpStatus);
        this.quantite = quantite;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.QUANTITE_INCORRECTE.getMessage(String.valueOf(this.quantite));
    }
}
