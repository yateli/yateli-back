package com.cpe.yateli.exception.phraseDuJour;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class PhraseDuJourException extends ExceptionPersonnelle {

    public static PhraseDuJourException creerAvec(HttpStatus status){
        return new PhraseDuJourException(status);
    }

    private PhraseDuJourException(HttpStatus httpStatus) {
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return ErreurMessages.PHRASE_DU_JOUR_EXISTE_PAS.toString();
    }

}
