package com.cpe.yateli.exception.produit;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class ProduitABloquerException extends ExceptionPersonnelle {

    public static ProduitABloquerException creerAvec(HttpStatus httpStatus){
        return new ProduitABloquerException(httpStatus);
    }

    private ProduitABloquerException(HttpStatus httpStatus) {
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return ErreurMessages.AUCUN_PRODUIT_A_BLOQUER.toString();
    }

}
