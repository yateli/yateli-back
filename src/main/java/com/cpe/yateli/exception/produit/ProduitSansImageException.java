package com.cpe.yateli.exception.produit;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class ProduitSansImageException extends ExceptionPersonnelle {
    private String reference;

    public static ProduitSansImageException creerAvec(HttpStatus httpStatus, String reference){
        return new ProduitSansImageException(httpStatus, reference);
    }

    private ProduitSansImageException(HttpStatus httpStatus, String reference) {
        super(httpStatus);
        this.reference = reference;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.PRODUIT_SANS_IMAGE.getMessage(this.reference);
    }
}
