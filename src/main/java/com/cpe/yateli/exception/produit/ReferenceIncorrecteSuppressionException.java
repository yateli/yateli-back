package com.cpe.yateli.exception.produit;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

import java.util.List;

public class ReferenceIncorrecteSuppressionException extends ExceptionPersonnelle {
    private List<String> referencesIncorrectes;

    public static ReferenceIncorrecteSuppressionException creerAvec(HttpStatus httpStatus, List<String> referencesIncorrectes){
        return new ReferenceIncorrecteSuppressionException(HttpStatus.NOT_FOUND, referencesIncorrectes);
    }

   private ReferenceIncorrecteSuppressionException(HttpStatus httpStatus, List<String> referencesIncorrectes) {
        super(httpStatus);
        this.referencesIncorrectes = referencesIncorrectes;
    }

    @Override
    public String getMessage() {
        StringBuilder stringBuilder = new StringBuilder();
        for(String reference : this.referencesIncorrectes){
            stringBuilder.append(reference);
            stringBuilder.append(",");
        }
        return ErreurMessages.REFERENCES_INCORRECTE_SUPPRESSION.getMessage(stringBuilder.toString());
    }
}
