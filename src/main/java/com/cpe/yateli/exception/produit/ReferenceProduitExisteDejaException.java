package com.cpe.yateli.exception.produit;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class ReferenceProduitExisteDejaException extends ExceptionPersonnelle {
    private String reference;

    public static ReferenceProduitExisteDejaException creerAvec(HttpStatus httpStatus, String reference){
        return new ReferenceProduitExisteDejaException(httpStatus, reference);
    }

    private ReferenceProduitExisteDejaException(HttpStatus httpStatus, String reference) {
        super(httpStatus);
        this.reference = reference;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.PRODUIT_EXISTE_PAS.getMessage(this.reference);
    }
}
