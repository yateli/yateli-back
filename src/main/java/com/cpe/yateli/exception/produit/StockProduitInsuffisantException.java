package com.cpe.yateli.exception.produit;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class StockProduitInsuffisantException extends ExceptionPersonnelle {
    private int stockProduit, quantiteSouhaitee;

    public static StockProduitInsuffisantException creerAvec(HttpStatus httpStatus, int stockProduit, int quantiteSouhaitee){
        return new StockProduitInsuffisantException(httpStatus, stockProduit, quantiteSouhaitee);
    }

    private StockProduitInsuffisantException(HttpStatus httpStatus, int stockProduit, int quantiteSouhaitee) {
        super(httpStatus);
        this.stockProduit = stockProduit;
        this.quantiteSouhaitee = quantiteSouhaitee;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.STOCK_PRODUIT_INSUFFISANT.getMessage(String.valueOf(this.quantiteSouhaitee), String.valueOf(this.stockProduit));
    }
}
