package com.cpe.yateli.exception.role;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class RoleExistePasException extends ExceptionPersonnelle {
    private String role;

    public static RoleExistePasException creerAvec(HttpStatus httpStatus, String role){
        return new RoleExistePasException(httpStatus,role);
    }

    private RoleExistePasException(HttpStatus httpStatus, String role) {
        super(httpStatus);
        this.role = role;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.ROLE_EXISTE_PAS.getMessage(this.role);
    }
}
