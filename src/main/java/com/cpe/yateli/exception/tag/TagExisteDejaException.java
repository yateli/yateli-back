package com.cpe.yateli.exception.tag;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class TagExisteDejaException extends ExceptionPersonnelle {
    private String nomTag;

    public static TagExisteDejaException creerAvec(HttpStatus httpStatus, String nomTag){
        return new TagExisteDejaException(httpStatus, nomTag);
    }

    private TagExisteDejaException(HttpStatus httpStatus, String nomTag) {
        super(httpStatus);
        this.nomTag = nomTag;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.TAG_EXISTE_DEJA.getMessage(this.nomTag);
    }
}
