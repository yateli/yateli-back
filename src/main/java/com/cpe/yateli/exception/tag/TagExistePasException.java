package com.cpe.yateli.exception.tag;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class TagExistePasException extends ExceptionPersonnelle {
    private String tag;

    public static TagExistePasException creerAvec(HttpStatus httpStatus, String tag){
        return new TagExistePasException(httpStatus, tag);
    }

    private TagExistePasException(HttpStatus httpStatus, String tag) {
        super(httpStatus);
        this.tag = tag;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.TAG_EXISTE_PAS.getMessage(this.tag);
    }
}
