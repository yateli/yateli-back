package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class CodeVerificationIncorrectException extends ExceptionPersonnelle {
    private String codeSaisi;

    public static CodeVerificationIncorrectException creerAvec(HttpStatus httpStatus,String code){
        return new CodeVerificationIncorrectException(httpStatus,code);
    }

    private CodeVerificationIncorrectException(HttpStatus httpStatus,String code){
        super(httpStatus);
        this.codeSaisi = code;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.CODE_VERIFICATION_INCORRECT.getMessage(this.codeSaisi);
    }
}
