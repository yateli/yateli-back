package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class EstDejaAbonneException extends ExceptionPersonnelle {

    public static EstDejaAbonneException creerAvec(HttpStatus httpStatus){
        return new EstDejaAbonneException(httpStatus);
    }

    private EstDejaAbonneException(HttpStatus httpStatus) {
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return ErreurMessages.USER_DEJA_ABONNE.toString();
    }
}
