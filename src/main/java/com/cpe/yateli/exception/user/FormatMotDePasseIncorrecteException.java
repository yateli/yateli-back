package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class FormatMotDePasseIncorrecteException extends ExceptionPersonnelle {
    public static FormatMotDePasseIncorrecteException creerAvec(HttpStatus httpStatus){
        return new FormatMotDePasseIncorrecteException(httpStatus);
    }

    private FormatMotDePasseIncorrecteException(HttpStatus httpStatus) {
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return ErreurMessages.FORMAT_MOT_DE_PASSE_NON_CORRECTE.toString();
    }
}
