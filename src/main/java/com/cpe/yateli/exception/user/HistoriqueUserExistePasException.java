package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class HistoriqueUserExistePasException extends ExceptionPersonnelle {
    private String login;

    public static HistoriqueUserExistePasException creerAvec(HttpStatus httpStatus, String login){
        return new HistoriqueUserExistePasException(httpStatus, login);
    }

    private HistoriqueUserExistePasException(HttpStatus httpStatus, String login) {
        super(httpStatus);
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.HISTORIQUE_USER_EXISTE_PAS.getMessage(this.login);
    }
}
