package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class MailNonCorrectException extends ExceptionPersonnelle {
    private String login;

    public static MailNonCorrectException creerAvec(HttpStatus httpStatus, String login){
        return new MailNonCorrectException(httpStatus,login);
    }

    private MailNonCorrectException(HttpStatus httpStatus, String login) {
        super(httpStatus);
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.MAIL_NON_CORRECTE.getMessage(this.login);
    }
}
