package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class MotDePasseIncorrectException extends ExceptionPersonnelle {

    public static MotDePasseIncorrectException creerAvec(HttpStatus httpStatus){
        return new MotDePasseIncorrectException(httpStatus);
    }

    private MotDePasseIncorrectException(HttpStatus httpStatus) {
        super(httpStatus);
    }

    @Override
    public String getMessage() {
        return ErreurMessages.PASSWORD_NOT_CORRECT.toString();
    }

}
