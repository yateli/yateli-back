package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class NonAbonneException extends ExceptionPersonnelle {
    private String login;

    public static NonAbonneException creerAvec(HttpStatus httpStatus,String login){
        return new NonAbonneException(httpStatus,login);
    }

    private NonAbonneException(HttpStatus httpStatus,String login) {
        super(httpStatus);
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.USER_NON_ABONNE.getMessage(this.login);
    }
}
