package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class PhoneFormatIncorrectException extends ExceptionPersonnelle {
    private String phone;

    public static PhoneFormatIncorrectException creerAvec(HttpStatus httpStatus, String phone){
        return new PhoneFormatIncorrectException(httpStatus, phone);
    }

    private PhoneFormatIncorrectException(HttpStatus httpStatus, String phone) {
        super(httpStatus);
        this.phone = phone;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.PHONE_FORMAT_INCORRECT.getMessage(this.phone);
    }
}
