package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class SuppressionUserProblemeException extends ExceptionPersonnelle {
    private String login;

    public static SuppressionUserProblemeException creerAvec(HttpStatus httpStatus, String login){
        return new SuppressionUserProblemeException(httpStatus,login);
    }

    private SuppressionUserProblemeException(HttpStatus httpStatus, String login) {
        super(httpStatus);
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.SUPPRESSION_USER_PROBLEME.getMessage(this.login);
    }
}
