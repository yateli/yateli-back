package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import com.cpe.yateli.model.Alerte;
import com.cpe.yateli.model.SocketEventType;
import com.cpe.yateli.service.websocket.WebSocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

public class UserBloqueException extends ExceptionPersonnelle {

    @Autowired
    private WebSocketService webSocketService;

    private String mail;

    public static UserBloqueException creerAvec(HttpStatus httpStatus, String mail){
        return new UserBloqueException(httpStatus,mail);
    }

    private UserBloqueException(HttpStatus httpStatus, String mail) {
        super(httpStatus);
        this.mail = mail;

        Alerte alerte = new Alerte();
        alerte.setLoginUser(this.mail);
        alerte.setType(SocketEventType.COMPTE_DESACTIVE);
        alerte.setMessage("Votre compte a été désactivé");
        webSocketService.envoyerUserSpecifique(this.mail, alerte);
    }

    @Override
    public String getMessage() {
        return ErreurMessages.USER_BLOQUE.toString();
    }

}
