package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class UserDejaActiveException extends ExceptionPersonnelle {
    private String login;

    public static UserDejaActiveException creerAvec(HttpStatus httpStatus, String login){
        return new UserDejaActiveException(httpStatus, login);
    }

    private UserDejaActiveException(HttpStatus httpStatus, String login) {
        super(httpStatus);
        this.login = login;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.USER_DEJA_ACTIVE.getMessage(this.login);
    }
}
