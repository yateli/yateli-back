package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class UserExisteDejaException extends ExceptionPersonnelle {   // J'indique à l'application que je crée une exception personnelle

    private String userLogin;

    public static UserExisteDejaException creerAvec(HttpStatus httpStatus, String login){
        return new UserExisteDejaException(httpStatus, login);
    }

    private UserExisteDejaException(HttpStatus httpStatus, String login){
        super(httpStatus);
        this.userLogin = login;
    }

    @Override
    public String getMessage(){
        return ErreurMessages.USER_EXISTE_DEJA.getMessage(this.userLogin);
    }

}
