package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class UserNonConnecteException extends ExceptionPersonnelle {

    private String mail;

    public static UserNonConnecteException creerAvec(HttpStatus httpStatus, String mail){
        return new UserNonConnecteException(httpStatus,mail);
    }

    private UserNonConnecteException(HttpStatus httpStatus, String mail) {
        super(httpStatus);
        this.mail = mail;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.USER_NON_CONNECTE.getMessage(this.mail);
    }

}
