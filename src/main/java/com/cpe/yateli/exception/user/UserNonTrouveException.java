package com.cpe.yateli.exception.user;

import com.cpe.yateli.exception.ErreurMessages;
import com.cpe.yateli.exception.ExceptionPersonnelle;
import org.springframework.http.HttpStatus;

public class UserNonTrouveException extends ExceptionPersonnelle {

    private String mail;

    public static UserNonTrouveException creerAvec(HttpStatus httpStatus, String mail){
        return new UserNonTrouveException(httpStatus,mail);
    }

    private UserNonTrouveException(HttpStatus httpStatus, String mail) {
        super(httpStatus);
        this.mail = mail;
    }

    @Override
    public String getMessage() {
        return ErreurMessages.USER_EXISTE_PAS.getMessage(this.mail);
    }

}
