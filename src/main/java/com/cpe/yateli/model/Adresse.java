package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Adresse")
public class Adresse {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Integer id;

    private String identification;

    //N° appartement, boite aux lettres, étage, couloir
    private String complementDestinataire;

    //entrée, tour, bâtiment, immeuble, résidence
    private String complementPointGeographique;

    private String numeroEtLibelleVoie;

    //exemple : Boite Postale ou lieu dit
    private String serviceParticulier;

    private String codePostal;

    private String ville;

    @Enumerated(EnumType.STRING)
    private Pays pays;

}
