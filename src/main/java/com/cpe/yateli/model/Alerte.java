package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.Instant;

@Document(collection = "alerte")
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Alerte {

    @Expose
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Indexed
    private String id;

    @JsonIgnore
    private Instant date_creation;

    @Expose
    private long timestamp_creation;

    @JsonIgnore
    @Indexed
    private String loginUser;

    @Expose
    @Enumerated(value = EnumType.STRING)
    private SocketEventType type;

    @Expose
    private Object message;

    @Expose
    private boolean vu = false;

    @JsonIgnore
    private Instant date_vu;

    @Expose
    private long timestamp_vu;

}
