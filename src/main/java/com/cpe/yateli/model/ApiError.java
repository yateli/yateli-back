package com.cpe.yateli.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@AllArgsConstructor
public class ApiError {

    private int codeErreur;

    private String exception;


    private String message;

}
