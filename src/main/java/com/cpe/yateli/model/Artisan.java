package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;
import lombok.*;

import javax.persistence.*;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Artisan")
public class Artisan {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Expose
    @OneToOne
    private User user;

    @Expose
    @Enumerated(EnumType.STRING)
    private Pays nationalite;

    @Expose
    @OneToOne(cascade = CascadeType.ALL)
    private Localisation emplacement;

    @JsonIgnore
    private Blob photoArtisan;

    @JsonIgnore
    private String extensionImage;

    @Expose
    private String telephone;

    @JsonBackReference
    @OneToMany(fetch = FetchType.LAZY)
    private List<Produit> produits;

    @JsonIgnore
    private boolean estActif = false;

    @OneToMany
    private List<Blocage> blocages = new ArrayList<>();

   /* private List<Commande> commandes;

    public void defineUserCommeRoleArtisan(){
        this.user.setRole(new Role("Artisan"));
    } */

}
