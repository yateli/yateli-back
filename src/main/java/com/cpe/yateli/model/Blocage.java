package com.cpe.yateli.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Blocage {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Timestamp date;

    @ManyToOne
    private User admin;

    @Enumerated(value = EnumType.STRING)
    private TypeBlocage typeBlocage;

    private String raison;

    @ElementCollection(targetClass = Notification.class)
    @JoinTable(name = "tblNotification", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "notification")
    @Enumerated(EnumType.STRING)
    private List<Notification> notification;

    private boolean actif = true;

}



