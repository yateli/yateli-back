package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.apache.commons.lang3.RandomStringUtils;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "BonAchat", indexes = {@Index(name = "index_code", columnList = "code", unique = true)})
public class BonAchat {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Integer id;

    @NotNull
    @JsonIgnore
    private Timestamp date_creation;

    @NotNull
    private String code;

    @NotNull
    private Double valeur;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @NotNull
    @JsonIgnore
    private boolean dejaUtilise = false;

    @PrePersist
    public void generateCodeBonAchat(){
        this.setCode("BON-" + this.user.getPrenom().toUpperCase()
                + "-"
                + new BigDecimal(this.valeur).setScale(0,BigDecimal.ROUND_HALF_DOWN).toString()
                + RandomStringUtils.random(4,true,true).toUpperCase());
    }

}
