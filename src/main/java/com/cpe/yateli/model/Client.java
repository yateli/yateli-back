package com.cpe.yateli.model;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Client {

    public static String host;

    public Client(@Value("${client.url}") String url){
        host = url;
    }

}
