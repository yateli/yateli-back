package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ColisMystere")
public class ColisMystere {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne
    private Produit produit;

    private Timestamp dateCreation;

    private Timestamp dateDecouverte;

    @ManyToOne(cascade = CascadeType.ALL)
    @JsonIgnore
    private User user;

    private boolean aRecupereColis = false;

}