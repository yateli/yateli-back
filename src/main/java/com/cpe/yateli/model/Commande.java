package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity // table dans la BDD
@Data // Getter et setter générés automatiquement
@Builder // Créer un builder de notre objet
@ToString //Génere seul le ToString
@AllArgsConstructor // Constructeur avec tous les arguments créé
@NoArgsConstructor // Contructeur vide créé
public class Commande {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Timestamp date_creation;

    @OneToMany(cascade = CascadeType.ALL)
    @JsonManagedReference
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ProduitQuantitePanier> produits;

    private float poidsCommande;

    @OneToMany(fetch = FetchType.LAZY) // on prend la commande et sa list de bonsachats
    private List<BonAchat> bonsAchat;

    @OneToOne
    private Adresse adresseLivraison;

    @OneToOne
    private Paiement paiement;

    @JsonManagedReference
    @OneToOne
    private Facture facture;

    @OneToOne
    private Livraison livraison;

    @JsonIgnore
    @OneToOne
    private User user;

}
