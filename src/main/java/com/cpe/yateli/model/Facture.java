package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.sql.Blob;
import java.sql.Timestamp;

@Entity // table dans la BDD
@Data // Getter et setter générés automatiquement
@Builder // Créer un builder de notre objet
@ToString // Génere seul le ToString
@AllArgsConstructor // Constructeur avec tous les arguments créé
@NoArgsConstructor // Contructeur vide créé
public class Facture {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Timestamp date_creation;

    @JsonIgnore
    @OneToOne
    private User user;

    @JsonBackReference
    @OneToOne(cascade = CascadeType.ALL)
    private Commande commande;

    @JsonIgnore
    private Blob pdf;
}



