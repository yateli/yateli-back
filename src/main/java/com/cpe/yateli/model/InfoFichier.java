package com.cpe.yateli.model;

import lombok.*;
import org.springframework.core.io.Resource;

@Data // Getter et setter générés automatiquement
@Builder // Créer un builder de notre objet
@ToString // Génere seul le ToString
@AllArgsConstructor // Constructeur avec tous les arguments créé
@NoArgsConstructor // Contructeur vide créé
public class InfoFichier {

    private String extension;

    private Resource fichier;

}
