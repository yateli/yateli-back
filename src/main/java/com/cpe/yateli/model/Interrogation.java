package com.cpe.yateli.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Interrogation")
public class Interrogation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    private String question;

    private boolean active;

    @NotNull
    @OneToMany
    private List<Reponse> reponses;
    
}
