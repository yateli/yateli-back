package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "JeuConcours", indexes = {@Index(name = "index_nom_jeuconcours", columnList = "nom", unique = true)})
public class JeuConcours {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String nom;

    private String description;

    private String cheminImage;

    private int pointsFideliteMinimum;

    private Timestamp debutConcours;

    private Timestamp finConcours;

    @JsonIgnore
    private boolean fermetureExceptionnelle = false;

    @OneToMany(fetch = FetchType.LAZY) // Un jeu pour plusieurs users
    private List<User> users;

}
