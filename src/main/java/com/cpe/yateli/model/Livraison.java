package com.cpe.yateli.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Livraison")
public class Livraison {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Enumerated(value = EnumType.STRING)
    private Societe societe;

    private float poidsMin;

    private float poidsMax;

    private double prixMin;

    private double prixMax;

    public double getPrixLivraison(float poidsCommande){
        return new BigDecimal((this.prixMin * poidsCommande) / this.poidsMin).setScale(2,BigDecimal.ROUND_HALF_DOWN).doubleValue();
    }

}
