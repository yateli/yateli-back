package com.cpe.yateli.model;

import com.google.gson.annotations.Expose;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Localisation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Expose
    @Enumerated(EnumType.STRING)
    private Pays pays;

    @Expose
    private Double latitude;

    @Expose
    private Double longitude;

}
