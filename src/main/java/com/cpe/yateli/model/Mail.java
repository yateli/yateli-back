package com.cpe.yateli.model;

import lombok.Data;

import java.io.File;
import java.util.List;
import java.util.Map;

@Data
public class Mail {

    private String from;

    private String to;

    private String subject;

    private String content;

    private Map<String,Object> model;

    private Map<String,String> imagesAAjouer;

    private List<File> piecesJointes;

    @Override
    public String toString() {
        return "Mail{" +
                "from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                '}';
    }

}
