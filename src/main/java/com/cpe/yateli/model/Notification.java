package com.cpe.yateli.model;

public enum Notification {

    SMS("SMS"),
    MAIL("MAIL");

    private String notification;

    Notification(String notification) {
        this.notification = notification;
    }

    public String getNotification() {
        return this.notification;
    }

}
