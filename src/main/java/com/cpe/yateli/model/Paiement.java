package com.cpe.yateli.model;

import com.cpe.yateli.exception.commande.CommandeInferieur0Exception;
import lombok.*;
import org.springframework.http.HttpStatus;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity // table dans la BDD
@Data // Getter et setter générés automatiquement
@Builder // Créer un builder de notre objet
@ToString
@AllArgsConstructor // Constructeur avec tous les arguments créé
@NoArgsConstructor // Contructeur vide créé
public class Paiement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String paiementId;

    private Double sousTotal;

    private Double coutTaxe;

    private Double coutLivraison;

    private Double reductions;

    private Double montantTotal;

    private boolean estPaye = false;

    public void calculMontants(Panier panier) throws CommandeInferieur0Exception {
        this.sousTotal = panier.getPrixSousTotalPanier();
        this.coutTaxe = panier.getPrixTotalTaxe();
        Double montantTotalMoinsReduction = panier.getPrixTTCTotal() - this.reductions;
        if(montantTotalMoinsReduction < 0){
            throw CommandeInferieur0Exception.creerAvec(HttpStatus.BAD_REQUEST,(montantTotalMoinsReduction * -1)); // Rendre le nombre positif
        }
        this.montantTotal = new BigDecimal(montantTotalMoinsReduction + this.coutLivraison).setScale(2,BigDecimal.ROUND_HALF_DOWN).doubleValue();
    }

}
