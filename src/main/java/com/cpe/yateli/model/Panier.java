package com.cpe.yateli.model;

import com.cpe.yateli.repository.ProduitQuantitePanierRepository;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.List;

@Entity // table dans la BDD
@Data // Getter et setter générés automatiquement
@Builder // Créer un builder de notre objet
@ToString
@AllArgsConstructor // Constructeur avec tous les arguments créé
@NoArgsConstructor // Contructeur vide créé
public class Panier {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private Timestamp date_creation;

    @OneToOne
    @MapsId
    @JsonBackReference //Permet de ne pas créer de boucle infinie lors le création d'un JSON User
    private User user;

    @OneToMany(cascade = CascadeType.ALL) // on prend le panier et sa list de produits
    @JsonManagedReference
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<ProduitQuantitePanier> produits;

    private Float poidsPanier;

    private Integer pointsFidelitePanier;

    //Nous effectuons les différentes actions qui dérivent de l'ajout d'un produit au panier (ex : ajout à la liste,changement poids panier, nombre points fidelite panier)
    public void ajoutProduitsAuPanier(ProduitQuantitePanier... produitsAvecQuantite){
        for(ProduitQuantitePanier produitAvecQuantite : produitsAvecQuantite){
            this.getProduits().add(produitAvecQuantite);
            updateProduitsAuPanier();
        }
    }

    public void updateProduitsAuPanier(){
        this.poidsPanier = 0f;
        this.pointsFidelitePanier = 0;
        for(ProduitQuantitePanier produitAvecQuantite : this.getProduits()){
            this.poidsPanier = new BigDecimal(this.poidsPanier + (produitAvecQuantite.getProduit().getPoids() * produitAvecQuantite.getQuantite())).setScale(2,BigDecimal.ROUND_UP).floatValue();
            this.pointsFidelitePanier += (produitAvecQuantite.getProduit().getPointsFidelite() * produitAvecQuantite.getQuantite());
        }
    }

    public void supprimerProduitsAuPanier(ProduitQuantitePanierRepository produitQuantitePanierRepository, ProduitQuantitePanier produitAvecQuantite, Integer quantite){
        if(quantite != null && produitAvecQuantite.getQuantite() > 1 && produitAvecQuantite.getQuantite() > quantite){
            produitAvecQuantite.setQuantite(produitAvecQuantite.getQuantite() - quantite);
        }else{
            this.getProduits().remove(produitAvecQuantite);
            produitQuantitePanierRepository.delete(produitAvecQuantite);
        }
        updateProduitsAuPanier();
    }

    public Double getPrixSousTotalPanier(){
        Double prixTotalPanier = 0d;
        for(ProduitQuantitePanier produitQuantitePanier : this.getProduits()){
            Double test = produitQuantitePanier.getProduit().getPrixHT() * (double) produitQuantitePanier.getQuantite();
            prixTotalPanier += test;
        }
        return new BigDecimal(prixTotalPanier).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue();
    }

    public Double getPrixTotalTaxe(){
        Double prixTotalTaxe = 0d;
        for(ProduitQuantitePanier produitQuantitePanier : this.getProduits()){
            prixTotalTaxe += produitQuantitePanier.getProduit().getValeurTaxe() * produitQuantitePanier.getQuantite();
        }
        return new BigDecimal(prixTotalTaxe).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue();
    }

    public Double getPrixTTCTotal(){
        Double prixTotalTTC = 0d;
        for(ProduitQuantitePanier produitQuantitePanier : this.getProduits()){
            prixTotalTTC += produitQuantitePanier.getProduit().getPrixTTC() * produitQuantitePanier.getQuantite();
        }
        return new BigDecimal(prixTotalTTC).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue();
    }

}
