package com.cpe.yateli.model;

public enum Pays {

    Afghanistan("Afghanistan"),

    Afriquedusud("Afrique du sud"),

    Albanie("Albanie"),

    Algerie("Algérie"),

    Allemagne("Allemagne"),

    Andorre("Andorre"),

    Angleterre("Angleterre"),

    Angola("Angola"),

    Anguilla("Anguilla"),

    Antarctique("Antarctique"),

    AntiguaetBarbuda("Antigua et Barbuda"),

    Antillesneerlandaises("Antilles néerlandaises"),

    Arabiesaoudite("Arabie saoudite"),

    Argentine("Argentine"),

    Armenie("Arménie"),

    Aruba("Aruba"),

    Australie("Australie"),

    Autriche("Autriche"),

    Azerbaidjan("Azerbaïdjan"),

    Bahamas("Bahamas"),

    Bahrain("Bahrain"),

    Bangladesh("Bangladesh"),

    Belgique("Belgique"),

    Belize("Belize"),

    Benin("Benin"),

    Bermudes("Bermudes"),

    Bhoutan("Bhoutan"),

    Bielorussie("Biélorussie"),

    Birmanie("Birmanie"),

    Bolivie("Bolivie"),

    BosnieHerzegovine("Bosnie-Herzégovine"),

    Botswana("Botswana"),

    Bouvet("Bouvet"),

    Bresil("Brésil"),

    Brunei("Brunei"),

    Bulgarie("Bulgarie"),

    BurkinaFaso("Burkina Faso"),

    Burundi("Burundi"),

    Cambodge("Cambodge"),

    Cameroun("Cameroun"),

    Canada("Canada"),

    CapVert("Cap Vert"),

    Cayman("Cayman"),

    Chili("Chili"),

    Chine("Chine"),

    Christmas("Christmas"),

    Chypre("Chypre"),

    Cocos("Cocos"),

    Colombie("Colombie"),

    Comores("Comores"),

    Congo("Congo"),

    Cook("Cook"),

    CoreeduNord("Corée du Nord"),

    CoreeduSud("Corée du Sud"),

    CostaRica("Costa Rica"),

    CotedIvoire("Côte d'Ivoire"),

    Croatie("Croatie"),

    Cuba("Cuba"),

    Danemark("Danemark"),

    Djibouti("Djibouti"),

    Dominique("Dominique"),

    Egypte("Egypte"),

    ElSalvador("El Salvador"),

    Emiratsarabesunis("Emirats arabes unis"),

    Equateur("Equateur"),

    Erythree("Erythrée"),

    Espagne("Espagne"),

    Estonie("Estonie"),

    EtatsUnis("Etats-Unis"),

    Ethiopie("Ethiopie"),

    Falkland("Falkland"),

    Feroe("Féroé"),

    Fidji("Fidji"),

    Finlande("Finlande"),

    France("France"),

    Gabon("Gabon"),

    Gambie("Gambie"),

    Georgie("Géorgie"),

    Ghana("Ghana"),

    Gibraltar("Gibraltar"),

    Grece("Grèce"),

    Grenade("Grenade"),

    Groenland("Groenland"),

    Guadeloupe("Guadeloupe"),

    Guam("Guam"),

    Guatemala("Guatemala"),

    Guinee("Guinée"),

    GuineeEquatoriale("Guinée Equatoriale"),

    GuineeBissau("Guinée-Bissau"),

    Guyane("Guyane"),

    Guyanefrancaise("Guyane française"),

    Haiti("Haïti"),

    Honduras("Honduras"),

    Hongrie("Hongrie"),

    Inde("Inde"),

    Indonesie("Indonésie"),

    Irak("Irak"),

    Iran("Iran"),

    Irlande("Irlande"),

    Islande("Islande"),

    Israel("Israël"),

    Italie("Italie"),

    Jamaique("Jamaique"),

    Japon("Japon"),

    Jordanie("Jordanie"),

    Kazakhstan("Kazakhstan"),

    Kenya("Kenya"),

    Kirghizistan("Kirghizistan"),

    Kiribati("Kiribati"),

    Koweit("Koweit"),

    LaBarbad("La Barbad"),

    Laos("Laos"),

    Lesotho("Lesotho"),

    Lettonie("Lettonie"),

    Liban("Liban"),

    Liberia("Libéria"),

    Libye("Libye"),

    Liechtenstein("Liechtenstein"),

    Lithuanie("Lithuanie"),

    Luxembourg("Luxembourg"),

    Macau("Macau"),

    Macedoine("Macédoine"),

    Madagascar("Madagascar"),

    Malaisie("Malaisie"),

    Malawi("Malawi"),

    Maldives("Maldives"),

    Mali("Mali"),

    Malte("Malte"),

    MariannesduNord("Mariannes du Nord"),

    Maroc("Maroc"),

    Marshall("Marshall"),

    Martinique("Martinique"),

    Maurice("Maurice"),

    Mauritanie("Mauritanie"),

    Mayotte("Mayotte"),

    Mexique("Mexique"),

    Micronesie("Micronésie"),

    Moldavie("Moldavie"),

    Monaco("Monaco"),

    Mongolie("Mongolie"),

    Montserrat("Montserrat"),

    Mozambique("Mozambique"),

    Myanmar("Myanmar"),

    Namibie("Namibie"),

    Nauru("Nauru"),

    Nepal("Nepal"),

    Nicaragua("Nicaragua"),

    Niger("Niger"),

    Nigeria("Nigeria"),

    Niue("Niue"),

    Norfolk("Norfolk"),

    Norvege("Norvège"),

    NouvelleCaledonie("Nouvelle Calédonie"),

    NouvelleZelande("Nouvelle-Zélande"),

    Oman("Oman"),

    Ouganda("Ouganda"),

    Ouzbekistan("Ouzbékistan"),

    Pakistan("Pakistan"),

    Palau("Palau"),

    Panama("Panama"),

    PapouasieNouvelleGuinee("Papouasie-Nouvelle-Guinée"),

    Paraguay("Paraguay"),

    PaysBas("Pays-Bas"),

    Perou("Pérou"),

    Philippines("Philippines"),

    Pitcairn("Pitcairn"),

    Pologne("Pologne"),

    Polynesiefrancaise("Polynésie française"),

    PortoRico("Porto Rico"),

    Portugal("Portugal"),

    Qatar("Qatar"),

    Republiquecentrafricaine("République centrafricaine"),

    RepubliqueDominicaine("République Dominicaine"),

    Republiquetcheque("République tchèque"),

    Reunion("Réunion"),

    Roumanie("Roumanie"),

    RoyaumeUni("Royaume-Uni"),

    Russie("Russie"),

    Rwanda("Rwanda"),

    SaharaOccidental("Sahara Occidental"),

    SaintPierreetMiquelon("Saint Pierre et Miquelon"),

    SaintVincentetlesGrenadine("Saint Vincent et les Grenadine"),

    SaintKittsetNevis("Saint-Kitts et Nevis"),

    SaintMarin("Saint-Marin"),

    SainteHelene("Sainte Hélène"),

    SainteLucie("Sainte Lucie"),

    Samoa("Samoa"),

    Senegal("Sénégal"),

    Seychelles("Seychelles"),

    SierraLeone("Sierra Leone"),

    Singapour("Singapour"),

    Slovaquie("Slovaquie"),

    Slovenie("Slovénie"),

    Somalie("Somalie"),

    Soudan("Soudan"),

    SriLanka("Sri Lanka"),

    Suede("Suède"),

    Suisse("Suisse"),

    Suriname("Suriname"),

    Syrie("Syrie"),

    Tadjikistan("Tadjikistan"),

    Taiwan("Taiwan"),

    Tanzanie("Tanzanie"),

    Tchad("Tchad"),

    Thailande("Thailande"),

    Timor("Timor"),

    Togo("Togo"),

    Tokelau("Tokelau"),

    Tonga("Tonga"),

    TriniteetTobago("Trinité et Tobago"),

    Tunisie("Tunisie"),

    Turkmenistan("Turkménistan"),

    Turquie("Turquie"),

    Tuvalu("Tuvalu"),

    Ukraine("Ukraine"),

    Uruguay("Uruguay"),

    Vanuatu("Vanuatu"),

    Vatican("Vatican"),

    Venezuela("Venezuela"),

    Vierges("Vierges"),

    Vietnam("Vietnam"),

    WallisetFutuna("Wallis et Futuna"),

    Yemen("Yemen"),

    Yougoslavie("Yougoslavie"),

    Zaire("Zaïre"),

    Zambie("Zambie"),

    Zimbabwe("Zimbabwe");

    private String pays;

    Pays(String pays) {
        this.pays = pays;
    }

    public String getNomPays() {
        return this.pays;
    }
}
