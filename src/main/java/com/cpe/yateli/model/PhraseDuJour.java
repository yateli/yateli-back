package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.gson.annotations.Expose;
import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "PhraseDuJour")
public class PhraseDuJour {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private int id;

    @Expose
    private String phrase;

    @Expose
    private String urlBackground;

    @Expose
    private Timestamp dateCreation;

    @OneToOne
    @JsonIgnore
    private User admin;

}
