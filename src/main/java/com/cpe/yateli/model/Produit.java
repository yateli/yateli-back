package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.google.gson.annotations.Expose;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Produit", indexes = {@Index(name = "index_nom_produit", columnList = "nom")}, uniqueConstraints={
        @UniqueConstraint(columnNames = {"reference"})})
public class Produit {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotEmpty
    @Expose
    private String reference;

    @Expose
    private String nom;

    @Expose
    private String description;

    @Expose
    @JsonIgnore
    private Blob image;

    @JsonIgnore
    private String extensionImage;

    @Expose
    private Timestamp date_ajout;

    @JsonManagedReference
    @ManyToMany(cascade = {CascadeType.MERGE})
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "produit_tag",
            joinColumns = @JoinColumn(name = "produit_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags = new HashSet<>();

    @NotNull
    private Double prixHT;

    @NotNull
    private float tauxTaxe;

    private Double valeurTaxe;

    private Double prixTTC;

    private Float poids;

    private Integer pointsFidelite;

    private Integer quantiteStock;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    private Artisan artisan;

    @OneToMany
    private List<Blocage> blocages = new ArrayList<>();

    private Integer quantiteMinimum;  //Limite alerte stock insuffisant

    public void calculTaxe(){
        this.valeurTaxe = new BigDecimal(this.prixHT * this.tauxTaxe).setScale(2,BigDecimal.ROUND_UP).doubleValue();
        this.prixTTC = new BigDecimal(this.valeurTaxe + this.prixHT).setScale(2,BigDecimal.ROUND_HALF_DOWN).doubleValue();
    }

    public void ajoutTag(Tag tag){
        tags.add(tag);
        tag.getProduits().add(this);
    }

    public void supprimerTag(Tag tag) {
        tag.getProduits().remove(this);
        tags.remove(tag);
    }

    @Override
    public boolean equals(Object o){
        if(this == o) return true;
        if(!(o instanceof Produit)) return false;
        return id != null && id.equals(((Produit) o).id);
    }

    @Override
    public int hashCode(){
        return 31;
    }

}
