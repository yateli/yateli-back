package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ProduitQuantitePanier")
public class ProduitQuantitePanier {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @OneToOne
    private Produit produit;

    @ManyToOne
    @JsonBackReference
    private Panier panier;

    private int quantite;

}
