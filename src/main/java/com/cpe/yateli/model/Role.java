package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Role", indexes = {@Index(name = "index_nom", columnList = "nom", unique = true)})
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String nom;

    @JsonBackReference
    @OneToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<User> users;

    public int getNombreDeUsers(){
        return users.size();
    }

}
