package com.cpe.yateli.model;

public enum Societe {

    CHRONOPOST("Chronopost"),
    UPS("UPS");

    private String societe;

    Societe(String societe) {
        this.societe = societe;
    }

    public String getSociete() {
        return this.societe;
    }

}
