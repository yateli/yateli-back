package com.cpe.yateli.model;

import com.google.gson.annotations.Expose;

public enum SocketEventType {

    DECONNEXION("DECONNEXION"),
    COMPTE_DESACTIVE("COMPTE_DESACTIVE"),
    EDITION_PHRASE_JOUR("EDITION_PHRASE_JOUR"),
    SUPPRESSION_PHRASE_JOUR("SUPPRESSION_PHRASE_JOUR"),
    UPDATE_PRODUIT("UPDATE_PRODUIT"),
    DEMANDE_ARTISAN("DEMANDE_ARTISAN"),
    ACCEPTATION_ARTISAN("ACCEPTATION_ARTISAN"),
    REFUS_ARTISAN("REFUS_ARTISAN"),
    DECOUVERTE_COLIS_MYSTERE("DECOUVERTE_COLIS_MYSTERE"),
    STOCK_INSUFFISANT("STOCK_INSUFFISANT"),
    PRODUIT_CREE("PRODUIT_CREE"),
    BLOCAGE_ARTISAN("BLOCAGE_ARTISAN");

    @Expose
    private String event;

    SocketEventType(String event) {
        this.event = event;
    }

    public String getLabel() {
        return this.event;
    }

}
