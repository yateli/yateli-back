package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Tag", indexes = {@Index(name = "index_nom_tag", columnList = "nom", unique = true)})
public class Tag {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @NaturalId
    private String nom;

    @JsonBackReference
    @ManyToMany(mappedBy = "tags", fetch = FetchType.EAGER)
    private Set<Reponse> reponses = new HashSet<>();

    @JsonBackReference
    @ManyToMany(mappedBy = "listeEnvies", fetch = FetchType.EAGER)
    private Set<User> users = new HashSet<>();

    @JsonBackReference
    @ManyToMany(mappedBy = "tags", fetch = FetchType.EAGER)
    private Set<Produit> produits =  new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tag tag = (Tag) o;
        return Objects.equals(nom, tag.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom);
    }

}
