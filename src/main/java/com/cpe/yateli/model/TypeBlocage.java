package com.cpe.yateli.model;

public enum TypeBlocage {

    NOUVEAU_PRODUIT("NOUVEAU_PRODUIT"),
    PRODUIT("PRODUIT"),
    ARTISAN("ARTISAN"),
    USER("USER");

    private String typeBlocage;

    TypeBlocage(String typeBlocage) {
        this.typeBlocage = typeBlocage;
    }

    public String getTypeBlocage() {
        return this.typeBlocage;
    }

}
