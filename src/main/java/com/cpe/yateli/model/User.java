package com.cpe.yateli.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.google.gson.annotations.Expose;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@Builder
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "User", indexes = {@Index(name = "index_mail", columnList = "mail", unique = true)})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private Integer id;

    @Expose
    private String prenom;

    @Expose
    private String mail;

    @Column(length = 70)
    @JsonIgnore
    private String password;

    @Expose
    private String telephone;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private Panier panier;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;

    @Expose
    private Timestamp dateCreation;

    private int pointsFidelite;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<ColisMystere> colisMystereListe;

    @OneToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private List<BonAchat> bonAchats;

    @JsonManagedReference
    @ManyToMany(cascade = {CascadeType.MERGE})
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "user_tag",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> listeEnvies = new HashSet<>();

    @Column(name = "estAbonne", columnDefinition = "boolean default false", nullable = false)
    private Boolean estAbonne = false;

    @JsonIgnore
    private boolean estActive = false;

    @JsonIgnore
    private String codeRecuperation;

    public void ajoutColisMystere(ColisMystere colisMystere){
        colisMystereListe.add(colisMystere);
        colisMystere.setUser(this);
    }

    public void retirerColisMystere(ColisMystere colisMystere){
        colisMystereListe.remove(colisMystere);
        colisMystere.setUser(null);
    }

    public List<ColisMystere> viderTousColisMystere(){
        List<ColisMystere> colisMystereaSupprimer = new ArrayList<>();
        for(ColisMystere colisMystere : colisMystereListe){
            colisMystere.setUser(null);
            colisMystereaSupprimer.add(colisMystere);
        }
        colisMystereListe.clear();
        return colisMystereaSupprimer;
    }

    public void ajoutEnvie(Tag tag){
        listeEnvies.add(tag);
        tag.getUsers().add(this);
    }

    public void supprimerEnvie(Tag tag) {
        listeEnvies.remove(tag);
        tag.getUsers().remove(this);
    }

    @Override
    public boolean equals(Object o){
        if(this == o) return true;
        if(!(o instanceof User)) return false;
        return id != null && id.equals(((User) o).id);
    }

    @Override
    public int hashCode(){
        return 31;
    }

}
