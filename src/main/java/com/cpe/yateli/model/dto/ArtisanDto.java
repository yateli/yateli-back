package com.cpe.yateli.model.dto;

import com.cpe.yateli.model.Pays;
import lombok.*;
import javax.validation.constraints.NotNull;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ArtisanDto {

    @NotNull
    private String mail;

    private Pays nationalite;

    private Double latitude;

    private Double longitude;

    private String telephone;

}
