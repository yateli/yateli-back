package com.cpe.yateli.model.dto;

import com.cpe.yateli.model.Adresse;
import com.cpe.yateli.model.Societe;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class CommandeDto {

    private List<String> codesBonsAchat;

    @NotNull
    private Adresse adresseLivraison;

    @NotNull
    private Societe societeLivraison;

}
