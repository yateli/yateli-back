package com.cpe.yateli.model.dto;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InterrogationActivesDto {

    @NotEmpty
    List<Integer> idQuestions;

}
