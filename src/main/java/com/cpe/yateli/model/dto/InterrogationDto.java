package com.cpe.yateli.model.dto;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InterrogationDto {

    @NotEmpty
    private String question;

    @NotNull
    private List<ReponseDto> reponses;

}
