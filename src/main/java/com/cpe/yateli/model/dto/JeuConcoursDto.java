package com.cpe.yateli.model.dto;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class JeuConcoursDto {

    @NotEmpty
    private String nom;

    @NotEmpty
    private String description;

    @NotNull
    private int pointsFideliteMinimum;

    @NotNull
    private long debutConcours;

    @NotNull
    private long finConcours;

    private boolean estFerme;

    private List<String> loginsInvite;

}
