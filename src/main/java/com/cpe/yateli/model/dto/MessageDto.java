package com.cpe.yateli.model.dto;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class MessageDto {

    private Object message;

}
