package com.cpe.yateli.model.dto;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ProduitArtisanDto {

    @NotEmpty
    private String reference;

    private String nom;

    private String description;

    private List<String> nomsTag;

    private Double prixHT;

    private float poids;

    private int quantiteStock;

    @NotEmpty
    private String mailArtisan;

}
