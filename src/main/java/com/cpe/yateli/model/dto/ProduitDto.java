package com.cpe.yateli.model.dto;

import lombok.*;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ProduitDto {

    @NotEmpty
    private String reference;

    private String nom;

    private String description;

    private List<String> nomsTag;

    private Double prixHT;

    private float tauxTaxe;

    private float poids;

    private int pointsFidelite;

    private int quantiteStock;

    private int quantiteMinimum;

    @NotEmpty
    private String mailArtisan;

}
