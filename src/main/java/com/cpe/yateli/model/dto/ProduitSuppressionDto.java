package com.cpe.yateli.model.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ProduitSuppressionDto {

    @NotNull
    List<String> referenceProduits;

}
