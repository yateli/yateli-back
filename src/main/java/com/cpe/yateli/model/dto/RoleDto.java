package com.cpe.yateli.model.dto;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto {

    private String nom;

}
