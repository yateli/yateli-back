package com.cpe.yateli.model.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    @NotNull
    private String prenom;

    @NotNull
    private String mail;

    @NotNull
    private String password;

    private String telephone;

    @NotNull
    private RoleDto role;

    @NotNull
    private List<Integer> reponses;

}
