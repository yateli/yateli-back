package com.cpe.yateli.model.dto;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UserModificationDto {

    private String prenom;

    @NotNull
    private String mail;

    private String password;

    private String telephone;

    private List<TagDto> listeEnvies;

}
