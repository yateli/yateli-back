package com.cpe.yateli.model.logMongo;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.time.Instant;

@Document(collection = "evenement")
@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Evenement {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Indexed
    private String id;

    private Instant date;

    @Indexed
    private String loginUser;

    private String log;

}