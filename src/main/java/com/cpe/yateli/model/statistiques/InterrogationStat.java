package com.cpe.yateli.model.statistiques;

import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InterrogationStat {

    private String question;

    private ReponseStat reponse;

}
