package com.cpe.yateli.model.statistiques;

import lombok.*;

import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ReponseStat {

    private String reponse;

    private List<String> tags;

}
