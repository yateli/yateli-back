package com.cpe.yateli.model.view;

import com.cpe.yateli.model.Facture;
import lombok.Data;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Data
@ToString
public class FactureView {

    private int id;

    private Timestamp dateCreation;

    private String userLogin;

    private int idCommande;

    private Double montantTotal;

    public static FactureView creerFactureViewDepuisFacture(Facture facture){
        FactureView factureView = new FactureView();
        factureView.setId(facture.getId());
        factureView.setDateCreation(facture.getDate_creation());
        factureView.setUserLogin(facture.getUser().getMail());
        factureView.setIdCommande(facture.getCommande().getId());
        factureView.setMontantTotal(facture.getCommande().getPaiement().getMontantTotal());
        return factureView;
    }

    public static List<FactureView> creerListFactureViewDepuisListFacture(List<Facture> factures){
        List<FactureView> factureViews = new ArrayList<>();
        for(Facture facture : factures){
            factureViews.add(creerFactureViewDepuisFacture(facture));
        }
        return factureViews;
    }

}
