package com.cpe.yateli.model.view;

import com.cpe.yateli.model.*;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.sql.Blob;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@ToString
@Builder
public class ProduitView {

    private String reference;

    private String nom;

    private String description;

    private Timestamp date_ajout;

    private Double prixHT;

    private float tauxTaxe;

    private Double valeurTaxe;

    private Double prixTTC;

    private Float poids;

    private Integer pointsFidelite;

    private Integer quantiteStock;

    private Artisan artisan;

    public static ProduitView creerProduitViewDepuisProduit(Produit produit){
        return ProduitView.builder()
                .reference(produit.getReference())
                .nom(produit.getNom())
                .description(produit.getDescription())
                .date_ajout(produit.getDate_ajout())
                .prixHT(produit.getPrixHT())
                .tauxTaxe(produit.getTauxTaxe())
                .valeurTaxe(produit.getValeurTaxe())
                .prixTTC(produit.getPrixTTC())
                .poids(produit.getPoids())
                .pointsFidelite(produit.getPointsFidelite())
                .quantiteStock(produit.getQuantiteStock())
                .artisan(produit.getArtisan())
                .build();
    }

}
