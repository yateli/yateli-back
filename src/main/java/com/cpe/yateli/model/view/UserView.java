package com.cpe.yateli.model.view;

import com.cpe.yateli.model.BonAchat;
import com.cpe.yateli.model.ColisMystere;
import com.cpe.yateli.model.Panier;
import com.cpe.yateli.model.Tag;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

@Data
@ToString
@Builder
public class UserView {

    private int id;

    private String prenom;

    private String mail;

    private String telephone;

    private Panier panier;

    private Timestamp dateCreation;

    private int pointsFidelite;

    private List<ColisMystere> colisMystereListe;

    private List<BonAchat> bonAchats;

    private Set<Tag> listeEnvies;

    private Boolean estAbonne;

    private String role;

}
