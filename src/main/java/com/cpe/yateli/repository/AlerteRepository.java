package com.cpe.yateli.repository;

import com.cpe.yateli.model.Alerte;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface AlerteRepository extends MongoRepository<Alerte,String> {

    List<Alerte> findAllByLoginUser(String login);

    Alerte findAlerteById(String id);

}
