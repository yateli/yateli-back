package com.cpe.yateli.repository;

import com.cpe.yateli.model.Artisan;
import com.cpe.yateli.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArtisanRepository extends JpaRepository<Artisan, Long> {

    Artisan findById(int id);

    Artisan findByUser(User artisan);

    Artisan findByUser_Mail(String login);

}
