package com.cpe.yateli.repository;

import com.cpe.yateli.model.Blocage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlocageRepository extends JpaRepository<Blocage, Long> {

}
