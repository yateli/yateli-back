package com.cpe.yateli.repository;

import com.cpe.yateli.model.BonAchat;
import com.cpe.yateli.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BonAchatRepository extends JpaRepository<BonAchat,Long> {

    BonAchat findBonAchatByCodeAndUser(String code, User user);

    List<BonAchat> findAllByUser(User user);

}
