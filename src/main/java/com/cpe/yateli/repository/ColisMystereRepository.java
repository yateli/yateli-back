package com.cpe.yateli.repository;

import com.cpe.yateli.model.ColisMystere;
import com.cpe.yateli.model.Produit;
import com.cpe.yateli.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Timestamp;
import java.util.List;

public interface ColisMystereRepository extends JpaRepository<ColisMystere,Long> {

    ColisMystere findColisMystereByUserAndARecupereColisIsFalse(User user);

    List<ColisMystere> findAllByDateDecouverteBetweenAndProduitIsNull(Timestamp date_debut_comparer,Timestamp date_fin_comparer);

    List<ColisMystere> findAllByDateDecouverteBetweenAndARecupereColisIsFalse(Timestamp date_debut_comparer,Timestamp date_fin_comparer);

    List<ColisMystere> findAllByProduitIs(Produit produit);

}
