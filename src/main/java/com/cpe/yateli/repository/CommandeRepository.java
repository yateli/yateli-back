package com.cpe.yateli.repository;

import com.cpe.yateli.model.Commande;
import com.cpe.yateli.model.Produit;
import com.cpe.yateli.model.ProduitQuantitePanier;
import com.cpe.yateli.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommandeRepository extends JpaRepository<Commande,Long>{

    Commande findCommandeById(Integer id);

    Commande findCommandeByPaiement_PaiementId(String idPaiement);

    List<Commande> findAllByUser(User user);

    List<Commande> findAllByUserAndPaiement_EstPayeFalse(User user);

    List<Commande> findAllByProduitsContaining(ProduitQuantitePanier produit);

}
