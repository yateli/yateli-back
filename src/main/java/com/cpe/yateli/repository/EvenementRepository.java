package com.cpe.yateli.repository;

import com.cpe.yateli.model.logMongo.Evenement;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface EvenementRepository extends MongoRepository<Evenement,String> {

    List<Evenement> findAllByLoginUserIs(String login);

}
