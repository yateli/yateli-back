package com.cpe.yateli.repository;

import com.cpe.yateli.model.Facture;
import com.cpe.yateli.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FactureRepository extends JpaRepository<Facture, Long> {

    Facture findFactureByIdAndUser(int id, User user);

    List<Facture> findAllByUser(User user);

}
