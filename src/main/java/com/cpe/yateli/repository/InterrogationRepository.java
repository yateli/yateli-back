package com.cpe.yateli.repository;

import com.cpe.yateli.model.Interrogation;
import com.cpe.yateli.model.Reponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InterrogationRepository extends JpaRepository<Interrogation,Long> {

    List<Interrogation> findInterrogationByReponsesContains(List<Reponse> reponses);

    Interrogation findById(Integer id);

    List<Interrogation> findAllByActiveIsTrue();

}
