package com.cpe.yateli.repository;

import com.cpe.yateli.model.JeuConcours;
import com.cpe.yateli.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface JeuConcoursRepository extends JpaRepository<JeuConcours,Long> {

    JeuConcours findJeuConcoursById(int idJeuConcours);

    JeuConcours findJeuConcoursByNom(String nom);

    List<JeuConcours> findAllByUsersContains(User user);

    List<JeuConcours> findAllByDebutConcoursBeforeAndFinConcoursAfterAndFermetureExceptionnelleFalse(Timestamp date1, Timestamp date2);

}
