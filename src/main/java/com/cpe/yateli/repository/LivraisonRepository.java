package com.cpe.yateli.repository;

import com.cpe.yateli.model.Livraison;
import com.cpe.yateli.model.Societe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LivraisonRepository extends JpaRepository<Livraison, Long> {

    Livraison findLivraisonById(Integer id);

    List<Livraison> findLivraisonBySociete(Societe societe);

}
