package com.cpe.yateli.repository;

import com.cpe.yateli.model.Panier;
import com.cpe.yateli.model.ProduitQuantitePanier;
import com.cpe.yateli.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PanierRepository extends JpaRepository<Panier, Long> {

    Panier findByUser(User user);

    Panier findByProduitsContaining(ProduitQuantitePanier produitQuantitePanier);

}
