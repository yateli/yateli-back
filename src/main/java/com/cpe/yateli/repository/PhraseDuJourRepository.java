package com.cpe.yateli.repository;

import com.cpe.yateli.model.PhraseDuJour;
import com.cpe.yateli.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PhraseDuJourRepository extends JpaRepository<PhraseDuJour,Integer> {

    List<PhraseDuJour> findAllByAdmin(User admin);

}
