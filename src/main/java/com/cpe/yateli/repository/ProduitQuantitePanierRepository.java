package com.cpe.yateli.repository;

import com.cpe.yateli.model.Panier;
import com.cpe.yateli.model.Produit;
import com.cpe.yateli.model.ProduitQuantitePanier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProduitQuantitePanierRepository extends JpaRepository<ProduitQuantitePanier, Long> {

    ProduitQuantitePanier findByProduit(Produit produit);

    List<ProduitQuantitePanier> findAllByProduit(Produit produit);

    List<ProduitQuantitePanier> findByPanier(Panier panier);

}
