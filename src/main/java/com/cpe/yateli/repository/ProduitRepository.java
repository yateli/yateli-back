package com.cpe.yateli.repository;

import com.cpe.yateli.model.Produit;
import com.cpe.yateli.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long> {

    Produit findProduitByReference(String reference);

    Produit findProduitByNom(String nom);

    List<Produit> findAllByTagsContaining(Tag tag);

    List<Produit> findAllByReferenceIn(Set<String> references);

}
