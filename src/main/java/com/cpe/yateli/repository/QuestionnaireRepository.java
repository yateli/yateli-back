package com.cpe.yateli.repository;

import com.cpe.yateli.model.statistiques.Questionnaire;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.Instant;
import java.util.List;

public interface QuestionnaireRepository extends MongoRepository<Questionnaire,String> {

    List<Questionnaire> findAllByDateAfterAndMailIs(Instant date, String login);

    List<Questionnaire> findAllByDateAfter(Instant date);

}
