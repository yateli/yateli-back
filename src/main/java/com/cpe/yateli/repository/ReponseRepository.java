package com.cpe.yateli.repository;

import com.cpe.yateli.model.Reponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReponseRepository extends JpaRepository<Reponse,Long> {

    List<Reponse> findAllByIdIn(List<Integer> ids);

}
