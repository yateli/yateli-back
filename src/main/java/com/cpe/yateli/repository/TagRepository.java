package com.cpe.yateli.repository;

import com.cpe.yateli.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

    Tag findTagById(int id);

    Tag findTagByNom(String nom);

    List<Tag> findTagsByNomIn(Set<String> noms);

}
