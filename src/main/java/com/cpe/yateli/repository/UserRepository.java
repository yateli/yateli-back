package com.cpe.yateli.repository;

import com.cpe.yateli.model.Role;
import com.cpe.yateli.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByMail(String mail);

    User findUserByCodeRecuperation(String code);

    List<User> findAll();

    List<User> findUsersByRole(Role role);

    List<User> findAllByDateCreationBeforeAndEstActiveIsFalseAndRoleIs(Timestamp date, Role role);

    List<User> findAllByDateCreationLessThan(Timestamp unAnAvant);

}
