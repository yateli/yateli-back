package com.cpe.yateli.security;

import com.cpe.yateli.model.Role;
import com.cpe.yateli.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Provides a basic implementation of the UserDetails interface
 */
public class CustomUserDetails implements UserDetails {

    private Collection<? extends GrantedAuthority> authorities;
    private String password;
    private String username;
    private boolean estActive;

    public CustomUserDetails(User user) {
        this.username = user.getMail();
        this.password = user.getPassword();
        this.authorities = translate(user.getRole());
        this.estActive = user.isEstActive();
    }

    /**
     * Translates the List<Role> to a List<GrantedAuthority>
     * @param role the input Role.
     * @return a list of granted authorities
     */
    private Collection<? extends GrantedAuthority> translate(Role role) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        String name = role.getNom().toUpperCase();
        //Make sure that all roles start with "ROLE_"
        if (!name.startsWith("ROLE_"))
            name = "ROLE_" + name;
        authorities.add(new SimpleGrantedAuthority(name));
        return authorities;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return estActive;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
