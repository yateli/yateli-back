package com.cpe.yateli.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@Order(1)
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .httpBasic().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .csrf().disable()
                .headers().frameOptions().disable().and()
                .requestMatchers()
                .antMatchers("/","/socket-yateli/**","/api/user/creer","/api/user/activerCompte","/api/user/motDePasseOublie","/api/user/verificationCode","/api/user/nouveauPasswordAvecCode","/api/paiement/returnPaiement", "/api/produit/recupererTout", "/api/produit/recupererProduitsArtisan", "/api/tag/recupererTousTags", "/api/produit/recupererImage/**", "/api/phraseDuJour/getPhraseDuJour", "/api/artisan/getTousLesArtisans", "/api/artisan/recupererImage/**", "/api/interrogation/recupererQuestions", "/api/pays/recupererPays", "/api/produit/detailsProduit/**","/oauth/authorize", "/oauth/token**", "/yateli/oauth/token**")
                .and()
                .authorizeRequests()
                .antMatchers("/","/socket-yateli/**","/api/user/creer","/api/user/activerCompte","/api/user/motDePasseOublie","/api/user/verificationCode","/api/user/nouveauPasswordAvecCode","/api/paiement/returnPaiement", "/api/produit/recupererTout", "/api/produit/recupererProduitsArtisan", "/api/tag/recupererTousTags", "/api/produit/recupererImage/**", "/api/phraseDuJour/getPhraseDuJour", "/api/artisan/getTousLesArtisans", "/api/artisan/recupererImage/**", "/api/interrogation/recupererQuestions", "/api/pays/recupererPays", "/api/produit/detailsProduit/**")
                .anonymous()
                .anyRequest().authenticated();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/v2/api-docs",
                        "/swagger-resources/**",
                        "/documentation/**",
                        "/swagger-ui.html",
                        "/webjars/**",
                        "/static/**",
                        "/resources/**",
                        "/public/**",
                        "/console/**");
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
