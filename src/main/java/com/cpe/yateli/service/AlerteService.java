package com.cpe.yateli.service;

import com.cpe.yateli.exception.alerte.AlerteExistePasException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.Alerte;
import com.cpe.yateli.model.Role;
import com.cpe.yateli.model.SocketEventType;
import com.cpe.yateli.model.User;
import com.cpe.yateli.model.dto.MessageDto;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public interface AlerteService {

    List<Alerte> recupererNotifications(Principal moi) throws UserNonTrouveException;

    MessageDto notificationVue(Principal moi, String id) throws UserNonTrouveException, AlerteExistePasException;

    MessageDto supprimerNotification(Principal moi, String id) throws UserNonTrouveException, AlerteExistePasException;

    /**
     * Crée la notification en BDD puis envoie à l'utilisateur via la WebSocket
     * @param userConcerne : Utilisateur en question
     * @param type : Type d'évenement
     * @param message : Objet à envoyer
     */
    void creerNotification(User userConcerne, SocketEventType type, Object message);

    void creerNotificationRole(String role, SocketEventType type, Object message);

    void creerNotificationTousUser(SocketEventType type, Object message);

}
