package com.cpe.yateli.service;

import com.cpe.yateli.exception.artisan.*;
import com.cpe.yateli.exception.produit.ProduitABloquerException;
import com.cpe.yateli.exception.role.RoleExistePasException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.Artisan;
import com.cpe.yateli.model.Blocage;
import com.cpe.yateli.model.InfoFichier;
import com.cpe.yateli.model.User;
import com.cpe.yateli.model.dto.ArtisanDto;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.List;

@Service
public interface ArtisanService {

    /**
     * Supprime l'artisan associé à'lobjet User passé en paramètre s'il existe
     * @param user Instance de User associée à l'artisan à supprimer
     */
    void supprimerArtisanSelonUser(User user);

    List<Blocage> bloquerArtisan(Principal moi, int idArtisan, String raison) throws ArtisanExistePasException, UserNonTrouveException, ProduitABloquerException;

    Blocage autoriserArtisan(int idArtisan) throws ArtisanExistePasException, ArtisanNonBloqueException, UserNonTrouveException, ProduitABloquerException;

    InfoFichier recupererImage(int idArtisan) throws ArtisanExistePasException, SQLException, ArtisanSansImageException;

    Artisan modifierArtisan(ArtisanDto artisanDto, MultipartFile image) throws ArtisanExistePasException, IOException, SQLException, UserBloqueException;

    /**
     * Envoi une demande d'association User-Artisan à faire valider par un admin
     * @param artisanDto Objet permettant de récupérer des informations pour l'artisan
     * @param imageArtisan Fichier image à associer à l'artisan
     * @param moi Objet contenant des informations permettant d'identifier l'User
     * @return Retourne l'objet artisan prêt à être créé si accepté
     * @throws UserNonTrouveException Exception levée si aucun User n'est associé à l'adresse mail de l'objet "moi"
     * @throws IOException
     * @throws SQLException
     * @throws RoleExistePasException
     */
    Artisan demandeArtisan(ArtisanDto artisanDto, MultipartFile imageArtisan, Principal moi) throws UserNonTrouveException, IOException, SQLException, RoleExistePasException, UserBloqueException, ArtisanEnCoursAutorisationException;

    /**
     * Procède à l'acceptation de la demande d'un User de passer Artisan
     * @param moi Objet contenant les informations de l'User appelant la méthode
     * @param loginArtisan  Adresse mail associée au compte de l'User qui demande à passer Artisan
     * @return "Ok" Si tout s'est bien passé
     * @throws UserNonTrouveException Exception levée si aucun User n'est associé à l'adresse mail de l'objet "moi"
     * @throws ArtisanExistePasException Exception levée si aucun Artisan n'est associé à loginArtisan
     */
    String acceptationArtisan(Principal moi, String loginArtisan) throws UserNonTrouveException, ArtisanExistePasException, ArtisanDejaAutoriseException;

    String refuserArtisan(Principal moi, String loginArtisan, String raison) throws UserNonTrouveException, ArtisanExistePasException, ArtisanDejaAutoriseException;

    /**
     * Récupère la liste de tous les User dont la demande de passer Artisan n'a pas encore été acceptée
     * @return Liste de tous les artisons qui n'ont pas encore été acceptés comme tels
     */
    List<Artisan> getArtisansAaccepter();

    /**
     * Récupère un artisan à partir de son adresse mail
     * @param loginUser Adresse mail de l'artisan recherché
     * @return Instance de l'artisan recherché
     */
    Artisan getArtisan(String loginUser);

    /**
     * Enregistre une instance d'artisan en BDD
     * @param artisan  Instance de l'artisan que l'on veut enregistré
     */
    void enregistrerArtisan(Artisan artisan);

    /**
     * Renvoie toutes les instances d'artisans enregistrées en BDD
     * @return Liste contenant tous les artisans
     */
    List<Artisan> getTousLesArtisans();

}
