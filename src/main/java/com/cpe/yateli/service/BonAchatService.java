package com.cpe.yateli.service;

import com.cpe.yateli.model.Commande;
import org.springframework.stereotype.Service;

@Service
public interface BonAchatService {

    void invaliderBonsAchat(Commande commande);

    void attribuerBonAchatSelonCommande(Commande commande);

}