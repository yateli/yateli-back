package com.cpe.yateli.service;

import com.cpe.yateli.exception.colisMystere.DateDecouverteColisMystereException;
import com.cpe.yateli.exception.user.NonAbonneException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.ColisMystere;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public interface ColisMystereService {
    /**
     *  Créé le prochain colis mystère associé au User identifié par "moi"
     * @param moi Objet contenant l'adresse mail du User avec laquelle on pourra récupérer son instance en BDD
     * @return Retourne le colis mystère fraîchement créé
     * @throws UserNonTrouveException Exception si l'adresse mail n'est associé à aucun User de la BDD
     */
    ColisMystere creerColisMystere(Principal moi) throws UserNonTrouveException;

    /**
     *  Récupère le colis mystère pas encore récupéré de l'User identifié par moi, lance, une fois récupéré, la création du colis mystère suivant
     * @param moi Objet contenant l'adresse mail du User avec laquelle on pourra récupérer son instance en BDD
     * @return Retourne le colis mystère à récupérer
     * @throws UserNonTrouveException Exception si l'adresse mail n'est associé à aucun User de la BDD
     * @throws DateDecouverteColisMystereException Exception si la date de découverte du colis mystère n'a pas encore été atteinte
     * @throws NonAbonneException Exception si l'User ne possède pas de colis mystère (n'est pas abonné)
     */
    ColisMystere getColisMystere(Principal moi) throws UserNonTrouveException, DateDecouverteColisMystereException, NonAbonneException, UserBloqueException;

    void attribuerColisMystere();

    /**
     *  Supprime en BDD tous les colis mystères de la liste passée en paramètre
     * @param colisMysteres Liste de colis mystères à supprimer
     */
    void supprimerTousColisMystere(List<ColisMystere> colisMysteres);

}
