package com.cpe.yateli.service;

import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.Commande;
import com.cpe.yateli.model.User;
import com.cpe.yateli.model.dto.CommandeDto;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public interface CommandeService {

    Commande passerCommande(Principal moi, CommandeDto commande) throws Exception;

    List<Commande> getCommandesUser(Principal moi) throws UserNonTrouveException, UserBloqueException;

    void supprimerToutesCommandesUser(User user);

    void supprimerCommande(Commande commande);

}
