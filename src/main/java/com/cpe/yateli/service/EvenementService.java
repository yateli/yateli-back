package com.cpe.yateli.service;

import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.User;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public interface EvenementService {

    List<String> recupererEvenementsUser(String login) throws UserNonTrouveException;

    void enregistrerEvenement(User user, String log);

}
