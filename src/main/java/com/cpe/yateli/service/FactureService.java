package com.cpe.yateli.service;

import com.cpe.yateli.exception.facture.FactureExistePasException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.Commande;
import com.cpe.yateli.model.User;
import com.cpe.yateli.model.view.FactureView;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.SQLException;
import java.util.List;

@Service
public interface FactureService {

    FactureView genererFacture(Commande commande) throws Exception;

    List<FactureView> getFacturesUser(Principal moi) throws UserNonTrouveException, FactureExistePasException, UserBloqueException;

    Resource getFacture(Principal moi, int idFacture) throws SQLException, UserNonTrouveException, FactureExistePasException, UserBloqueException;

    void supprimerFacturesCommandesUser(User user);

}
