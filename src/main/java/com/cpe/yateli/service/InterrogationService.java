package com.cpe.yateli.service;

import com.cpe.yateli.exception.interrogation.*;
import com.cpe.yateli.exception.tag.TagExistePasException;
import com.cpe.yateli.model.Interrogation;
import com.cpe.yateli.model.dto.InterrogationActivesDto;
import com.cpe.yateli.model.dto.InterrogationDto;
import org.apache.commons.codec.EncoderException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface InterrogationService {

    Interrogation creerQuestion(InterrogationDto interrogationDto) throws EncoderException, QuestionExisteDejaException, TagExistePasException;

    String supprimerQuestion(Integer idQuestion) throws QuestionExistePasException;

    List<Interrogation> getInterrogationsActives();

    List<Interrogation> getToutesInterrogations();

    String activerQuestions(InterrogationActivesDto interrogationActivesDto) throws QuestionExistePasException, QuestionsActivesException, QuestionDejaActiveException;

    String desactiverQuestions(InterrogationActivesDto interrogationActivesDto) throws QuestionExistePasException, QuestionsActivesException, QuestionPasActiveException;

}
