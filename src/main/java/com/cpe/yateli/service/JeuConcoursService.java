package com.cpe.yateli.service;

import com.cpe.yateli.exception.jeuConcours.*;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.JeuConcours;
import com.cpe.yateli.model.User;
import com.cpe.yateli.model.dto.JeuConcoursDto;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public interface JeuConcoursService {

    JeuConcours creerJeuConcours(JeuConcoursDto jeuConcoursDto) throws JeuConcoursExisteDejaException;

    JeuConcours changerEtatExceptionnelJeuConcours(Integer idJeuConcours, boolean estFerme) throws JeuConcoursExistePasException, JeuConcoursDejaOuvertFermeException;

    JeuConcours participerJeuConcours(int idJeuConcours, Principal moi) throws UserNonTrouveException, JeuConcoursExistePasException, JeuConcoursFermeException, PointsFideliteInsuffisantException, JeuConcoursDejaInscritException, JeuConcoursFermeExceptionnelementException, UserBloqueException;

    User desinscrireJeuConcours(int idJeuConcours, Principal moi) throws UserNonTrouveException, JeuConcoursExistePasException, JeuConcoursParticipePasException, UserBloqueException;

    List<JeuConcours> recupererJeuConcours(Principal moi) throws UserNonTrouveException, UserBloqueException;

    /**
     * Retourne les concours même fermés aux Admin mais seulement les ouverts aux 2 autres profils
     * @param moi : Role de l'utilisateur de la demande
     * @return Liste qui contient tous les jeux concours
     */
    List<JeuConcours> recupererTousLesConcours(Principal moi) throws UserNonTrouveException, UserBloqueException;

}
