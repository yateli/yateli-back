package com.cpe.yateli.service;

import com.cpe.yateli.exception.commande.CommandeDejaPayeeException;
import com.cpe.yateli.exception.commande.CommandeNonTrouveeException;
import com.cpe.yateli.model.view.FactureView;
import com.paypal.base.rest.PayPalRESTException;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public interface PaiementService {

    String payerCommande(Principal moi, int commandeId) throws PayPalRESTException, CommandeNonTrouveeException, CommandeDejaPayeeException;

    FactureView effectuerPaiement(String payerId, String paymentId) throws Exception;

}
