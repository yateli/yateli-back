package com.cpe.yateli.service;

import com.cpe.yateli.exception.panier.ProduitInexistantException;
import com.cpe.yateli.exception.panier.QuantiteIncorrecteException;
import com.cpe.yateli.exception.produit.StockProduitInsuffisantException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.Panier;
import com.cpe.yateli.model.User;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
public interface PanierService {

    /**
     * Ajoute un produit au panier d'un utilisateur.
     * @param moi : Token permettant l'identification de l'utilisateur.
     * @param referenceProduit : Identifiant du produit a ajouter au panier.
     * @param quantite : Quantite du produit a ajouter au panier.
     * @return : Renvoie le panier de l'utilisateur une fois modifie.
     * @throws UserNonTrouveException : Propage une exception si l'utlisateur n'existe pas.
     * @throws ProduitInexistantException : Propage une exception si le produit a ajouter n'existe pas.
     * @throws QuantiteIncorrecteException : Propage une exception si la quantite est incorrecte.
     * @throws StockProduitInsuffisantException : Propage une exception si le stock du produit est insuffisant.
     */
    Panier ajoutProduit(Principal moi, String referenceProduit, int quantite) throws UserNonTrouveException, ProduitInexistantException, QuantiteIncorrecteException, StockProduitInsuffisantException, UserBloqueException;

    /**
     * Modifie la quantite d'un produit present dans le panier.
     * @param moi : Token permettant l'identification de l'utilisateur.
     * @param referenceProduit : Identifiant du produit dont la quantite est a modifier.
     * @param quantite : Nouvelle quantite a affecter au produit du panier.
     * @return : Renvoie le panier de l'utilisateur une fois modifie.
     * @throws UserNonTrouveException : Propage une exception si l'utlisateur n'existe pas.
     * @throws ProduitInexistantException : Propage une exception si le produit a ajouter n'existe pas.
     * @throws QuantiteIncorrecteException : Propage une exception si la quantite est incorrecte.
     * @throws StockProduitInsuffisantException : Propage une exception si le stock du produit est insuffisant.
     */
    Panier modifierQuantiteProduit(Principal moi, String referenceProduit, int quantite) throws UserNonTrouveException, ProduitInexistantException, QuantiteIncorrecteException, StockProduitInsuffisantException, UserBloqueException;

    /**
     * Retire un produit et sa quantite associee d'un panier
     * @param moi : Token permettant l'identification de l'utilisateur.
     * @param referenceProduit : Identifiant du produit a supprimer du panier.
     * @param quantite : Quantite a retirer du panier (s'il est egale ou superieure a la quantite presente dans le pnaier, le produit sera retire du panier)
     * @return : Renvoie le panier de l'utilisateur une fois modifie.
     * @throws UserNonTrouveException : Propage une exception si l'utlisateur n'existe pas.
     * @throws ProduitInexistantException : Propage une exception si le produit a ajouter n'existe pas.
     */
    Panier supprimerProduit(Principal moi, String referenceProduit, int quantite) throws UserNonTrouveException, ProduitInexistantException, UserBloqueException;

    /**
     * Calcule les points de fidelite potentiellement gagnes par l'utilisateur dans l'etat actuel de son panier.
     * @param user : Represente l'utilisateur dont on manipule le panier.
     * @return : Retourne le total de points de fidelite potentiels.
     */
    int getPointsDeFidelite(User user);

    /**
     * Supprime le contenu entier du panier d'un utilisateur.
     * @param user : Utilisateur dont on veut vider le panier.
     */
    void viderPanierUser(User user);

}
