package com.cpe.yateli.service;

import com.cpe.yateli.exception.phraseDuJour.PhraseDuJourException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.PhraseDuJour;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public interface PhraseDuJourService {

    PhraseDuJour creerPhraseDuJour(Principal moi, String phrase, String urlImage) throws UserNonTrouveException;

    PhraseDuJour modifierPhraseDuJour(Principal moi, String nouvellePhrase, String urlImage) throws UserNonTrouveException, PhraseDuJourException;

    String supprimerPhraseDuJour(Principal moi) throws UserNonTrouveException, PhraseDuJourException;

    PhraseDuJour getPhraseDuJour() throws PhraseDuJourException;

    List<PhraseDuJour> getToutesPhrasesDuJour() throws PhraseDuJourException;

}
