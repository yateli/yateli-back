package com.cpe.yateli.service;

import com.cpe.yateli.exception.artisan.ArtisanExistePasException;
import com.cpe.yateli.exception.artisan.ArtisanNonAutoriseProduitException;
import com.cpe.yateli.exception.panier.ProduitInexistantException;
import com.cpe.yateli.exception.produit.ProduitABloquerException;
import com.cpe.yateli.exception.produit.ProduitSansImageException;
import com.cpe.yateli.exception.produit.ReferenceIncorrecteSuppressionException;
import com.cpe.yateli.exception.produit.ReferenceProduitExisteDejaException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.*;
import com.cpe.yateli.model.dto.BloquerProduitDto;
import com.cpe.yateli.model.dto.ProduitArtisanDto;
import com.cpe.yateli.model.dto.ProduitDto;
import com.cpe.yateli.model.dto.ProduitSuppressionDto;
import com.cpe.yateli.model.view.ProduitView;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.util.List;

@Service
public interface ProduitService {

    void mettreAJourStockProduit(Produit produit, int quantiteEnMoins) throws IOException;

    /**
     * Renvoie tous les produits non bloqués
     * @return : liste des produits
     */
    List<ProduitView> getTousLesProduits();

    /**
     * Renvoie tous les produits dont ceux bloqués
     * @return : liste des produits
     */
    List<Produit> getTousLesProduitsAvecBloques();

    List<ProduitView> getProduitsArtisan(Integer idArtisan);

    List<Blocage> bloquerProduits(Principal moi, BloquerProduitDto produits) throws ProduitABloquerException, UserNonTrouveException;

    List<Produit> autoriserProduits(BloquerProduitDto produits) throws ProduitABloquerException, UserNonTrouveException;

    InfoFichier recupererImage(String reference) throws ProduitInexistantException, SQLException, ProduitSansImageException;

    Produit creerProduit(ProduitArtisanDto produitDto, MultipartFile image) throws ReferenceProduitExisteDejaException, ArtisanExistePasException, IOException, SQLException, UserBloqueException;

    Produit modifierProduit(ProduitDto produitDto, MultipartFile image) throws ProduitInexistantException, IOException, SQLException, UserBloqueException;

    Produit modifierProduitParArtisan(ProduitArtisanDto produitDto, MultipartFile image) throws ProduitInexistantException, IOException, SQLException, UserBloqueException;

    String supprimerProduit(Principal moi, ProduitSuppressionDto produitSuppressionDto) throws ReferenceIncorrecteSuppressionException, UserNonTrouveException, ArtisanNonAutoriseProduitException, UserBloqueException;

    Produit affectationColisMystere(User utilisateur, Tag preference);

    ProduitView getDetailsProduit(String reference) throws ProduitInexistantException;

}
