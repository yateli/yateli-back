package com.cpe.yateli.service;

import com.cpe.yateli.exception.excel.AucuneDonneeExcelException;
import com.cpe.yateli.exception.excel.ErreurGenerationException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.InfoFichier;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.Principal;

@Service
public interface StatistiquesService {

    InfoFichier genererExcel(Principal moi, String nomFichier, Long timestampMinimum, String login) throws UserNonTrouveException, IOException, ErreurGenerationException, AucuneDonneeExcelException;

}
