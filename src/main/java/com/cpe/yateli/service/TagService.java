package com.cpe.yateli.service;

import com.cpe.yateli.exception.tag.TagExisteDejaException;
import com.cpe.yateli.exception.tag.TagExistePasException;
import com.cpe.yateli.model.Tag;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TagService {

    Tag creerTag(String nom) throws TagExisteDejaException;

    Tag modifierTag(int id, String nouveauNom) throws TagExistePasException;

    String supprimerTag(int id) throws TagExistePasException;

    List<Tag> getTousLesTags();

}
