package com.cpe.yateli.service;

import com.cpe.yateli.exception.role.RoleExistePasException;
import com.cpe.yateli.exception.user.*;
import com.cpe.yateli.model.User;
import com.cpe.yateli.model.dto.UserDto;
import com.cpe.yateli.model.dto.UserModificationDto;
import com.cpe.yateli.model.view.UserView;
import com.cpe.yateli.service.usersConnectes.UserConnecte;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;

@Service
public interface UserService {

    /**
     * Recupere le token identifiant l'utilisateur actuellement connecte.
     * @param moi : Objet representant l'utilisateur actif.
     * @return : Token identifiant l'utilisateur connecte.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     */
    String getTokenWebsocket(Principal moi) throws UserNonTrouveException, UserBloqueException;

    /**
     * Recupere une liste d'utilisateurs connectes selon leur role.
     * @param moi : Objet representant l'utilisateur actif.
     * @param role : Roles des utilisateurs connectes a recuperer.
     * @return : Liste des utilisateurs dont le role correspond au parametre d'entree connectes.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     */
    List<UserConnecte> getConnectedUsers(Principal moi, String role) throws UserNonTrouveException;

    /**
     * Permet a un admnistrateur de deconnecte un utilisateur en specifiant la raison.
     * @param login : Adresse mail de l'utilisateur a deconnecter.
     * @param raison : Raison specifiee par l'administrateur.
     * @return : Texte exposant la raison de la deconnexion.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     */
    String logoutSpecificUser(String login,String raison) throws UserNonTrouveException, UserNonConnecteException;

    /**
     * Cree un compte utilisateur depuis un formulaire.
     * @param userJson : Objet de type DTO comportant les informations necessaires a la creation d'un utilisateur.
     * @return : Utilisateur ci-cree.
     * @throws UserExisteDejaException : L'utilisateur qu'on tente de creer existe deja.
     * @throws PhoneFormatIncorrectException : Le format du numero de telephone est incorrect.
     */
    User creerUser(UserDto userJson) throws UserExisteDejaException, PhoneFormatIncorrectException, MailNonCorrectException, FormatMotDePasseIncorrecteException;

    /**
     * Modifie les informations d'un utilisateur.
     * @param moi : Objet representant l'utilisateur actif.
     * @param userDto : Objet DTO comportant les informations de l'utilisateur a modifier.
     * @return : L'utilisateur une fois modifie.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     */
    User modifierUser(Principal moi, UserModificationDto userDto) throws UserNonTrouveException, UserBloqueException;

    /**
     * Confirme l'inscription d'un nouvel utilisateur.
     * @param login : Adresse mail de l'utilisateur a activer.
     * @param password : Mot de passe de l'utilisateur.
     * @return : Utilisateur maintenant active.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     * @throws MotDePasseIncorrectException : Le mot de passe n'est pas le bon.
     * @throws UserDejaActiveException : L'utilisateur est deja active.
     */
    User activerUser(String login, String password) throws UserNonTrouveException, MotDePasseIncorrectException, UserDejaActiveException;

    /**
     * Permet a l'administrateur de desactiver un utilisateur.
     * @param login : Adresse mail de l'utilisateur a desactiver.
     * @return : Message OK si l'utilisateur a bien ete desactive.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     */
    String desactiverUser(String login) throws UserNonTrouveException;

    /**
     * Permet a l'administrateur de réactiver un utilisateur.
     * @param login : Adresse mail de l'utilisateur a desactiver.
     * @return : Message OK si l'utilisateur a bien ete desactive.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     */
    String reactiverUser(String login) throws UserNonTrouveException;

    /**
     * Permet de generer un code de double authentification pour un utilisateur qui a oublie le sien.
     * @param login : Adresse mail de l'utilisateur.
     * @return : Message OK si le ode de double authentification a bien ete genere.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     */
    String motDePasseOublie(String login) throws UserNonTrouveException, UserBloqueException;

    /**
     * Verifie le code de double authentification.
     * @param login : Adresse mail de l'utilisateur.
     * @param code : Code entre par l'utilisateur.
     * @return : Message OK si le code entre par l'utilisateur correspond au code de double authentification.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     * @throws CodeVerificationIncorrectException : Le code entre par l'utilisateur n'est pas le bon.
     */
    String verificationCodeRecuperation(String login, String code) throws UserNonTrouveException, CodeVerificationIncorrectException, UserBloqueException;

    /**
     * Enregistre le nouveau mot de passe entre par l'utilisateur.
     * @param code : Code de double authentification servant ici d'identifiant.
     * @param nouveauPassword : Nouveau mot de passe.
     * @return : Message OK si le nouveau de mot de passe est bien enregistre.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     */
    String nouveauPasswordAvecCode(String code, String nouveauPassword) throws UserNonTrouveException, UserBloqueException;

    /**
     * Supprimer un utilisateur.
     * @param moi : Objet representant l'utilisateur actif.
     * @return : Message OK si l'utilisateur a bien ete supprime.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     */
    String supprimerUser(Principal moi) throws UserNonTrouveException, UserBloqueException;

    String supprimerSpecificUser(Principal moi, String loginUser) throws UserNonTrouveException;

    /**
     * Retourne les informations de l'utilisateur actif.
     * @param moi : Objet representant l'utilisateur actif.
     * @return : Format "Vue" contenant les informations d'un utilisateur dont le front aura besoin.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     */
    UserView getUser(Principal moi) throws UserNonTrouveException, UserBloqueException;

    /**
     * Retourne le contenu complet de la rubrique User (utilisateurs) de la base de donnees.
     * @return : Totalite des utilisateurs enregistres sous forme de List.
     */
    List<User> getAllUsers();

    /**
     * Recupere tous les utilisateurs ayant le meme role.
     * @param role : Role des utilisateurs que l'on veut recuperer.
     * @return : List comprenant tous les utilisateurs ayant pour role celui defini en parametre.
     * @throws RoleExistePasException : Le role entre en parametre n'est pas un role existant.
     */
    List<User> getUsersSelonRole(String role) throws RoleExistePasException;

    /**
     * Permet a un utilisateur de s'abonner a la boite mystere.
     * @param moi : Objet representant l'utilisateur actif.
     * @return : L'utilisateur maintenant abonne.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     * @throws EstDejaAbonneException : L'utilisateur est deja abonne.
     */
    User abonnerUser(Principal moi) throws UserNonTrouveException, EstDejaAbonneException, UserBloqueException;

    /**
     * Permet a un utilisateur de se desabonner de la boite mystere.
     * @param moi : Objet representant l'utilisateur actif.
     * @return : L'utilisateur maintenant desabonne.
     * @throws UserNonTrouveException : L'utilisateur n'est pas enregistre.
     */
    User desabonnerUser(Principal moi) throws UserNonTrouveException, UserBloqueException;

}