package com.cpe.yateli.service.excel;

import com.cpe.yateli.exception.excel.AucuneDonneeExcelException;
import com.cpe.yateli.model.statistiques.InterrogationStat;
import com.cpe.yateli.model.statistiques.Questionnaire;
import com.cpe.yateli.repository.QuestionnaireRepository;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.rowset.serial.SerialBlob;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

@Service
public class ExcelService {

    @Value("${files.facture}")
    private String cheminDossierOuvert;

    @Autowired
    private QuestionnaireRepository questionnaireRepository;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private static List<String> header = new ArrayList<>(Arrays.asList("Identifiant", "Date", "Login"));
    private static List<String> headerSheet2 = new ArrayList<>(Arrays.asList("Question", "Réponse", "Fréquence", "Tags associés"));

    public MultipartFile generateExcel(String nameFile, Date minimumDate, String login) throws AucuneDonneeExcelException {

        List<Questionnaire> questionnaires = null;

        if(login != null && !login.isEmpty()){
            questionnaires = questionnaireRepository.findAllByDateAfterAndMailIs(minimumDate.toInstant(),login);
        }else{
            //Récupère toutes les questions ayant au moins été répondues une fois
            questionnaires = questionnaireRepository.findAllByDateAfter(minimumDate.toInstant());
        }

        //Récupère toutes les questions de façon unique et avec un ordre
        Set<String> questions = new LinkedHashSet<>();
        questionnaires
                .forEach(q -> q.getInterrogations()
                        .forEach(i -> questions.add(i.getQuestion())));

        //Conversion Set en List (utile après)
        List<String> questionsList = new ArrayList<>(questions);

        //Vérifie s'il y a des questions à afficher
        if(questionsList.size() == 0){
            throw AucuneDonneeExcelException.creerAvec(HttpStatus.PRECONDITION_FAILED);
        }

        //Ajout de ces nouveaux headers de question
        header.addAll(questionsList);

        //Création Excel
        Workbook workbook = new XSSFWorkbook();
        CreationHelper createHelper = workbook.getCreationHelper();

        Sheet sheet = workbook.createSheet("Récapitulatif");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.DARK_RED.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        //Remplit la première ligne
        for (int i = 0; i < header.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(header.get(i));
            cell.setCellStyle(headerCellStyle);
        }

        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

        //Récupération Tags pour une réponse
        Map<String,List<String>> listTagsPourReponse = new HashMap<>();

        //Récupération Fréquences
        Map<String,List<MutablePair<String,Integer>>> coupleQuestionReponses = new HashMap<>();

        for(int rowNum = 1; rowNum <= questionnaires.size(); rowNum++){

            Questionnaire questionnaire = questionnaires.get(rowNum - 1);

            Row row = sheet.createRow(rowNum);

            row.createCell(0).setCellValue(questionnaire.getId());

            Cell dateQuestion = row.createCell(1);
            dateQuestion.setCellValue(Date.from(questionnaire.getDate()));
            dateQuestion.setCellStyle(dateCellStyle);

            row.createCell(2).setCellValue(questionnaire.getMail());

            boolean reponseObtenue;
            for(int numQuestion = 0; numQuestion < questionsList.size(); numQuestion++){

                reponseObtenue = false;

                StringBuilder sb = new StringBuilder();
                for(InterrogationStat interrogationStat : questionnaire.getInterrogations()){
                    if(interrogationStat.getQuestion().equalsIgnoreCase(questionsList.get(numQuestion))){
                        if(coupleQuestionReponses.get(interrogationStat.getQuestion()) != null){
                            List<MutablePair<String,Integer>> reponsesFrequences = coupleQuestionReponses.get(interrogationStat.getQuestion());

                            boolean ajoutNouvelleReponse = true;

                            for(MutablePair<String,Integer> reponseFreq : reponsesFrequences){
                                if(reponseFreq.getLeft().equals(interrogationStat.getReponse().getReponse())){
                                    int newValue = reponseFreq.getRight() + 1;
                                    reponseFreq.setRight(newValue);
                                    ajoutNouvelleReponse = false;
                                }
                            }

                            if(ajoutNouvelleReponse){
                                MutablePair<String,Integer> nouvelleReponse = new MutablePair<>();
                                nouvelleReponse.setLeft(interrogationStat.getReponse().getReponse());
                                nouvelleReponse.setRight(1);
                                reponsesFrequences.add(nouvelleReponse);
                            }

                        }else{
                            List<MutablePair<String,Integer>> reponsesFrequences = new ArrayList<>();
                            MutablePair<String,Integer> reponseFrequence = new MutablePair<>();
                            reponseFrequence.setLeft(interrogationStat.getReponse().getReponse());
                            reponseFrequence.setRight(1);
                            reponsesFrequences.add(reponseFrequence);
                            coupleQuestionReponses.put(interrogationStat.getQuestion(), reponsesFrequences);
                        }

                        listTagsPourReponse.computeIfAbsent(interrogationStat.getReponse().getReponse(), k -> interrogationStat.getReponse().getTags());

                        if(sb.length() > 0){ sb.append(";"); }
                        sb.append(interrogationStat.getReponse().getReponse());
                        reponseObtenue = true;
                    }
                }

                if(!reponseObtenue){
                    row.createCell(numQuestion + 3).setCellValue("-");
                }else{
                    row.createCell(numQuestion + 3).setCellValue(sb.toString());
                }

            }

        }

        // Redimensionne toutes les colonnes pour être adaptées au contenu présent dans les cellules
        for (int i = 0; i < header.size(); i++) {
            sheet.autoSizeColumn(i);
        }

        //Deuxième onglet
        Sheet sheet2 = workbook.createSheet("Fréquence");

        Row headerRowSheet2 = sheet2.createRow(0);

        //Remplit la première ligne
        for (int i = 0; i < headerSheet2.size(); i++) {
            Cell cell = headerRowSheet2.createCell(i);
            cell.setCellValue(headerSheet2.get(i));
            cell.setCellStyle(headerCellStyle);
        }

        int rowSheet2 = 1;
        for (Map.Entry<String,List<MutablePair<String,Integer>>> entry : coupleQuestionReponses.entrySet()) {

            String question = entry.getKey();
            List<MutablePair<String,Integer>> listeReponseFrequence = entry.getValue();

            for(MutablePair<String,Integer> reponseFrequence : listeReponseFrequence){
                Row row = sheet2.createRow(rowSheet2);
                List<String> tagsReponse = listTagsPourReponse.get(reponseFrequence.getLeft());

                row.createCell(0).setCellValue(question);
                row.createCell(1).setCellValue(reponseFrequence.getLeft());
                row.createCell(2).setCellValue(reponseFrequence.getRight());

                StringBuilder tagsMultiples = new StringBuilder();
                for(String tag : tagsReponse){
                    if(tagsMultiples.length() > 0){ tagsMultiples.append(";"); }
                    tagsMultiples.append(tag);
                }
                row.createCell(3).setCellValue(tagsMultiples.toString());
                rowSheet2++;
            }

        }

        for (int i = 0; i < headerSheet2.size(); i++) {
            sheet2.autoSizeColumn(i);
        }

        File excel = null;
        FileOutputStream fileOut = null;
        MultipartFile multipartFile = null;
        String fileName = nameFile + ".xlsx";

        try {

            //final File outputFile = File.createTempFile(fileName, ".xlsx");
            excel = new File(cheminDossierOuvert + fileName);
            fileOut = new FileOutputStream(excel);
            workbook.write(fileOut);
            multipartFile = new MockMultipartFile("file", fileName, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", IOUtils.toByteArray(new FileInputStream(excel)));

            fileOut.close();
            workbook.close();

            if(!excel.delete()){
                log.error("Erreur pendant la suppression du fichier Excel");
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return multipartFile;

    }

}
