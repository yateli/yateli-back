package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.alerte.AlerteExistePasException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.Alerte;
import com.cpe.yateli.model.Role;
import com.cpe.yateli.model.SocketEventType;
import com.cpe.yateli.model.User;
import com.cpe.yateli.model.dto.MessageDto;
import com.cpe.yateli.repository.AlerteRepository;
import com.cpe.yateli.repository.RoleRepository;
import com.cpe.yateli.repository.UserRepository;
import com.cpe.yateli.service.AlerteService;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.websocket.WebSocketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AlerteServiceImpl implements AlerteService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AlerteRepository alerteRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private WebSocketService webSocketService;

    @Autowired
    private EvenementService evenementService;

    @Override
    public List<Alerte> recupererNotifications(Principal moi) throws UserNonTrouveException {

        User user = userRepository.findUserByMail(moi.getName());

        if(user == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        List<Alerte> alertes = alerteRepository.findAllByLoginUser(user.getMail());

        if(alertes == null){
            return new ArrayList<>(); //Retourne liste vide si aucune notification en BDD
        }

        return alertes;

    }

    @Override
    public MessageDto notificationVue(Principal moi, String id) throws UserNonTrouveException, AlerteExistePasException {

        User user = userRepository.findUserByMail(moi.getName());

        if(user == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        Alerte alerte = alerteRepository.findAlerteById(id);

        if(alerte == null){
            throw AlerteExistePasException.creerAvec(HttpStatus.NOT_FOUND, id);
        }

        alerte.setVu(true);
        alerte.setDate_vu(Instant.now().plus(1, ChronoUnit.HOURS));
        alerte.setTimestamp_vu(new Date().getTime());

        alerteRepository.save(alerte);

        String logCreated = "Notification " + alerte.getId() + " a été vue par " + alerte.getLoginUser() + " à " + Timestamp.from(alerte.getDate_vu()).getTime();
        log.info(logCreated);
        evenementService.enregistrerEvenement(user, logCreated);

        return new MessageDto("OK");

    }

    @Override
    public MessageDto supprimerNotification(Principal moi, String id) throws UserNonTrouveException, AlerteExistePasException {

        User user = userRepository.findUserByMail(moi.getName());

        if(user == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        Alerte alerte = alerteRepository.findAlerteById(id);

        if(alerte == null){
            throw AlerteExistePasException.creerAvec(HttpStatus.NOT_FOUND, id);
        }

        alerteRepository.delete(alerte);

        String logCreated = "Notification " + alerte.getId() + " supprimée de la base Mongo";
        log.info(logCreated);
        evenementService.enregistrerEvenement(user, logCreated);

        return new MessageDto("OK");

    }

    @Override
    public void creerNotification(User userConcerne, SocketEventType type, Object message) {
        Alerte alerte = this.creerAlerte(userConcerne,type,message);
        webSocketService.envoyerUserSpecifique(alerte.getLoginUser(), alerte);
    }

    @Override
    public void creerNotificationRole(String role, SocketEventType type, Object message) {
        Role roleDestinataire = roleRepository.findRoleByNom(role);
        for(User user : roleDestinataire.getUsers()){
            Alerte alerte = this.creerAlerte(user, type, message);
            webSocketService.envoyerUserSpecifique(user.getMail(), alerte);
        }
    }

    @Override
    public void creerNotificationTousUser(SocketEventType type, Object message) {
        List<User> users = userRepository.findAll();
        for(User user : users){
            Alerte alerte = this.creerAlerte(user, type, message);
            webSocketService.envoyerUserSpecifique(user.getMail(), alerte);
        }
    }

    private Alerte creerAlerte(User userConcerne, SocketEventType type, Object message){
        Alerte alerte = Alerte.builder()
                .date_creation(Instant.now().plus(1, ChronoUnit.HOURS))
                .timestamp_creation(new Date().getTime())
                .loginUser(userConcerne.getMail())
                .type(type)
                .message(message)
                .vu(false)
                .date_vu(null)
                .timestamp_vu(0)
                .build();

        alerteRepository.save(alerte);

        String logCreated = "Notification créée de type " + alerte.getType().getLabel() + " pour " + alerte.getLoginUser() + " et envoyée à la websocket";
        log.info(logCreated);
        User user = userRepository.findUserByMail(alerte.getLoginUser());
        evenementService.enregistrerEvenement(user, logCreated);

        return alerte;

    }

}
