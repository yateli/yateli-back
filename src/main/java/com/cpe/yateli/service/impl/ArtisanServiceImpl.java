package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.artisan.*;
import com.cpe.yateli.exception.produit.ProduitABloquerException;
import com.cpe.yateli.exception.role.RoleExistePasException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.*;
import com.cpe.yateli.model.dto.ArtisanDto;
import com.cpe.yateli.model.dto.BloquerProduitDto;
import com.cpe.yateli.model.view.ProduitView;
import com.cpe.yateli.repository.*;
import com.cpe.yateli.service.AlerteService;
import com.cpe.yateli.service.ArtisanService;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.ProduitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.security.Principal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ArtisanServiceImpl implements ArtisanService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ArtisanRepository artisanRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LocalisationRepository localisationRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private AlerteService alerteService;

    @Autowired
    private ProduitService produitService;

    @Autowired
    private BlocageRepository blocageRepository;

    @Autowired
    private EvenementService evenementService;

    @Override
    public void supprimerArtisanSelonUser(User user) {

        Artisan artisanASupprimer = artisanRepository.findByUser(user);

        if (artisanASupprimer != null) {
            artisanRepository.delete(artisanASupprimer);

            String logCreated = "Artisan " + artisanASupprimer.getUser().getMail() + " supprimé";
            log.info(logCreated);
            evenementService.enregistrerEvenement(artisanASupprimer.getUser(), logCreated);
        }

    }

    @Override
    public List<Blocage> bloquerArtisan(Principal moi, int idArtisan, String raison) throws ArtisanExistePasException, UserNonTrouveException, ProduitABloquerException {

        User admin = userRepository.findUserByMail(moi.getName());

        if (admin == null) {
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND, moi.getName());
        }

        Artisan artisan = artisanRepository.findById(idArtisan);

        if (artisan == null) {
            throw ArtisanExistePasException.creerAvec(HttpStatus.NOT_FOUND, String.valueOf(idArtisan));
        }

        List<Blocage> nouveauxBlocages = new ArrayList<>();

        if (artisan.getBlocages() == null) {
            artisan.setBlocages(new ArrayList<>());
        }

        if (artisan.getBlocages().size() == 0 || !artisan.getBlocages().get(artisan.getBlocages().size() - 1).isActif()) {
            Blocage blocage = Blocage.builder()
                    .date(new Timestamp(new Date().getTime()))
                    .admin(admin)
                    .typeBlocage(TypeBlocage.ARTISAN)
                    .notification(null)
                    .actif(true)
                    .build();

            if (!raison.isEmpty()) {
                blocage.setRaison(raison);
            }

            blocageRepository.save(blocage);
            nouveauxBlocages.add(blocage);

            artisan.getBlocages().add(blocage);
            artisanRepository.save(artisan);

            artisan.getUser().setEstActive(false);
            userRepository.save(artisan.getUser());

            //Blocage des produits de l'artisan
            List<String> referencesProduitArtisan = produitService.getProduitsArtisan(idArtisan).stream()
                    .map(ProduitView::getReference)
                    .collect(Collectors.toList());
            if(referencesProduitArtisan != null && referencesProduitArtisan.size() > 0){
                BloquerProduitDto bloquerProduitDto = BloquerProduitDto.builder()
                        .references(referencesProduitArtisan)
                        .raison(raison)
                        .build();
                nouveauxBlocages.addAll(produitService.bloquerProduits(moi, bloquerProduitDto));
            }

            String logCreated = "La connexion de l'artisan " + artisan.getUser().getMail() + " a été bloquée ainsi que tous ses produits";
            log.info(logCreated);
            evenementService.enregistrerEvenement(artisan.getUser(), logCreated);

            alerteService.creerNotification(artisan.getUser(), SocketEventType.BLOCAGE_ARTISAN, raison);

        }

        return nouveauxBlocages;

    }

    @Override
    public Blocage autoriserArtisan(int idArtisan) throws ArtisanExistePasException, ArtisanNonBloqueException, UserNonTrouveException, ProduitABloquerException {

        Artisan artisan = artisanRepository.findById(idArtisan);

        if (artisan == null) {
            throw ArtisanExistePasException.creerAvec(HttpStatus.NOT_FOUND, String.valueOf(idArtisan));
        }

        if (artisan.getBlocages() == null || artisan.getBlocages().size() == 0) {
            throw ArtisanNonBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, artisan.getUser().getMail());
        }

        Blocage dernierBlocage = artisan.getBlocages().get(artisan.getBlocages().size() - 1);

        if (!dernierBlocage.isActif()) {
            throw ArtisanNonBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, artisan.getUser().getMail());
        }

        dernierBlocage.setActif(false);
        blocageRepository.save(dernierBlocage);

        artisan.getUser().setEstActive(true);
        userRepository.save(artisan.getUser());

        //Autorisation des produits de l'artisan
        List<String> referencesProduitArtisan = produitService.getProduitsArtisan(idArtisan).stream()
                .map(ProduitView::getReference)
                .collect(Collectors.toList());
        if(referencesProduitArtisan != null && referencesProduitArtisan.size() > 0){
            BloquerProduitDto autoriserProduitDto = BloquerProduitDto.builder()
                    .references(referencesProduitArtisan)
                    .raison(null)
                    .build();
            produitService.autoriserProduits(autoriserProduitDto);
        }

        String logCreated = "L'artisan " + artisan.getUser().getMail() + " est de nouveau actif avec tous ses produits";
        log.info(logCreated);
        evenementService.enregistrerEvenement(artisan.getUser(), logCreated);

        return dernierBlocage;

    }

    @Override
    public InfoFichier recupererImage(int idArtisan) throws ArtisanExistePasException, SQLException, ArtisanSansImageException {

        Artisan artisan = artisanRepository.findById(idArtisan);

        if (artisan == null) {
            throw ArtisanExistePasException.creerAvec(HttpStatus.NOT_FOUND, String.valueOf(idArtisan));
        }

        if (artisan.getPhotoArtisan() == null) {
            throw ArtisanSansImageException.creerAvec(HttpStatus.NOT_FOUND, artisan.getUser().getMail());
        }

        return new InfoFichier(artisan.getExtensionImage(), new InputStreamResource(artisan.getPhotoArtisan().getBinaryStream()));

    }

    @Override
    public Artisan modifierArtisan(ArtisanDto artisanDto, MultipartFile image) throws ArtisanExistePasException, IOException, SQLException, UserBloqueException {

        Artisan artisan = artisanRepository.findByUser_Mail(artisanDto.getMail());

        if (artisan == null) {
            throw ArtisanExistePasException.creerAvec(HttpStatus.NOT_FOUND, artisanDto.getMail());
        }

        if(!artisan.isEstActif()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, artisan.getUser().getMail());
        }

        if (artisanDto.getTelephone() != null) {
            artisan.setTelephone(artisanDto.getTelephone());
        }

        //Localisation
        Localisation localisation = artisan.getEmplacement();

        if (artisanDto.getLatitude() != null) {
            localisation.setLatitude(artisanDto.getLatitude());
        }

        if (artisanDto.getLongitude() != null) {
            localisation.setLongitude(artisanDto.getLongitude());
        }

        if (artisanDto.getNationalite() != null) {
            localisation.setPays(artisanDto.getNationalite());
        }

        localisationRepository.save(localisation);

        if (image != null) {
            artisan.setExtensionImage(image.getContentType());
            artisan.setPhotoArtisan(new SerialBlob(image.getBytes()));
        }

        artisanRepository.save(artisan);

        String logCreated = "L'artisan " + artisan.getUser().getMail() + "  a été modifié à " + String.valueOf(new Date().getTime());
        log.info(logCreated);
        evenementService.enregistrerEvenement(artisan.getUser(), logCreated);

        return artisan;

    }

    @Override
    public Artisan demandeArtisan(ArtisanDto artisanDto, MultipartFile imageArtisan, Principal moi) throws UserNonTrouveException, IOException, SQLException, RoleExistePasException, UserBloqueException, ArtisanEnCoursAutorisationException {

        User user = userRepository.findUserByMail(moi.getName());

        if (user == null) {
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND, moi.getName());
        }

        if(!user.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, user.getMail());
        }

        List<String> loginsArtisanEnCoursAcceptation = getArtisansAaccepter().stream()
                .map(a -> a.getUser().getMail())
                .collect(Collectors.toList());

        if(loginsArtisanEnCoursAcceptation.size() > 0 && loginsArtisanEnCoursAcceptation.contains(artisanDto.getMail())){
            throw ArtisanEnCoursAutorisationException.creerAvec(HttpStatus.PRECONDITION_FAILED, artisanDto.getMail());
        }

        //Localisation
        Localisation localisation = new Localisation();

        if (artisanDto.getLatitude() != null) {
            localisation.setLatitude(artisanDto.getLatitude());
        }

        if (artisanDto.getLongitude() != null) {
            localisation.setLongitude(artisanDto.getLongitude());
        }

        if (artisanDto.getNationalite() != null) {
            localisation.setPays(artisanDto.getNationalite());
        }

        localisationRepository.save(localisation);


        User userToTransform = userRepository.findUserByMail(artisanDto.getMail());

        if (userToTransform == null) {
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND, artisanDto.getMail());
        }

        Artisan artisan = Artisan.builder()
                .user(userToTransform)
                .emplacement(localisation)
                .nationalite(artisanDto.getNationalite())
                .telephone(artisanDto.getTelephone())
                .build();

        if (imageArtisan != null) {
            artisan.setExtensionImage(imageArtisan.getContentType());
            artisan.setPhotoArtisan(new SerialBlob(imageArtisan.getBytes()));
        }

        artisanRepository.save(artisan);

        alerteService.creerNotificationRole("ADMIN", SocketEventType.DEMANDE_ARTISAN, artisan.getUser().getMail());

        String logCreated = "Demande link Artisan/User envoyée aux admins par " + artisan.getUser().getMail();
        log.info(logCreated);
        evenementService.enregistrerEvenement(artisan.getUser(), logCreated);

        return artisan;

    }

    @Override
    public String acceptationArtisan(Principal moi, String loginArtisan) throws UserNonTrouveException, ArtisanExistePasException, ArtisanDejaAutoriseException {

        User admin = userRepository.findUserByMail(moi.getName());

        if (admin == null) {
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND, moi.getName());
        }

        Artisan artisan = artisanRepository.findByUser_Mail(loginArtisan);

        if (artisan == null) {
            throw ArtisanExistePasException.creerAvec(HttpStatus.NOT_FOUND, loginArtisan);
        }

        if(artisan.isEstActif()){
            throw ArtisanDejaAutoriseException.creerAvec(HttpStatus.PRECONDITION_FAILED, artisan.getUser().getMail());
        }

        //Suppression role USER classique
        Role roleUser = roleRepository.findRoleByNom("USER");
        if(roleUser != null){
            roleUser.getUsers().remove(artisan.getUser());
            roleRepository.save(roleUser);
        }

        //Affectation role artisan
        Role role = roleRepository.findRoleByNom("ARTISAN");
        if (role == null) {
            role = Role.builder()
                    .nom("ARTISAN")
                    .users(new ArrayList<>())
                    .build();
        }
        role.getUsers().add(artisan.getUser());
        artisan.getUser().setRole(role);
        roleRepository.save(role);

        userRepository.save(artisan.getUser());

        artisan.setEstActif(true);

        artisanRepository.save(artisan);

        alerteService.creerNotification(artisan.getUser(), SocketEventType.ACCEPTATION_ARTISAN, artisan.getUser().getMail());

        String logCreated = "Demande link Artisan/User pour " + artisan.getUser().getMail() + " acceptée par admin " + admin.getMail();
        log.info(logCreated);
        evenementService.enregistrerEvenement(admin, logCreated);

        return "OK";

    }

    @Override
    public String refuserArtisan(Principal moi, String loginArtisan, String raison) throws UserNonTrouveException, ArtisanExistePasException, ArtisanDejaAutoriseException {

        User admin = userRepository.findUserByMail(moi.getName());

        if (admin == null) {
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND, moi.getName());
        }

        Artisan artisan = artisanRepository.findByUser_Mail(loginArtisan);

        if (artisan == null) {
            throw ArtisanExistePasException.creerAvec(HttpStatus.NOT_FOUND, loginArtisan);
        }

        if(artisan.isEstActif()){
            throw ArtisanDejaAutoriseException.creerAvec(HttpStatus.PRECONDITION_FAILED, artisan.getUser().getMail());
        }

        Localisation localisation = artisan.getEmplacement();
        localisationRepository.delete(localisation);

        artisanRepository.delete(artisan);

        alerteService.creerNotification(artisan.getUser(), SocketEventType.REFUS_ARTISAN, raison);

        String logCreated = "Demande link Artisan/User pour " + artisan.getUser().getMail() + " refusée par admin" + admin.getMail();
        log.info(logCreated);
        evenementService.enregistrerEvenement(artisan.getUser(), logCreated);

        return "OK";
    }

    @Override
    public List<Artisan> getArtisansAaccepter() {
        return artisanRepository.findAll().stream()
                .filter(p -> !p.isEstActif())
                .collect(Collectors.toList());
    }

    @Override
    public Artisan getArtisan(String loginUser) {
        return artisanRepository.findByUser_Mail(loginUser);
    }

    @Override
    public void enregistrerArtisan(Artisan artisan) {
        artisanRepository.save(artisan);
    }

    @Override
    public List<Artisan> getTousLesArtisans() {
        return artisanRepository.findAll().stream()
                .filter(Artisan::isEstActif)
                .collect(Collectors.toList());
    }

}
