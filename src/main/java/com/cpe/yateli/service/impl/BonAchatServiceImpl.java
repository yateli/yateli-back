package com.cpe.yateli.service.impl;

import com.cpe.yateli.model.BonAchat;
import com.cpe.yateli.model.Client;
import com.cpe.yateli.model.Commande;
import com.cpe.yateli.model.User;
import com.cpe.yateli.repository.BonAchatRepository;
import com.cpe.yateli.repository.CommandeRepository;
import com.cpe.yateli.repository.UserRepository;
import com.cpe.yateli.service.BonAchatService;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.mail.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Service
public class BonAchatServiceImpl implements BonAchatService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final static String offreUnAn = "BON-OFFRE-UN-AN-";

    @Autowired
    private BonAchatRepository bonAchatRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CommandeRepository commandeRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private EvenementService evenementService;

    @Override
    public void invaliderBonsAchat(Commande commande) {
        List<BonAchat> bonAchats = commande.getBonsAchat(); // Nous pouvons une fois le paiement effectué invalider les bons d'achat.
        for(BonAchat bonAchat : bonAchats){
            bonAchat.setDejaUtilise(true);
            bonAchatRepository.save(bonAchat);
            String logCreated = "Bon Achat avec code " + bonAchat.getCode() + " vient d'être utilisé par l'utilisateur " + commande.getUser().getMail();
            log.info(logCreated);
            User user = userRepository.findUserByMail(commande.getUser().getMail());
            evenementService.enregistrerEvenement(user, logCreated);
        }
    }

    @Override
    public void attribuerBonAchatSelonCommande(Commande commande) {

        List<BonAchat> bonAchatsCrees = new ArrayList<>();

        //Attribuer bons d'achats selon prix de la commande
        if(commande.getPaiement().getMontantTotal() >= 300){
            BonAchat bonAchat = BonAchat.builder()
                    .date_creation(new Timestamp(new Date().getTime()))
                    .valeur(8d)
                    .user(commande.getUser())
                    .dejaUtilise(false)
                    .build();
            bonAchat.generateCodeBonAchat();

            commande.getUser().getBonAchats().add(bonAchat);
            bonAchatsCrees.add(bonAchat);
            String logCreated = "Bon d'achat d'une valeur de " + bonAchat.getValeur() + "€ créé pour l'utilisateur " + commande.getUser().getMail() + " car commande " + commande.getId() + " supérieure à 300€";
            log.info(logCreated);
            evenementService.enregistrerEvenement(null, logCreated);
        }

        //Attribuer bons d'achats selon nombre de commandes
        List<Commande> commandes = commandeRepository.findAllByUser(commande.getUser());

        if(commandes != null && commandes.size() > 0) {

            BonAchat bonAchat = BonAchat.builder()
                    .date_creation(new Timestamp(new Date().getTime()))
                    .valeur(null)
                    .user(commande.getUser())
                    .dejaUtilise(false)
                    .build();

            switch (commandes.size()) {

                case 5:
                    bonAchat.setValeur(5d);
                    break;

                case 10:
                    bonAchat.setValeur(15d);
                    break;

                case 30:
                    bonAchat.setValeur(60d);
                    break;

            }

            if (bonAchat.getValeur() != null && bonAchat.getValeur() > 0) {
                bonAchat.generateCodeBonAchat();
                commande.getUser().getBonAchats().add(bonAchat);
                bonAchatsCrees.add(bonAchat);

                String logCreated = "Bon d'achat d'une valeur de " + bonAchat.getValeur() + "€ créé pour l'utilisateur " + commande.getUser().getMail() + " car total de " + commandes.size() + " commande(s)";
                log.info(logCreated);
                evenementService.enregistrerEvenement(null, logCreated);
            }

        }

        userRepository.save(commande.getUser());
        bonAchatRepository.save(bonAchatsCrees);

        envoyerMailNouveauBonAchats("Un cadeau se trouve dans ce mail", "Félicitations, vous avez gagné des avantages !", bonAchatsCrees);

    }

    private void envoyerMailNouveauBonAchats(String objet, String texteMail, List<BonAchat> bonAchats){

        if(bonAchats != null && bonAchats.size() > 0) {
            Thread envoiMailNouveauBonAchat = new Thread(() -> {
                User user = bonAchats.get(0).getUser();

                Map<String, Object> variablesMail = new HashMap<>();
                variablesMail.put("bonAchats", bonAchats);
                variablesMail.put("login", user.getMail());
                variablesMail.put("texteMail", texteMail);
                variablesMail.put("allerSurSite", Client.host);

                Map<String, String> imagesAAjouter = new HashMap<>();
                imagesAAjouter.put("bonachat", "./static/cadeau.gif");

                emailService.sendMail(user.getMail(), "Yateli : " + objet, variablesMail, imagesAAjouter, null, "mailNouveauBonAchat.ftl");
                String logCreated = "Mail de création de " + bonAchats.size() + " bons d'achats pour l'utilisateur " + user.getMail() + " envoyé";
                log.info(logCreated);
                evenementService.enregistrerEvenement(user, logCreated);
            });
            envoiMailNouveauBonAchat.start();
        }
    }

    //TODO : Tester date d'inscription
    private void attribuerBonAchatSelonDateInscriptionNombreCommandes(){

        LocalDate maintenant = LocalDate.now();
        Date unAnAvant = Date.from((maintenant.minusYears(1).atTime(0, 0).toLocalDate()).atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

        List<User> users = userRepository.findAllByDateCreationLessThan(new Timestamp(unAnAvant.getTime()));

        for(User user : users){
            boolean offreUnAnDejaCree = false;

            for(BonAchat bonAchat : user.getBonAchats()){
                if(bonAchat.getCode().equals(offreUnAn + user.getMail())){
                    offreUnAnDejaCree = true;
                    break;
                }
            }

            if(!offreUnAnDejaCree){
                BonAchat bonAchatUnAn = BonAchat.builder()
                        .date_creation(new Timestamp(new Date().getTime()))
                        .valeur(15d)
                        .user(user)
                        .dejaUtilise(false)
                        .build();
                bonAchatUnAn.generateCodeBonAchat();

                user.getBonAchats().add(bonAchatUnAn);
                userRepository.save(user);
                bonAchatRepository.save(bonAchatUnAn);
                String logCreated = "Bon Achat de " + bonAchatUnAn.getValeur() + "€ créé pour l'utilisateur " + user.getMail() + " car 1 an d'inscription";
                log.info(logCreated);
                evenementService.enregistrerEvenement(user, logCreated);
                envoyerMailNouveauBonAchats("Déjà un an que vous êtes parmi nous, donc cadeau", "Merci pour votre confiance", Collections.singletonList(bonAchatUnAn));
            }
        }

    }

}
