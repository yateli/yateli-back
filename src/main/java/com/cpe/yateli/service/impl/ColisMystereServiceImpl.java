package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.colisMystere.DateDecouverteColisMystereException;
import com.cpe.yateli.exception.user.NonAbonneException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.*;
import com.cpe.yateli.repository.ColisMystereRepository;
import com.cpe.yateli.repository.CommandeRepository;
import com.cpe.yateli.repository.UserRepository;
import com.cpe.yateli.service.ColisMystereService;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.ProduitService;
import com.cpe.yateli.service.mail.EmailService;
import com.cpe.yateli.service.telephone.TelephoneService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.security.Principal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class ColisMystereServiceImpl implements ColisMystereService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ColisMystereRepository colisMystereRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CommandeRepository commandeRepository;

    @Autowired
    private ProduitService produitService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private TelephoneService telephoneService;

    @Autowired
    private EvenementService evenementService;

    @Override
    public ColisMystere creerColisMystere(Principal moi) throws UserNonTrouveException {

        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        LocalDate maintenant = LocalDate.now();
      //  LocalDate unMoisPlusTard = maintenant.plusMonths(1).atTime(0,0).toLocalDate(); //Un mois plus tard à minuit

        Date maintenantDate = Date.from(maintenant.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
     //   Date unMoisPlusTardDate = Date.from(unMoisPlusTard.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

        //Test
       // LocalDate lendemain = maintenant.plusDays(1).atTime(0,0).toLocalDate();
       // Date lendemainDate = Date.from(lendemain.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        Instant now = Instant.now();
        Instant cinqMinutesApres = now.plus(5, ChronoUnit.MINUTES);
        Date cinqMinutesApresDate = Date.from(cinqMinutesApres.atZone(ZoneId.systemDefault()).toInstant());


        ColisMystere colisMystere = ColisMystere.builder()
                .dateCreation(new Timestamp(maintenantDate.getTime()))
                .dateDecouverte(new Timestamp(cinqMinutesApresDate.getTime()))
              //  .dateDecouverte(new Timestamp(unMoisPlusTardDate.getTime()))
              //  .dateDecouverte(new Timestamp(lendemainDate.getTime()))
                .build();

        colisMystereRepository.save(colisMystere);

        userConnecte.ajoutColisMystere(colisMystere);

        userRepository.save(userConnecte);

        String logCreated = "Colis Mystere " + String.valueOf(colisMystere.getId()) + " créé pour User " + userConnecte.getMail() + " et sera découvert à " +String.valueOf(colisMystere.getDateDecouverte().getTime());
        log.info(logCreated);
        evenementService.enregistrerEvenement(userConnecte, logCreated);

        return colisMystere;

    }

    @Scheduled(cron = "0 */2 * * * ?")
    public void attribuerColisMystere() { // Permet d'attribuer les colis mystère chaque jour (Ajouter schedule)

        log.info("Début attribution Colis Mystères");

        LocalDate maintenant = LocalDate.now();
        Date maintenantDate = Date.from(maintenant.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

        LocalDate lendemain = maintenant.plusDays(1).atTime(0,0).toLocalDate();
        Date lendemainDate = Date.from(lendemain.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

        List<ColisMystere> colisMystereList = colisMystereRepository.findAllByDateDecouverteBetweenAndProduitIsNull(new Timestamp(maintenantDate.getTime()),new Timestamp(lendemainDate.getTime()));

        for(ColisMystere colisMystere : colisMystereList){
            //Traitement decision tree + log + Affectation produit
            Map<Tag,Integer> frequenceTagAchete = new HashMap<>();

            //Calcul de la fréquence de tags pour les commandes achetées ce mois-ci
            List<Commande> commandesDuMois = commandeRepository.findAllByUser(colisMystere.getUser());
            if(commandesDuMois != null && commandesDuMois.size() > 0){
                for(Commande commande : commandesDuMois){
                    for(ProduitQuantitePanier produitQuantitePanier : commande.getProduits()){
                        for(Tag tagCommande : produitQuantitePanier.getProduit().getTags()){
                            if(frequenceTagAchete.containsKey(tagCommande)){
                                frequenceTagAchete.put(tagCommande, frequenceTagAchete.get(tagCommande) + produitQuantitePanier.getQuantite());
                            }else{
                                frequenceTagAchete.put(tagCommande, produitQuantitePanier.getQuantite());
                            }
                        }
                    }
                }
            }

            //Calcul de la fréquence de tags selon la liste d'envies des utilisateurs
            Set<Tag> tagsListeEnvie = colisMystere.getUser().getListeEnvies();
            for(Tag envieUser : tagsListeEnvie){
                if(frequenceTagAchete.containsKey(envieUser)){
                    frequenceTagAchete.put(envieUser, frequenceTagAchete.get(envieUser) + 1); //Une envie du questionnaire d'inscription a un coefficient de 1
                }else{
                    frequenceTagAchete.put(envieUser, 1);
                }
            }

            //Affectation produit selon fréquence des tags
            Tag preferenceUtilisateur = Collections.max(frequenceTagAchete.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey();
            Produit produitDansColisMystere = produitService.affectationColisMystere(colisMystere.getUser(), preferenceUtilisateur);

            //Définition produit dans colis mystere
            colisMystere.setProduit(produitDansColisMystere);
            colisMystereRepository.save(colisMystere);

            String logCreatedBis = "Produit " + colisMystere.getProduit().getReference() + " affecté au Colis Mystere n°" + String.valueOf(colisMystere.getId()) + " pour User " + colisMystere.getUser().getMail() + " traité";
            log.info(logCreatedBis);
            evenementService.enregistrerEvenement(colisMystere.getUser(), logCreatedBis);

            //webSocketService.envoyerUserSpecifique(colisMystere.getUser().getMail(), SocketEventType.DECOUVERTE_COLIS_MYSTERE, new MessageDto("Découvrez votre colis mystère"));

            Thread affectationColisMystere = new Thread(() -> {
                if(colisMystere.getUser().getTelephone() != null && !colisMystere.getUser().getTelephone().isEmpty()){
                    telephoneService.envoyerSms(colisMystere.getUser().getTelephone(), "Yateli : Votre colis mystère est arrivé (Découvrez-le sur https://www.yateli.fr/)");

                    String logCreatedTer = "SMS d'affectation de colis mystère envoyé à " + colisMystere.getUser().getMail();
                    log.info(logCreatedTer);
                    evenementService.enregistrerEvenement(colisMystere.getUser(), logCreatedTer);
                }else{
                    Map<String,Object> variablesMail = new HashMap<>();
                    variablesMail.put("prenom", colisMystere.getUser().getPrenom());
                    variablesMail.put("login", colisMystere.getUser().getMail());
                    variablesMail.put("nomProduit", colisMystere.getProduit().getNom());
                    variablesMail.put("nomArtisan", colisMystere.getProduit().getArtisan().getUser().getPrenom());
                    variablesMail.put("allerSurSite", Client.host + "/#!/profile");
                    Map<String,String> imagesAAjouter = new HashMap<>();
                    imagesAAjouter.put("colisMystere","./static/colismystere.gif");
                    emailService.sendMail(colisMystere.getUser().getMail(),"Yateli : Votre colis mystère est arrivé",variablesMail,imagesAAjouter,null,"mailColisMystereAttribue.ftl");

                    String logCreatedTer = "Mail d'affectation de colis mystère envoyé à " + colisMystere.getUser().getMail();
                    log.info(logCreatedTer);
                    evenementService.enregistrerEvenement(colisMystere.getUser(), logCreatedTer);

                }
            });
            affectationColisMystere.start();

        }

        log.info("Fin attribution Colis Mystères");

    }

    @Override
    public ColisMystere getColisMystere(Principal moi) throws UserNonTrouveException, DateDecouverteColisMystereException, NonAbonneException, UserBloqueException {

        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        ColisMystere colisMystereARecuperer = colisMystereRepository.findColisMystereByUserAndARecupereColisIsFalse(userConnecte);

        if(colisMystereARecuperer == null){
            throw NonAbonneException.creerAvec(HttpStatus.PRECONDITION_FAILED,userConnecte.getMail());
        }

        if(colisMystereARecuperer.getProduit() == null || (new Date().getTime() < colisMystereARecuperer.getDateDecouverte().getTime())){
            throw DateDecouverteColisMystereException.creerAvec(HttpStatus.PRECONDITION_FAILED,String.valueOf(colisMystereARecuperer.getDateDecouverte().getTime()));
        }

        colisMystereARecuperer.setARecupereColis(true);

        colisMystereRepository.save(colisMystereARecuperer);

        this.creerColisMystere(moi);

        return colisMystereARecuperer;

    }

    @Override
    public void supprimerTousColisMystere(List<ColisMystere> colisMysteres) { colisMystereRepository.delete(colisMysteres); }


}
