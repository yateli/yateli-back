package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.bonAchat.BonAchatNullOuDejaUtiliseException;
import com.cpe.yateli.exception.commande.*;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.*;
import com.cpe.yateli.model.dto.CommandeDto;
import com.cpe.yateli.repository.*;
import com.cpe.yateli.service.CommandeService;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.ProduitService;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CommandeServiceImpl implements CommandeService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CommandeRepository commandeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LivraisonRepository livraisonRepository;

    @Autowired
    private BonAchatRepository bonAchatRepository;

    @Autowired
    private PaiementRepository paiementRepository;

    @Autowired
    private AdresseRepository adresseRepository;

    @Autowired
    private ProduitService produitService;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private EvenementService evenementService;

    @Override
    public Commande passerCommande(Principal moi, CommandeDto commande) throws Exception {

        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        Panier panier = userConnecte.getPanier();

        if(panier.getPoidsPanier() <= 0.1){
            throw PoidsPanier0Exception.creerAvec(HttpStatus.PRECONDITION_FAILED);
        }

        //Test si le stock des produits dans le panier n'est pas vide avant commande
        for(ProduitQuantitePanier produitQuantitePanier : panier.getProduits()){
            Produit produitACommander = produitQuantitePanier.getProduit();
            if(produitACommander.getQuantiteStock() < produitQuantitePanier.getQuantite()){
                String logCreated = "Stock insuffisant pour commande de " + userConnecte.getMail();
                log.error(logCreated);

                evenementService.enregistrerEvenement(userConnecte, logCreated);
                throw StockInsuffisantPourCommandeException.creerAvec(HttpStatus.PRECONDITION_FAILED, produitACommander.getNom(), produitACommander.getQuantiteStock(), produitQuantitePanier.getQuantite());
            }
        }

        //Supprime les anciennes commandes non payées
        List<Commande> commandeNonPayeeExistante = commandeRepository.findAllByUserAndPaiement_EstPayeFalse(userConnecte);
        if(commandeNonPayeeExistante != null && commandeNonPayeeExistante.size() > 0){
            commandeRepository.delete(commandeNonPayeeExistante);
            String logCreated = commandeNonPayeeExistante.size() + " commande(s) non payée(s) a été supprimée(s) pour l'utilisateur " + userConnecte.getMail();
            log.info(logCreated);
            evenementService.enregistrerEvenement(userConnecte, logCreated);
        }

        //Récupération Livraison choisie par le User
        Livraison livraisonChoisieSelonPoidsEtSociete = null;
        List<Livraison> livraisonsSocieteChoisie = livraisonRepository.findLivraisonBySociete(commande.getSocieteLivraison());
        for(Livraison livraisonSocieteChoisie : livraisonsSocieteChoisie){
            if(panier.getPoidsPanier() >= livraisonSocieteChoisie.getPoidsMin() && panier.getPoidsPanier() < livraisonSocieteChoisie.getPoidsMax()){
                livraisonChoisieSelonPoidsEtSociete = livraisonSocieteChoisie;
                break;
            }
        }

        if(livraisonChoisieSelonPoidsEtSociete == null){
            throw NonLivraisonException.creerAvec(HttpStatus.CONFLICT);
        }

        /*----------------------------------------------------------------------------------------*/
        //Test d'une adresse française pour voir si elle existe (API gouvernementale)
        boolean adresseTrouvee = false;
        Adresse adresseLivraison = commande.getAdresseLivraison();

        //Vérifie que l'adresse a été envoyée correctement
        if(adresseLivraison.getIdentification().isEmpty() || adresseLivraison.getNumeroEtLibelleVoie().isEmpty() || adresseLivraison.getCodePostal().isEmpty() || adresseLivraison.getVille().isEmpty() || adresseLivraison.getPays() == null){
            throw AddressNullException.creerAvec(HttpStatus.PRECONDITION_FAILED);
        }

        if(!adresseLivraison.getCodePostal().matches("[0-9]+")){
            throw FormatCodePostalNonCorrectException.creerAvec(HttpStatus.PRECONDITION_FAILED, adresseLivraison.getCodePostal());
        }

        ResponseEntity<String> responseEntityAddressAPI = restTemplate.getForEntity("https://api-adresse.data.gouv.fr/search/?q="+adresseLivraison.getNumeroEtLibelleVoie(), String.class);

        String bodyResponseEntityAddressAPI = responseEntityAddressAPI.getBody();

        if(bodyResponseEntityAddressAPI != null && !bodyResponseEntityAddressAPI.isEmpty()) {
            JsonObject jsonAddressAPI = new Gson().fromJson(bodyResponseEntityAddressAPI, JsonObject.class);

            for (JsonElement adresse : jsonAddressAPI.getAsJsonArray("features")) {
                String codePostalAPI = adresse.getAsJsonObject().getAsJsonObject("properties").getAsJsonPrimitive("postcode").getAsString();
                String villeAPI = adresse.getAsJsonObject().getAsJsonObject("properties").getAsJsonPrimitive("city").getAsString();

                if (codePostalAPI.toUpperCase().equals(adresseLivraison.getCodePostal().toUpperCase()) && villeAPI.toUpperCase().equals(adresseLivraison.getVille().toUpperCase())) {
                    adresseTrouvee = true;
                    break;
                }
            }

            if (!adresseTrouvee) {
                throw AddressNullException.creerAvec(HttpStatus.BAD_REQUEST);
            }
        }

        adresseRepository.save(adresseLivraison);
        /*----------------------------------------------------------------------------------------*/

        // Récupération Bons d'Achat
        List<BonAchat> bonAchatsUtilises = new ArrayList<>();
        Double cumulReductions = 0d;
        if(commande.getCodesBonsAchat() != null){
            for(String bonAchat : commande.getCodesBonsAchat()){
                BonAchat entiteBonAchat = bonAchatRepository.findBonAchatByCodeAndUser(bonAchat.toUpperCase(),userConnecte);
                if(entiteBonAchat == null || entiteBonAchat.isDejaUtilise()){
                    throw BonAchatNullOuDejaUtiliseException.creerAvec(HttpStatus.BAD_REQUEST,bonAchat);
                }
                if(!bonAchatsUtilises.contains(entiteBonAchat)){ //On bloque le fait de pouvoir mettre 2 fois le même bon d'achat
                    cumulReductions += entiteBonAchat.getValeur();
                    bonAchatsUtilises.add(entiteBonAchat);
                }
            }
        }

        //Création paiement
        Paiement paiement = Paiement.builder()
                .coutLivraison(livraisonChoisieSelonPoidsEtSociete.getPrixLivraison(panier.getPoidsPanier()))
                .reductions(cumulReductions)
                .build();
        paiement.calculMontants(panier); // Calcul de tous les attributs de paiement selon Panier
        paiementRepository.save(paiement);

        //Création commande si l'adresse saisie pour la livraison semble correcte (ville+code postal)
        Commande nouvelleCommande = Commande.builder()
                .date_creation(new Timestamp(new Date().getTime()))
                .adresseLivraison(adresseLivraison)
                .bonsAchat(bonAchatsUtilises)
                .livraison(livraisonChoisieSelonPoidsEtSociete)
                .facture(null)
                .produits(new ArrayList<>(panier.getProduits()))
                .poidsCommande(panier.getPoidsPanier())
                .paiement(paiement)
                .user(panier.getUser())
                .build();

        commandeRepository.save(nouvelleCommande);

        String logCreated = "Commande " + String.valueOf(nouvelleCommande.getId()) + " cree pour le User " + userConnecte.getMail();
        log.info(logCreated);
        evenementService.enregistrerEvenement(userConnecte, logCreated);

        return nouvelleCommande;

    }

    @Override
    public List<Commande> getCommandesUser(Principal moi) throws UserNonTrouveException, UserBloqueException {
        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        return commandeRepository.findAllByUser(userConnecte);
    }

    @Override
    public void supprimerToutesCommandesUser(User user) {
        commandeRepository.delete(commandeRepository.findAllByUser(user));
    }

    @Override
    public void supprimerCommande(Commande commande) {
        commandeRepository.delete(commande);
    }



}
