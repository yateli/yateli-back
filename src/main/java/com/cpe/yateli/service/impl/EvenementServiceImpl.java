package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.User;
import com.cpe.yateli.model.logMongo.Evenement;
import com.cpe.yateli.repository.EvenementRepository;
import com.cpe.yateli.repository.UserRepository;
import com.cpe.yateli.service.EvenementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EvenementServiceImpl implements EvenementService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private EvenementRepository evenementRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<String> recupererEvenementsUser(String login) throws UserNonTrouveException {

        User user = userRepository.findUserByMail(login);

        if(user == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND, login);
        }

        return evenementRepository.findAllByLoginUserIs(user.getMail())
                .stream()
                .map(Evenement::getLog)
                .collect(Collectors.toList());

    }

    @Override
    public void enregistrerEvenement(User user, String log) {

        String userName;
        if(user == null) {
            userName = "NULL";
        }else{
            userName = user.getMail();
        }

        Evenement evenement = Evenement.builder()
                .date(Instant.now().plus(1, ChronoUnit.HOURS))
                .loginUser(userName)
                .log(log)
                .build();

        evenementRepository.save(evenement);

        logger.info("Evenement \"{}\" ajouté en BDD Mongo pour User {}", log, userName);

    }

}
