package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.facture.FactureExistePasException;
import com.cpe.yateli.exception.facture.PdfNullException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.*;
import com.cpe.yateli.model.view.FactureView;
import com.cpe.yateli.repository.CommandeRepository;
import com.cpe.yateli.repository.FactureRepository;
import com.cpe.yateli.repository.UserRepository;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.FactureService;
import com.cpe.yateli.service.mail.EmailService;
import com.cpe.yateli.service.pdf.PDFService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.rowset.serial.SerialBlob;
import java.io.File;
import java.security.Principal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class FactureServiceImpl implements FactureService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${files.facture}")
    private String cheminFacture;

    @Autowired
    private PDFService pdfService;

    @Autowired
    private FactureRepository factureRepository;

    @Autowired
    private CommandeRepository commandeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private EvenementService evenementService;

    @Override
    public FactureView genererFacture(Commande commande) throws Exception {

        //Génération de la facture
        Facture factureGeneree = Facture.builder()
                .date_creation(new Timestamp(new Date().getTime()))
                .user(commande.getUser())
                .commande(commande)
                .pdf(null)
                .build();

        factureRepository.save(factureGeneree);

        MultipartFile pdfGeneree = pdfService.creerPdfFacture(factureGeneree);

        if(pdfGeneree == null || pdfGeneree.getBytes() == null){
            throw PdfNullException.creerAvec(HttpStatus.INTERNAL_SERVER_ERROR,commande.getUser().getMail());
        }

        factureGeneree.setPdf(new SerialBlob(pdfGeneree.getBytes()));
        factureRepository.save(factureGeneree);

        commande.setFacture(factureGeneree);
        commandeRepository.save(commande);

        User user = commande.getUser();
        Thread envoiValidationCommande = new Thread(() -> {
            try {
                Date date = new Date();
                String dateCommande = new SimpleDateFormat("dd MMMMM yyyy").format(date);

                Map<String, Object> variablesMail = new HashMap<>();
                variablesMail.put("prenom", user.getPrenom());
                variablesMail.put("commande", commande);
                variablesMail.put("allerSurSite", Client.host);
                variablesMail.put("dateCommande", dateCommande);

                Map<String, String> imagesAAjouter = new HashMap<>();
                imagesAAjouter.put("orderSucces", "./static/ordersuccess.gif");

                List<File> pieceJointes = new ArrayList<>();
                String dateFactureRecupere = new SimpleDateFormat("ddMMyy").format(date);
                File conversionFichier = new File(cheminFacture + "Facture" + dateFactureRecupere + ".pdf");
                pdfGeneree.transferTo(conversionFichier);
                pieceJointes.add(conversionFichier);

                emailService.sendMail(user.getMail(), "Yateli : Récapitulatif de votre commande n°" + String.valueOf(commande.getId()), variablesMail, imagesAAjouter, pieceJointes,"mailRecapitulatifCommande.ftl");

                //Suppression fichier temporaire apres envoi
                if(!conversionFichier.delete()){
                    String logCreated = "Fichier temporaire de \"" + conversionFichier.getName() + "\" au chemin(" + conversionFichier.getAbsolutePath() + ") pour commande n°" + commande.getId() + " non supprimé";
                    log.error(logCreated);
                    evenementService.enregistrerEvenement(commande.getUser(), logCreated);
                }
                String logCreated = "Mail de récapitulatif de commande d'ID " + commande.getId() + " avec facture envoyé à " + user.getMail();
                log.info(logCreated);
                evenementService.enregistrerEvenement(user, logCreated);
            }catch(Exception e){
                log.error(e.getMessage());
            }
        });
        envoiValidationCommande.start();

        String logCreated = "Facture n°" + factureGeneree.getId() + " générée pour le User " + user.getMail();
        log.info(logCreated);
        evenementService.enregistrerEvenement(user, logCreated);

        return FactureView.creerFactureViewDepuisFacture(factureGeneree);

    }

    @Override
    public List<FactureView> getFacturesUser(Principal moi) throws UserNonTrouveException, FactureExistePasException, UserBloqueException {

        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        List<Facture> factures = factureRepository.findAllByUser(userConnecte);

        if(factures == null || factures.isEmpty()){
            throw FactureExistePasException.creerAvec(HttpStatus.NOT_FOUND,"",userConnecte.getMail());
        }

        log.debug("Factures du User {} récupérées", userConnecte.getMail());

        return FactureView.creerListFactureViewDepuisListFacture(factures);

    }

    @Override
    public Resource getFacture(Principal moi, int idFacture) throws SQLException, UserNonTrouveException, FactureExistePasException, UserBloqueException {

        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        Facture facture = factureRepository.findFactureByIdAndUser(idFacture,userConnecte);

        if(facture == null || facture.getPdf() == null){
            throw FactureExistePasException.creerAvec(HttpStatus.NOT_FOUND,String.valueOf(idFacture),userConnecte.getMail());
        }

        String logCreated = "Facture n°" + facture.getId() + " récupérée par " + facture.getUser().getMail();
        log.info(logCreated);
        evenementService.enregistrerEvenement(facture.getUser(), logCreated);

        return new InputStreamResource(facture.getPdf().getBinaryStream());

    }

    @Override
    public void supprimerFacturesCommandesUser(User user) {

        List<Facture> facturesUser = factureRepository.findAllByUser(user);

        if(facturesUser != null){
            factureRepository.delete(facturesUser);
        }

    }


}
