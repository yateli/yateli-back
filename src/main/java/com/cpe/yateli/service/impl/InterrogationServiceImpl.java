package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.interrogation.*;
import com.cpe.yateli.exception.tag.TagExistePasException;
import com.cpe.yateli.model.Interrogation;
import com.cpe.yateli.model.Reponse;
import com.cpe.yateli.model.Tag;
import com.cpe.yateli.model.dto.InterrogationActivesDto;
import com.cpe.yateli.model.dto.InterrogationDto;
import com.cpe.yateli.model.dto.ReponseDto;
import com.cpe.yateli.model.dto.TagDto;
import com.cpe.yateli.repository.InterrogationRepository;
import com.cpe.yateli.repository.ReponseRepository;
import com.cpe.yateli.repository.TagRepository;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.InterrogationService;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.language.Soundex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InterrogationServiceImpl implements InterrogationService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private InterrogationRepository interrogationRepository;

    @Autowired
    private ReponseRepository reponseRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private EvenementService evenementService;

    @Override
    public Interrogation creerQuestion(InterrogationDto interrogationDto) throws EncoderException, QuestionExisteDejaException, TagExistePasException {

        //Vérifie si existe deja
        Soundex comparaison = new Soundex();
        List<Interrogation> questions = getToutesInterrogations();
        for(Interrogation interrogation : questions){
            int caracteresIdentiques = comparaison.difference(interrogationDto.getQuestion(),interrogation.getQuestion());
            if(caracteresIdentiques > interrogationDto.getQuestion().length() - 4){
                throw QuestionExisteDejaException.creerAvec(HttpStatus.PRECONDITION_FAILED, interrogation.getId(), interrogation.getQuestion());
            }
        }

        //Création des réponses
        List<Reponse> reponsesCrees = new ArrayList<>();

        for(ReponseDto reponseDto : interrogationDto.getReponses()){
            List<Tag> tagsTrouves = new ArrayList<>();

            for(TagDto tagDto : reponseDto.getTags()){
                Tag tag = tagRepository.findTagByNom(tagDto.getNom());
                if(tag == null){
                    throw TagExistePasException.creerAvec(HttpStatus.NOT_FOUND, tagDto.getNom());
                }
                tagsTrouves.add(tag);
            }

            Reponse reponse = Reponse.builder()
                    .texte(reponseDto.getTexte())
                    .tags(new HashSet<>(tagsTrouves))
                    .interrogation(null)
                    .build();

            reponsesCrees.add(reponse);

            reponseRepository.save(reponse);
        }

        Interrogation question = Interrogation.builder()
                .question(interrogationDto.getQuestion())
                .reponses(reponsesCrees)
                .build();

        List<Interrogation> questionsActives = interrogationRepository.findAllByActiveIsTrue();

        if (questionsActives.size()>= 6)
            question.setActive(false);
        else
            question.setActive(true);

        interrogationRepository.save(question);

        for(Reponse reponse : reponsesCrees){
            reponse.setInterrogation(question);
            reponseRepository.save(reponse);
        }

        String logCreated = "Question \"" + question.getQuestion() + "\" créée";
        log.info(logCreated);
        evenementService.enregistrerEvenement(null, logCreated);

        return question;

    }

    public String supprimerQuestion(Integer idQuestion) throws QuestionExistePasException {

        Interrogation question = interrogationRepository.findById(idQuestion);

        if (question == null){
            throw QuestionExistePasException.creerAvec(HttpStatus.NOT_FOUND, idQuestion);
        }

        for(Reponse reponse : question.getReponses()){
            question.getReponses().remove(reponse);
            reponseRepository.delete(reponse);
        }

        interrogationRepository.delete(question);

        String logCreated = "Question \"" + question.getQuestion() + "\" supprimée";
        log.info(logCreated);
        evenementService.enregistrerEvenement(null, logCreated);

        return "OK";

    }

    @Override
    public List<Interrogation> getInterrogationsActives() {
        return interrogationRepository.findAll().stream()
                .filter(Interrogation::isActive)
                .collect(Collectors.toList());
    }

    @Override
    public List<Interrogation> getToutesInterrogations() {
        return interrogationRepository.findAll();
    }

    @Override
    public String activerQuestions(InterrogationActivesDto interrogationActivesDto) throws QuestionExistePasException, QuestionsActivesException, QuestionDejaActiveException {

        List<Interrogation> questionsActives = interrogationRepository.findAllByActiveIsTrue();

        if ((questionsActives.size()>= 6)|| (interrogationActivesDto.getIdQuestions().size() > (6-questionsActives.size()))) {
            throw QuestionsActivesException.creerAvec(HttpStatus.PRECONDITION_FAILED);
        }

        for( Integer idQuestion : interrogationActivesDto.getIdQuestions()){

            Interrogation question = interrogationRepository.findById(idQuestion);
            if (question == null){
                throw QuestionExistePasException.creerAvec(HttpStatus.NOT_FOUND, idQuestion);
            }

            if(question.isActive()){
                throw QuestionDejaActiveException.creerAvec(HttpStatus.PRECONDITION_FAILED, idQuestion);
            }

            question.setActive(true);
            interrogationRepository.save(question);
        }

        return "OK";
    }

    @Override
    public String desactiverQuestions(InterrogationActivesDto interrogationActivesDto) throws QuestionExistePasException, QuestionsActivesException, QuestionPasActiveException {

        List<Interrogation> questionsActives = interrogationRepository.findAllByActiveIsTrue();

        if ((questionsActives.size()<= 0)|| (interrogationActivesDto.getIdQuestions().size() > (questionsActives.size()))) {
            throw QuestionsActivesException.creerAvec(HttpStatus.PRECONDITION_FAILED);
        }

        for( Integer idQuestion : interrogationActivesDto.getIdQuestions()){

            Interrogation question = interrogationRepository.findById(idQuestion);
            if (question == null){
                throw QuestionExistePasException.creerAvec(HttpStatus.NOT_FOUND, idQuestion);
            }

            if(!question.isActive()){
                throw QuestionPasActiveException.creerAvec(HttpStatus.PRECONDITION_FAILED, idQuestion);
            }

            question.setActive(false);
            interrogationRepository.save(question);
        }

        return "OK";
    }

}
