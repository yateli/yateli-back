package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.jeuConcours.*;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.JeuConcours;
import com.cpe.yateli.model.Role;
import com.cpe.yateli.model.User;
import com.cpe.yateli.model.dto.JeuConcoursDto;
import com.cpe.yateli.repository.JeuConcoursRepository;
import com.cpe.yateli.repository.UserRepository;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.JeuConcoursService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class JeuConcoursServiceImpl implements JeuConcoursService{

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private JeuConcoursRepository jeuConcoursRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EvenementService evenementService;

    @Override
    public JeuConcours creerJeuConcours(JeuConcoursDto jeuConcoursDto) throws JeuConcoursExisteDejaException {

        JeuConcours jeuConcoursExiste = jeuConcoursRepository.findJeuConcoursByNom(jeuConcoursDto.getNom());

        if(jeuConcoursExiste != null){
            throw JeuConcoursExisteDejaException.creerAvec(HttpStatus.PRECONDITION_FAILED, jeuConcoursExiste.getNom());
        }

        JeuConcours jeuConcours = JeuConcours.builder()
                .nom(jeuConcoursDto.getNom())
                .description(jeuConcoursDto.getDescription())
                .pointsFideliteMinimum(jeuConcoursDto.getPointsFideliteMinimum())
                .debutConcours(new Timestamp(jeuConcoursDto.getDebutConcours()))
                .finConcours(new Timestamp(jeuConcoursDto.getFinConcours()))
                .fermetureExceptionnelle(false)
                .users(new ArrayList<>())
                .build();

        if(jeuConcoursDto.isEstFerme()){
            jeuConcours.setFermetureExceptionnelle(true);
        }

        if(jeuConcoursDto.getLoginsInvite() != null && jeuConcoursDto.getLoginsInvite().size() > 0){
            for(String loginUser : jeuConcoursDto.getLoginsInvite()){
                User user = userRepository.findUserByMail(loginUser);
                if(user != null){
                    jeuConcours.getUsers().add(user);
                }
            }
        }

        jeuConcoursRepository.save(jeuConcours);

        String logCreated = "Jeu Concours " + jeuConcours.getNom() + " créé avec " + String.valueOf(jeuConcours.getUsers().size()) + " utilisateur(s) participant";
        log.info(logCreated);
        evenementService.enregistrerEvenement(null, logCreated);
        return jeuConcours;

    }

    @Override
    public JeuConcours changerEtatExceptionnelJeuConcours(Integer idJeuConcours, boolean estFerme) throws JeuConcoursExistePasException, JeuConcoursDejaOuvertFermeException {

        JeuConcours jeuConcours = jeuConcoursRepository.findJeuConcoursById(idJeuConcours);

        if(jeuConcours == null){
            throw JeuConcoursExistePasException.creerAvec(HttpStatus.NOT_FOUND, String.valueOf(idJeuConcours));
        }

        if(jeuConcours.isFermetureExceptionnelle() == estFerme){
            throw JeuConcoursDejaOuvertFermeException.creerAvec(HttpStatus.PRECONDITION_FAILED, jeuConcours.getNom(), (jeuConcours.isFermetureExceptionnelle()) ? "fermé" : "ouvert");
        }

        jeuConcours.setFermetureExceptionnelle(estFerme);

        jeuConcoursRepository.save(jeuConcours);

        String logCreated = "Jeu concours d'ID " + String.valueOf(jeuConcours.getId()) + " (" + jeuConcours.getNom() + ") " + ((jeuConcours.isFermetureExceptionnelle()) ? "fermé" : "ouvert")+ " avec succés";
        log.info(logCreated);
        evenementService.enregistrerEvenement(null, logCreated);

        return jeuConcours;

    }

    @Override
    public JeuConcours participerJeuConcours(int idJeuConcours, Principal moi) throws UserNonTrouveException, JeuConcoursExistePasException, JeuConcoursFermeException, PointsFideliteInsuffisantException, JeuConcoursDejaInscritException, JeuConcoursFermeExceptionnelementException, UserBloqueException {

        User user = userRepository.findUserByMail(moi.getName());

        if(user == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!user.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, user.getMail());
        }

        JeuConcours jeuConcours = jeuConcoursRepository.findJeuConcoursById(idJeuConcours);

        if(jeuConcours == null){
            throw JeuConcoursExistePasException.creerAvec(HttpStatus.NOT_FOUND, String.valueOf(idJeuConcours));
        }

        if(jeuConcours.getUsers().contains(user)){
            throw JeuConcoursDejaInscritException.creerAvec(HttpStatus.PRECONDITION_FAILED, jeuConcours.getNom(), user.getMail());
        }

        if(jeuConcours.isFermetureExceptionnelle()){
            throw JeuConcoursFermeExceptionnelementException.creerAvec(HttpStatus.PRECONDITION_FAILED, jeuConcours.getNom());
        }

        Timestamp maintenant = new Timestamp(new Date().getTime());
        if(maintenant.getTime() < jeuConcours.getDebutConcours().getTime() || maintenant.getTime() > jeuConcours.getFinConcours().getTime()){
            throw JeuConcoursFermeException.creerAvec(HttpStatus.PRECONDITION_FAILED, jeuConcours.getNom(), jeuConcours.getDebutConcours().getTime(), jeuConcours.getFinConcours().getTime());
        }

        if(user.getPointsFidelite() < jeuConcours.getPointsFideliteMinimum()){
            throw PointsFideliteInsuffisantException.creerAvec(HttpStatus.PRECONDITION_FAILED, jeuConcours.getNom(), user.getPointsFidelite(), jeuConcours.getPointsFideliteMinimum());
        }

        jeuConcours.getUsers().add(user);
        user.setPointsFidelite(user.getPointsFidelite() - jeuConcours.getPointsFideliteMinimum());

        jeuConcoursRepository.save(jeuConcours);
        userRepository.save(user);

        return jeuConcours;

    }

    @Override
    public User desinscrireJeuConcours(int idJeuConcours, Principal moi) throws UserNonTrouveException, JeuConcoursExistePasException, JeuConcoursParticipePasException, UserBloqueException {

        User user = userRepository.findUserByMail(moi.getName());

        if(user == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!user.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, user.getMail());
        }

        JeuConcours jeuConcours = jeuConcoursRepository.findJeuConcoursById(idJeuConcours);

        if(jeuConcours == null){
            throw JeuConcoursExistePasException.creerAvec(HttpStatus.NOT_FOUND, String.valueOf(idJeuConcours));
        }

        if(!jeuConcours.getUsers().contains(user)){
            throw JeuConcoursParticipePasException.creerAvec(HttpStatus.PRECONDITION_FAILED, jeuConcours.getNom(), user.getMail());
        }

        jeuConcours.getUsers().remove(user);
        user.setPointsFidelite(user.getPointsFidelite() + jeuConcours.getPointsFideliteMinimum());

        jeuConcoursRepository.save(jeuConcours);
        userRepository.save(user);

        return user;

    }


    @Override
    public List<JeuConcours> recupererJeuConcours(Principal moi) throws UserNonTrouveException, UserBloqueException {

        User user = userRepository.findUserByMail(moi.getName());

        if(user == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!user.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, user.getMail());
        }

        return jeuConcoursRepository.findAllByUsersContains(user);

    }

    @Override
    public List<JeuConcours> recupererTousLesConcours(Principal moi) throws UserNonTrouveException, UserBloqueException {

        User user = userRepository.findUserByMail(moi.getName());

        if(user == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!user.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, user.getMail());
        }

        Role roleUser = user.getRole();

        if(roleUser.getNom().equalsIgnoreCase("ADMIN")){
            return jeuConcoursRepository.findAll();
        }

        //Cas Artisan et User
        Timestamp now = new Timestamp(new Date().getTime());
        return jeuConcoursRepository.findAllByDebutConcoursBeforeAndFinConcoursAfterAndFermetureExceptionnelleFalse(now, now);

    }

}
