package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.commande.CommandeDejaPayeeException;
import com.cpe.yateli.exception.commande.CommandeNonTrouveeException;
import com.cpe.yateli.model.*;
import com.cpe.yateli.model.view.FactureView;
import com.cpe.yateli.repository.CommandeRepository;
import com.cpe.yateli.service.*;
import com.google.gson.*;
import com.paypal.api.payments.*;
import com.paypal.base.rest.APIContext;
import com.paypal.base.rest.PayPalRESTException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service
public class PaiementServiceImpl implements PaiementService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

  //  private static String clientId = "ATNUKiUGyDc48cOP4sqgy0KQlJGU3T645PV_tZCx8ZlzKu-Wu2Sb-7LX6JOkK3HdjGY6AyUyiUvEp0T6"; //live
  //  private static String clientSecret = "EFC8ya-m8R4l3tyHU5-P1XnWd4uibIR7rI1OR-fnzbWmLpYiFndx-cwipsdEh8s2Hr3cU5yd0SD3c6X7"; //live
  private static String clientId = "AZqPjvb-FL6k_o9k_qTy8v1z7QUDoMtEU6eYs-f6FCdruPmJ9YvSH-9mE2nxZjvXJkxTy8FWa1BdPWKy"; //sandbox
  private static String clientSecret = "EJxWTzeKjjA1q8u74p9D-IlhyxGxP2CgCTBeKugvKw7nqDRrQlnc6AUdhvwwbPCUXFkxVajFfBnvep13"; //sandbox

    private APIContext apiContext;

    @Autowired
    private CommandeRepository commandeRepository;

    @Autowired
    private FactureService factureService;

    @Autowired
    private PanierService panierService;

    @Autowired
    private BonAchatService bonAchatService;

    @Autowired
    private ProduitService produitService;

    @Autowired
    private EvenementService evenementService;

    @PostConstruct
    private void initPaypalAPI(){
       // apiContext = new APIContext(clientId, clientSecret, "live");
        apiContext = new APIContext(clientId, clientSecret, "sandbox");
    }

    @Override
    public String payerCommande(Principal moi, int commandeId) throws PayPalRESTException, CommandeNonTrouveeException, CommandeDejaPayeeException {

        Commande commandeAPayer = commandeRepository.findCommandeById(commandeId);

        if(commandeAPayer == null){
            throw CommandeNonTrouveeException.creerAvec(HttpStatus.NOT_FOUND,String.valueOf(commandeId));
        }

        if(commandeAPayer.getPaiement().isEstPaye()){ //On vérifie que la commande n'a pas déjà été payée
            throw CommandeDejaPayeeException.creerAvec(HttpStatus.CONFLICT, commandeAPayer.getId());
        }

        Double reductions = commandeAPayer.getPaiement().getReductions(); //On récupère la réduction

        Details details = new Details();

        if(commandeAPayer.getPaiement() != null){
            details.setShipping(String.valueOf(commandeAPayer.getPaiement().getCoutLivraison()));
            details.setSubtotal(String.valueOf(new BigDecimal(commandeAPayer.getPaiement().getSousTotal() - reductions).setScale(2, BigDecimal.ROUND_HALF_DOWN).doubleValue()));
            details.setTax(String.valueOf(commandeAPayer.getPaiement().getCoutTaxe()));
        }

        Amount amount = new Amount();
        amount.setCurrency("EUR");
        amount.setTotal(String.valueOf(commandeAPayer.getPaiement().getMontantTotal()));
        amount.setDetails(details);

        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setDescription("Vos achats du jour " + commandeAPayer.getUser().getPrenom());

        //Définition des produits pour Paypal
        List<Item> items = new ArrayList<>();
        for(ProduitQuantitePanier produitQuantite : commandeAPayer.getProduits()){
            Item item = new Item();
            item.setName(produitQuantite.getProduit().getNom()).setQuantity(String.valueOf(produitQuantite.getQuantite())).setCurrency("EUR").setPrice(String.valueOf(produitQuantite.getProduit().getPrixHT()));
            items.add(item);
        }
        if(reductions > 0){ //Si réduction durant la commande on ajoute la réduction
            items.add(new Item().setName("Réduction").setQuantity("1").setCurrency("EUR").setPrice(String.valueOf(reductions * -1)));
        }
        ItemList itemList = new ItemList();
        itemList.setItems(items);

        Adresse adresseLivraison = commandeAPayer.getAdresseLivraison();
        ShippingAddress adresseExpediation = new ShippingAddress();
        adresseExpediation.setRecipientName(adresseLivraison.getIdentification());
        adresseExpediation.setLine1(adresseLivraison.getNumeroEtLibelleVoie());
        adresseExpediation.setCity(adresseLivraison.getVille());
        adresseExpediation.setPostalCode(adresseLivraison.getCodePostal());

        //Récupère le code du pays à 2 lettres
        for (String iso : Locale.getISOCountries()) {
            Locale l = new Locale("", iso);
            if(l.getDisplayCountry().equalsIgnoreCase(adresseLivraison.getPays().getNomPays())){
                adresseExpediation.setCountryCode(l.getCountry());
                break;
            }
        }
        itemList.setShippingAddress(adresseExpediation);

        transaction.setItemList(itemList);

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(transaction);

        PayerInfo payerInfo = new PayerInfo();
        payerInfo.setFirstName(commandeAPayer.getUser().getPrenom());

        Payer payer = new Payer();
        payer.setPayerInfo(payerInfo);
        payer.setPaymentMethod("paypal");

        Payment payment = new Payment();
        payment.setIntent("sale");
        payment.setPayer(payer);
        payment.setTransactions(transactions);

        RedirectUrls redirectUrls = new RedirectUrls();
        redirectUrls.setCancelUrl(Client.host + "/#!/error_paid"); //redirection echec paypal
        redirectUrls.setReturnUrl(Client.host + "/#!/paid"); //redirection succes paypal
        payment.setRedirectUrls(redirectUrls);

        Payment createdPayment = payment.create(apiContext);

        log.info(createdPayment.toString());

        String url = null;
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();

        commandeAPayer.getPaiement().setPaiementId(gson.fromJson(createdPayment.toJSON(), JsonObject.class).getAsJsonPrimitive("id").getAsString());
        commandeRepository.save(commandeAPayer);

        JsonArray linksPaypal = gson.fromJson(createdPayment.toJSON(), JsonObject.class).getAsJsonArray("links");
        for(JsonElement jsonElement : linksPaypal){
            if(jsonElement.getAsJsonObject().getAsJsonPrimitive("rel").getAsString().equalsIgnoreCase("approval_url")){
                url = jsonElement.getAsJsonObject().getAsJsonPrimitive("href").getAsString();
                break;
            }
        }

        return url;

    }

    @Override
    public FactureView effectuerPaiement(String payerId, String paymentId) throws Exception {

        Commande commandePayee = commandeRepository.findCommandeByPaiement_PaiementId(paymentId);

        if(commandePayee == null){
            throw CommandeNonTrouveeException.creerAvec(HttpStatus.NOT_FOUND,paymentId);
        }

        if(commandePayee.getPaiement().isEstPaye()){ //On vérifie que la commande n'a pas déjà été payée
            throw CommandeDejaPayeeException.creerAvec(HttpStatus.CONFLICT, commandePayee.getId());
        }

        PaymentExecution paymentExecution = new PaymentExecution();
        paymentExecution.setPayerId(payerId);

        Payment payment = new Payment();
        payment.setId(paymentId);

        payment.execute(apiContext, paymentExecution);

        commandePayee.getPaiement().setEstPaye(true); //commande payée

        /* réduire quantité stock produits achetés */
        List<ProduitQuantitePanier> produitQuantitePaniers = commandePayee.getProduits();
        for(ProduitQuantitePanier produitQuantitePanier : produitQuantitePaniers){
            produitService.mettreAJourStockProduit(produitQuantitePanier.getProduit(),produitQuantitePanier.getQuantite());
            String logCreated = "Il reste " + produitQuantitePanier.getProduit().getQuantiteStock() + " produit(s) en stock pour le produit " + produitQuantitePanier.getProduit().getReference();
            log.info(logCreated);
            evenementService.enregistrerEvenement(null, logCreated);
        }

        bonAchatService.invaliderBonsAchat(commandePayee);
        bonAchatService.attribuerBonAchatSelonCommande(commandePayee);

        User userCommande = commandePayee.getUser();
        userCommande.setPointsFidelite(userCommande.getPointsFidelite() + panierService.getPointsDeFidelite(userCommande));

        panierService.viderPanierUser(userCommande);

        return factureService.genererFacture(commandePayee);

    }

}
