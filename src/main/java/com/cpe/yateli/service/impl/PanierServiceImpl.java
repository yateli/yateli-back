package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.panier.ProduitInexistantException;
import com.cpe.yateli.exception.panier.ProduitQuantitePanierExistePasException;
import com.cpe.yateli.exception.panier.QuantiteIncorrecteException;
import com.cpe.yateli.exception.produit.StockProduitInsuffisantException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.Panier;
import com.cpe.yateli.model.Produit;
import com.cpe.yateli.model.ProduitQuantitePanier;
import com.cpe.yateli.model.User;
import com.cpe.yateli.repository.PanierRepository;
import com.cpe.yateli.repository.ProduitQuantitePanierRepository;
import com.cpe.yateli.repository.ProduitRepository;
import com.cpe.yateli.repository.UserRepository;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.PanierService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PanierServiceImpl implements PanierService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired // Comme = new()
    private UserRepository userRepository;

    @Autowired
    private PanierRepository panierRepository;

    @Autowired
    private ProduitRepository produitRepository;

    @Autowired
    private ProduitQuantitePanierRepository produitQuantitePanierRepository;

    @Autowired
    private EvenementService evenementService;

    @Override
    public Panier ajoutProduit(Principal moi, String referenceProduit, int quantite) throws UserNonTrouveException, ProduitInexistantException, QuantiteIncorrecteException, StockProduitInsuffisantException, UserBloqueException {

        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        if(quantite < 1){
            throw QuantiteIncorrecteException.creerAvec(HttpStatus.CONFLICT, quantite);
        }

        Panier panierActuel = userConnecte.getPanier();

        Optional<ProduitQuantitePanier> produitQuantitePanierReference = panierActuel.getProduits().stream()
                .filter(p -> p.getProduit().getReference().toLowerCase().equals(referenceProduit.toLowerCase()))
                .findFirst();

        if(produitQuantitePanierReference.isPresent()){
            return modifierQuantiteProduit(moi, referenceProduit, produitQuantitePanierReference.get().getQuantite() + quantite);
        }

        //Récupération produit selon référence
        Produit produitAjoute = produitRepository.findProduitByReference(referenceProduit);

        if(produitAjoute == null){
            throw ProduitInexistantException.creerAvec(HttpStatus.NOT_FOUND,referenceProduit);
        }

        if(produitAjoute.getQuantiteStock() < quantite){
            String logCreated = "Stock insuffisant pour ajout au panier de " + userConnecte.getMail();
            log.error(logCreated);
            evenementService.enregistrerEvenement(userConnecte, logCreated);
            throw StockProduitInsuffisantException.creerAvec(HttpStatus.PRECONDITION_FAILED, produitAjoute.getQuantiteStock(), quantite);
        }

        ProduitQuantitePanier produitQuantitePanier = ProduitQuantitePanier.builder()
                .produit(produitAjoute)
                .panier(panierActuel)
                .quantite(quantite)
                .build();

        produitQuantitePanierRepository.save(produitQuantitePanier);

        panierActuel.ajoutProduitsAuPanier(produitQuantitePanier);

        panierRepository.save(panierActuel);

        String logCreated = "Produit " + produitAjoute.getNom() + " ajouté avec quantité " + produitQuantitePanier.getQuantite() + " au panier " + panierActuel.getId() + " de " + userConnecte.getMail();
        log.info(logCreated);
        evenementService.enregistrerEvenement(userConnecte, logCreated);

        return panierActuel;

    }

    @Override
    public Panier modifierQuantiteProduit(Principal moi, String referenceProduit, int quantite) throws UserNonTrouveException, ProduitInexistantException, QuantiteIncorrecteException, StockProduitInsuffisantException, UserBloqueException {

        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        if(quantite < 1){
            throw QuantiteIncorrecteException.creerAvec(HttpStatus.CONFLICT, quantite);
        }

        Panier panierActuel = userConnecte.getPanier();

        Optional<ProduitQuantitePanier> produitQuantitePanierReference = panierActuel.getProduits().stream()
                .filter(p -> p.getProduit().getReference().equals(referenceProduit))
                .findFirst();

        if(!produitQuantitePanierReference.isPresent()){
            throw ProduitInexistantException.creerAvec(HttpStatus.NOT_FOUND, referenceProduit);
        }

        ProduitQuantitePanier produitQuantitePanier = produitQuantitePanierReference.get();

        if(produitQuantitePanier.getProduit().getQuantiteStock() < quantite){
            String logCreated = "Stock insuffisant pour ajout au panier de " + userConnecte.getMail();
            log.error(logCreated);
            evenementService.enregistrerEvenement(userConnecte, logCreated);
            throw StockProduitInsuffisantException.creerAvec(HttpStatus.PRECONDITION_FAILED, produitQuantitePanier.getProduit().getQuantiteStock(), quantite);
        }

        produitQuantitePanier.setQuantite(quantite);

        panierActuel.updateProduitsAuPanier(); //Mise à jour points de fidelite, poids,...

        produitQuantitePanierRepository.save(produitQuantitePanier);

        String logCreated = "Produit " + produitQuantitePanier.getProduit().getNom() + " modifié avec quantité " + produitQuantitePanier.getQuantite() + " au panier " + panierActuel.getId() + " de " + userConnecte.getMail();
        log.info(logCreated);
        evenementService.enregistrerEvenement(userConnecte, logCreated);

        return panierActuel;

    }

    @Override
    public Panier supprimerProduit(Principal moi, String referenceProduit, int quantite) throws UserNonTrouveException, ProduitInexistantException, UserBloqueException {

        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        Panier panierActuel = userConnecte.getPanier();

        Optional<ProduitQuantitePanier> produitQuantitePanierReference = panierActuel.getProduits().stream()
                .filter(p -> p.getProduit().getReference().equals(referenceProduit))
                .findFirst();

        if(!produitQuantitePanierReference.isPresent()){
            throw ProduitInexistantException.creerAvec(HttpStatus.NOT_FOUND, referenceProduit);
        }

        ProduitQuantitePanier produitQuantitePanier = produitQuantitePanierReference.get();

        panierActuel.supprimerProduitsAuPanier(produitQuantitePanierRepository, produitQuantitePanier, quantite);

        panierRepository.save(panierActuel);

        String logCreated = "Produit " + produitQuantitePanier.getProduit().getNom() + " supprimé avec quantité " + produitQuantitePanier.getQuantite() + " au panier " + panierActuel.getId() + " de " + userConnecte.getMail();
        log.info(logCreated);
        evenementService.enregistrerEvenement(userConnecte, logCreated);

        return panierActuel;

    }

    @Override
    public int getPointsDeFidelite(User user) {
        Panier panierActuel = panierRepository.findByUser(user);
        return panierActuel.getPointsFidelitePanier();
    }

    @Override
    public void viderPanierUser(User user) {

        Panier panierActuel = panierRepository.findByUser(user);

        panierActuel.setDate_creation(new Timestamp(new Date().getTime()));
        panierActuel.setPoidsPanier(0f);
        panierActuel.setPointsFidelitePanier(0);

        List<ProduitQuantitePanier> produitsQuantitePanier = produitQuantitePanierRepository.findByPanier(panierActuel);

        panierActuel.setProduits(new ArrayList<>());

        panierRepository.save(panierActuel);

        if(produitsQuantitePanier != null && produitsQuantitePanier.size() > 0){
            produitQuantitePanierRepository.delete(produitsQuantitePanier);
        }

        String logCreated = "Panier de " + panierActuel.getUser().getMail() + " vidé";
        log.info(logCreated);
        evenementService.enregistrerEvenement(panierActuel.getUser(), logCreated);
    }

}
