package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.phraseDuJour.PhraseDuJourException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.PhraseDuJour;
import com.cpe.yateli.model.SocketEventType;
import com.cpe.yateli.model.User;
import com.cpe.yateli.repository.PhraseDuJourRepository;
import com.cpe.yateli.repository.UserRepository;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.PhraseDuJourService;
import com.cpe.yateli.service.websocket.WebSocketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public class PhraseDuJourServiceImpl implements PhraseDuJourService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PhraseDuJourRepository phraseDuJourRepository;

    @Autowired
    private WebSocketService webSocketService;

    @Autowired
    private EvenementService evenementService;

    @Override
    public PhraseDuJour creerPhraseDuJour(Principal moi, String phrase, String urlImage) throws UserNonTrouveException {

        User admin = userRepository.findUserByMail(moi.getName());

        if(admin == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        PhraseDuJour phraseDuJour = PhraseDuJour.builder()
                .phrase(phrase)
                .admin(admin)
                .dateCreation(new Timestamp(new Date().getTime()))
                .build();

        if(!urlImage.isEmpty()){
            phraseDuJour.setUrlBackground(urlImage);
        }

        phraseDuJourRepository.save(phraseDuJour);

        webSocketService.envoyerTouslesUsers(SocketEventType.EDITION_PHRASE_JOUR, phraseDuJour);

        String logCreated = "L'administrateur " + admin.getMail() + " a créé une nouvelle phrase du jour.";
        log.info(logCreated);
        evenementService.enregistrerEvenement(admin, logCreated);

        return phraseDuJour;

    }

    @Override
    public PhraseDuJour getPhraseDuJour() throws PhraseDuJourException {

        List<PhraseDuJour> phrases = phraseDuJourRepository.findAll();
        PhraseDuJour phraseDuJourActuelle = Collections.max(phrases, Comparator.comparing(PhraseDuJour::getDateCreation));

        if(phraseDuJourActuelle == null || phraseDuJourActuelle.getPhrase().isEmpty()){
            throw PhraseDuJourException.creerAvec(HttpStatus.NOT_FOUND);
        }

        return phraseDuJourActuelle;

    }

    @Override
    public List<PhraseDuJour> getToutesPhrasesDuJour() throws PhraseDuJourException {

        List<PhraseDuJour> phraseDuJourList = phraseDuJourRepository.findAll();

        if(phraseDuJourList == null){
            throw PhraseDuJourException.creerAvec(HttpStatus.NOT_FOUND);
        }

        return phraseDuJourList;

    }

    @Override
    public PhraseDuJour modifierPhraseDuJour(Principal moi, String nouvellePhrase, String urlImage) throws UserNonTrouveException, PhraseDuJourException {

        User admin = userRepository.findUserByMail(moi.getName());

        if(admin == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        List<PhraseDuJour> phrases = phraseDuJourRepository.findAll();
        PhraseDuJour phraseDuJour = Collections.max(phrases, Comparator.comparing(PhraseDuJour::getDateCreation));

        if(phraseDuJour == null || phraseDuJour.getPhrase().isEmpty()){
            throw PhraseDuJourException.creerAvec(HttpStatus.NOT_FOUND);
        }

        if(nouvellePhrase == null || nouvellePhrase.isEmpty()){
            return phraseDuJour;
        }

        phraseDuJour.setPhrase(nouvellePhrase);

        if(!urlImage.isEmpty()){
            phraseDuJour.setUrlBackground(urlImage);
        }

        phraseDuJourRepository.save(phraseDuJour);

        webSocketService.envoyerTouslesUsers(SocketEventType.EDITION_PHRASE_JOUR, phraseDuJour);

        String logCreated = "Phrase du jour d'ID " + phraseDuJour.getId() + " modifiée par admin " + admin.getMail();
        log.info(logCreated);
        evenementService.enregistrerEvenement(admin, logCreated);

        return phraseDuJour;

    }

    @Override
    public String supprimerPhraseDuJour(Principal moi) throws UserNonTrouveException, PhraseDuJourException {

        User admin = userRepository.findUserByMail(moi.getName());

        if(admin == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        List<PhraseDuJour> phrases = phraseDuJourRepository.findAllByAdmin(admin);
        PhraseDuJour phraseDuJour = Collections.max(phrases, Comparator.comparing(PhraseDuJour::getDateCreation));

        if(phraseDuJour == null || phraseDuJour.getPhrase().isEmpty()){
            throw PhraseDuJourException.creerAvec(HttpStatus.NOT_FOUND);
        }

        phraseDuJourRepository.delete(phraseDuJour);
        phrases.remove(phraseDuJour);

        PhraseDuJour dernierePhraseDuJourAdmin = Collections.max(phrases, Comparator.comparing(PhraseDuJour::getDateCreation));

        if(dernierePhraseDuJourAdmin == null){
            dernierePhraseDuJourAdmin = PhraseDuJour.builder()
                    .phrase("Phrase du jour temporairement indisponible")
                    .admin(admin)
                    .dateCreation(new Timestamp(new Date().getTime()))
                    .build();
        }else{
            dernierePhraseDuJourAdmin.setDateCreation(new Timestamp(new Date().getTime()));
        }

        phraseDuJourRepository.save(dernierePhraseDuJourAdmin);

        webSocketService.envoyerTouslesUsers(SocketEventType.SUPPRESSION_PHRASE_JOUR, dernierePhraseDuJourAdmin.getPhrase());

        String logCreated = "Phrase du jour d'ID " + phraseDuJour.getId() + " supprimée par " + admin.getMail() + ". Remplacée par \"" + dernierePhraseDuJourAdmin.getPhrase() + "\"";
        log.info(logCreated);
        evenementService.enregistrerEvenement(admin, logCreated);

        return "OK";

    }

}
