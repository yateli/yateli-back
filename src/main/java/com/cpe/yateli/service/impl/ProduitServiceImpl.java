package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.artisan.ArtisanExistePasException;
import com.cpe.yateli.exception.artisan.ArtisanNonAutoriseProduitException;
import com.cpe.yateli.exception.panier.ProduitInexistantException;
import com.cpe.yateli.exception.produit.ProduitABloquerException;
import com.cpe.yateli.exception.produit.ProduitSansImageException;
import com.cpe.yateli.exception.produit.ReferenceIncorrecteSuppressionException;
import com.cpe.yateli.exception.produit.ReferenceProduitExisteDejaException;
import com.cpe.yateli.exception.user.UserBloqueException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.*;
import com.cpe.yateli.model.dto.BloquerProduitDto;
import com.cpe.yateli.model.dto.ProduitArtisanDto;
import com.cpe.yateli.model.dto.ProduitDto;
import com.cpe.yateli.model.dto.ProduitSuppressionDto;
import com.cpe.yateli.model.view.ProduitView;
import com.cpe.yateli.repository.*;
import com.cpe.yateli.service.AlerteService;
import com.cpe.yateli.service.ArtisanService;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.ProduitService;
import com.cpe.yateli.service.mail.EmailService;
import com.cpe.yateli.service.telephone.TelephoneService;
import com.cpe.yateli.service.websocket.WebSocketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.rowset.serial.SerialBlob;
import java.io.IOException;
import java.security.Principal;
import java.security.SecureRandom;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.cpe.yateli.model.SocketEventType.STOCK_INSUFFISANT;

@Service
public class ProduitServiceImpl implements ProduitService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ProduitRepository produitRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private ArtisanService artisanService;

    @Autowired
    private ProduitQuantitePanierRepository produitQuantitePanierRepository;

    @Autowired
    private PanierRepository panierRepository;

    @Autowired
    private CommandeRepository commandeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TelephoneService telephoneService;

    @Autowired
    private BlocageRepository blocageRepository;

    @Autowired
    private ColisMystereRepository colisMystereRepository;

    @Autowired
    private EmailService emailService;

    @Autowired
    private WebSocketService webSocketService;

    @Autowired
    private EvenementService evenementService;

    @Autowired
    private AlerteService alerteService;

    @Override
    public void mettreAJourStockProduit(Produit produit, int quantiteEnMoins) {
        if (produit.getQuantiteStock() > quantiteEnMoins) {
            produit.setQuantiteStock(produit.getQuantiteStock() - quantiteEnMoins);
        } else {
            produit.setQuantiteStock(0);
        }
        if (produit.getQuantiteStock() < ((produit.getQuantiteMinimum() == null) ? 5 : produit.getQuantiteMinimum()))
            alerteService.creerNotification(produit.getArtisan().getUser(),SocketEventType.STOCK_INSUFFISANT,produit);
        produitRepository.save(produit);
        webSocketService.envoyerTouslesUsers(SocketEventType.UPDATE_PRODUIT, produit);
    }

    private List<Produit> getProduitsAutorises() {
        return produitRepository.findAll().stream()
                .filter(p -> p.getBlocages().size() == 0 || !p.getBlocages().get(p.getBlocages().size() - 1).isActif())
                .collect(Collectors.toList());
    }

    @Override
    public List<ProduitView> getTousLesProduits() {
        List<Produit> produitsAutorises = this.getProduitsAutorises();

        List<ProduitView> tousLesProduitsView = new ArrayList<>();

        for (Produit produit : produitsAutorises) {
            tousLesProduitsView.add(ProduitView.creerProduitViewDepuisProduit(produit));
        }

        return tousLesProduitsView;
    }

    @Override
    public List<Produit> getTousLesProduitsAvecBloques() {
        return produitRepository.findAll();
    }

    @Override
    public List<ProduitView> getProduitsArtisan(Integer idArtisan) {
        List<Produit> produitsAutorisesArtisan = this.getProduitsAutorises().stream()
                                .filter(p -> p.getArtisan() != null && p.getArtisan().getId().equals(idArtisan))
                                .collect(Collectors.toList());

        List<ProduitView> produitsAutorisesArtisanView = new ArrayList<>();

        for(Produit produit : produitsAutorisesArtisan){
            produitsAutorisesArtisanView.add(ProduitView.creerProduitViewDepuisProduit(produit));
        }

        return produitsAutorisesArtisanView;
    }

    @Override
    public List<Blocage> bloquerProduits(Principal moi, BloquerProduitDto produits) throws ProduitABloquerException, UserNonTrouveException {
        User admin = userRepository.findUserByMail(moi.getName());

        if (admin == null) {
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND, moi.getName());
        }

        List<Blocage> blocagesFinaux = new ArrayList<>();
        List<Produit> produitsAbloquer = produitRepository.findAllByReferenceIn(new HashSet<>(produits.getReferences()));

        if (produitsAbloquer == null || produitsAbloquer.size() == 0) {
            throw ProduitABloquerException.creerAvec(HttpStatus.NOT_FOUND);
        }

        Map<Artisan, List<Produit>> produitsBloques = new HashMap<>();
        for (Produit produit : produitsAbloquer) {

            List<Blocage> blocages = produit.getBlocages();

            if (blocages.size() == 0 || !blocages.get(blocages.size() - 1).isActif()) {
                Blocage blocage = Blocage.builder()
                        .date(new Timestamp(new Date().getTime()))
                        .admin(admin)
                        .typeBlocage(TypeBlocage.PRODUIT)
                        .notification(new ArrayList<>())
                        .actif(true)
                        .build();

                if (!produits.getRaison().isEmpty()) {
                    blocage.setRaison(produits.getRaison());
                }

                blocageRepository.save(blocage);
                blocagesFinaux.add(blocage);

                //Si un même artisan a plusieurs de ses articles bloqués
                List<Produit> produitsArtisan = produitsBloques.get(produit.getArtisan());
                if (produitsArtisan != null && produitsArtisan.size() > 0) {
                    //Ajout si même artisan
                    produitsArtisan.add(produit);
                } else {
                    //Ajout si nouvel artisan
                    List<Produit> nouvelleListeProduitArtisan = new ArrayList<>();
                    nouvelleListeProduitArtisan.add(produit);
                    produitsBloques.put(produit.getArtisan(), nouvelleListeProduitArtisan);
                }

                produit.getBlocages().add(blocage);
                produitRepository.save(produit);

                String logCreated = "Le produit de référence " + produit.getReference() + " (" + produit.getNom() + ") a correctement été bloqué";
                log.info(logCreated);
                evenementService.enregistrerEvenement(null, logCreated);
            }
        }

        Thread envoiNotificationArtisan = new Thread(() -> {
            for (Map.Entry<Artisan, List<Produit>> entry : produitsBloques.entrySet()) {
                List<Notification> notifications = new ArrayList<>();
                String raisonBlocage = "";

                Artisan artisan = entry.getKey();
                List<Produit> produitsArtisanBloques = entry.getValue();

                StringBuilder sb = new StringBuilder();
                int cpt = 0;
                for (Produit produit : produitsArtisanBloques) {
                    sb.append(produit.getReference());
                    if (cpt < produitsArtisanBloques.size() - 1) sb.append(", ");
                    cpt++;

                    raisonBlocage = produit.getBlocages().get(produit.getBlocages().size() - 1).getRaison();
                }

                if (!artisan.getTelephone().isEmpty()) {
                    telephoneService.envoyerSms(artisan.getTelephone(), "Yateli : Vos références produit " + sb.toString() + " ont été bloqués (Voir raison dans le mail envoyé)");
                    notifications.add(Notification.SMS);
                    String logCreated = "SMS de notification de référence(s) bloquée(s) envoyé à " + artisan.getTelephone();
                    log.info(logCreated);
                    evenementService.enregistrerEvenement(artisan.getUser(), logCreated);
                } else if (!artisan.getUser().getTelephone().isEmpty()) {
                    telephoneService.envoyerSms(artisan.getUser().getTelephone(), "Yateli : Vos références produit " + sb.toString() + " ont été bloqués (Voir raison dans le mail envoyé)");
                    notifications.add(Notification.SMS);
                    String logCreated = "SMS de notification de référence(s) produit(s) bloquée(s) envoyé à " + artisan.getUser().getTelephone();
                    log.info(logCreated);
                    evenementService.enregistrerEvenement(artisan.getUser(), logCreated);
                }

                Date now = new Date();
                String heureBlocage = new SimpleDateFormat("HH:mm").format(now);
                String dateBlocage = new SimpleDateFormat("dd MMMMM yyyy").format(now);

                Map<String, Object> variablesMail = new HashMap<>();
                variablesMail.put("prenom", artisan.getUser().getPrenom());
                variablesMail.put("produits", produitsArtisanBloques);
                variablesMail.put("heureBlocage", heureBlocage);
                variablesMail.put("dateBlocage", dateBlocage);
                variablesMail.put("raisonBlocage", raisonBlocage);
                variablesMail.put("allerSurSite", Client.host + "/#!/profile");
                Map<String, String> imagesAAjouter = new HashMap<>();
                imagesAAjouter.put("block", "./static/block.gif");
                emailService.sendMail(artisan.getUser().getMail(), "Yateli : Certains de vos produits ont été bloqués", variablesMail, imagesAAjouter, null, "mailProduitBloque.ftl");

                //Définition du type de notification dans blocage
                notifications.add(Notification.MAIL);
                for (Produit produit : produitsArtisanBloques) {
                    Blocage dernierBlocage = produit.getBlocages().get(produit.getBlocages().size() - 1);
                    dernierBlocage.setNotification(notifications);
                    blocageRepository.save(dernierBlocage);
                }

                String logCreated = "Mail de notification de référence(s) produit(s) bloquée(s) envoyé à " + artisan.getUser().getMail();
                log.info(logCreated);
                evenementService.enregistrerEvenement(artisan.getUser(), logCreated);

            }
        });
        envoiNotificationArtisan.start();

        return blocagesFinaux;

    }

    @Override
    public List<Produit> autoriserProduits(BloquerProduitDto produits) throws ProduitABloquerException, UserNonTrouveException {
        List<Produit> produitsAutorises = new ArrayList<>();
        List<Produit> produitsAautoriser = produitRepository.findAllByReferenceIn(new HashSet<>(produits.getReferences()));

        if (produitsAautoriser == null || produitsAautoriser.size() == 0) {
            throw ProduitABloquerException.creerAvec(HttpStatus.NOT_FOUND);
        }

        for (Produit produit : produitsAautoriser) {
            if (produit.getBlocages().size() > 0) {
                Blocage dernierBlocage = produit.getBlocages().get(produit.getBlocages().size() - 1);

                if (dernierBlocage != null && dernierBlocage.isActif()) {
                    dernierBlocage.setActif(false);
                    blocageRepository.save(dernierBlocage);
                    produitsAutorises.add(produit);
                    String logCreated = "Le produit de référence " + produit.getReference() + " (" + produit.getNom() + ") a correctement été débloqué";
                    log.info(logCreated);
                    evenementService.enregistrerEvenement(null, logCreated);
                }
            }
        }

        return produitsAutorises;

    }

    @Override
    public InfoFichier recupererImage(String reference) throws ProduitInexistantException, SQLException, ProduitSansImageException {

        Produit produit = produitRepository.findProduitByReference(reference);

        if (produit == null) {
            throw ProduitInexistantException.creerAvec(HttpStatus.NOT_FOUND, reference);
        }

        if (produit.getImage() == null) {
            throw ProduitSansImageException.creerAvec(HttpStatus.NOT_FOUND, reference);
        }

        return new InfoFichier(produit.getExtensionImage(), new InputStreamResource(produit.getImage().getBinaryStream()));

    }

    @Override
    public Produit creerProduit(ProduitArtisanDto produitDto, MultipartFile image) throws ReferenceProduitExisteDejaException, ArtisanExistePasException, IOException, SQLException, UserBloqueException {

        Produit produitExiste = produitRepository.findProduitByReference(produitDto.getReference());

        if (produitExiste != null) {
            throw ReferenceProduitExisteDejaException.creerAvec(HttpStatus.BAD_REQUEST, produitExiste.getReference());
        }

        //Récupère Artisan du Produit
        Artisan artisan = artisanService.getArtisan(produitDto.getMailArtisan());
        if (artisan == null) {
            throw ArtisanExistePasException.creerAvec(HttpStatus.NOT_FOUND, produitDto.getMailArtisan());
        }

        if(!artisan.isEstActif()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, artisan.getUser().getMail());
        }

        Produit produit = Produit.builder()
                .reference(produitDto.getReference())
                .nom(produitDto.getNom())
                .description(produitDto.getDescription())
                .date_ajout(new Timestamp(new Date().getTime()))
                .tags(new HashSet<>())
                .pointsFidelite(0)
                .quantiteMinimum(0)
                .tauxTaxe(0)
                .prixHT(produitDto.getPrixHT())
                .poids(produitDto.getPoids())
                .quantiteStock(produitDto.getQuantiteStock())
                .blocages(new ArrayList<>())
                .build();

        //Récupération Tags
        if(produit.getTags() != null && produit.getTags().size() > 0){
            List<Tag> tags = tagRepository.findTagsByNomIn(new HashSet<>(produitDto.getNomsTag()));
            if (tags != null && !tags.isEmpty()) {
                for (Tag tag : tags) {
                    produit.ajoutTag(tag);
                }
            }
        }

        //Assignation Artisan
        produit.setArtisan(artisan);
        artisan.getProduits().add(produit);

        //Affectation image
        if (image != null) {
            produit.setImage(new SerialBlob(image.getBytes()));
        }

        produitRepository.save(produit);

        Blocage blocage = Blocage.builder()
                .date(new Timestamp(new Date().getTime()))
                .admin(null)
                .typeBlocage(TypeBlocage.NOUVEAU_PRODUIT)
                .notification(new ArrayList<>())
                .actif(true)
                .build();

        blocageRepository.save(blocage);

        produit.getBlocages().add(blocage);

        produitRepository.save(produit);

        artisanService.enregistrerArtisan(artisan);

        Thread enregistrementAlerte = new Thread(() -> {
            String logCreated = "Produit " + produit.getId() + " de référence " + produit.getReference() + " créé pour l'artisan " + produit.getArtisan().getUser().getMail();
            log.info(logCreated);
            evenementService.enregistrerEvenement(produit.getArtisan().getUser(), logCreated);

            alerteService.creerNotificationRole("ADMIN", SocketEventType.PRODUIT_CREE, produit.getReference());
        });
        enregistrementAlerte.start();

        return produit;

    }

    @Override
    public Produit modifierProduit(ProduitDto produitDto, MultipartFile image) throws ProduitInexistantException, IOException, SQLException, UserBloqueException {

        Produit produit = produitRepository.findProduitByReference(produitDto.getReference());

        if (produit == null) {
            throw ProduitInexistantException.creerAvec(HttpStatus.NOT_FOUND, produitDto.getReference());
        }

        if (produitDto.getNom() != null && !produit.getNom().equalsIgnoreCase(produitDto.getNom())) {
            produit.setNom(produitDto.getNom());
        }
        if (produitDto.getDescription() != null && !produit.getDescription().equalsIgnoreCase(produitDto.getDescription())) {
            produit.setDescription(produitDto.getDescription());
        }
        if (produitDto.getPrixHT() != null && !produitDto.getPrixHT().equals(produit.getPrixHT())) {
            produit.setPrixHT(produitDto.getPrixHT());
        }
        if (produitDto.getTauxTaxe() != 0.0f && produit.getTauxTaxe() != produitDto.getTauxTaxe()) {
            produit.setTauxTaxe(produitDto.getTauxTaxe());
        }
        if (produitDto.getPoids() != 0.0f && !produit.getPoids().equals(produitDto.getPoids())) {
            produit.setPoids(produitDto.getPoids());
        }
        if (produitDto.getPointsFidelite() != 0 && !produit.getPointsFidelite().equals(produitDto.getPointsFidelite())) {
            produit.setPointsFidelite(produitDto.getPointsFidelite());
        }
        if (produitDto.getQuantiteStock() != 0 && !produit.getQuantiteStock().equals(produitDto.getQuantiteStock())) {
            produit.setQuantiteStock(produitDto.getQuantiteStock());
        }
        if (produitDto.getQuantiteMinimum() != 0 && !produit.getQuantiteMinimum().equals(produitDto.getQuantiteMinimum())) {
            produit.setQuantiteMinimum(produitDto.getQuantiteMinimum());
        }

        produit.calculTaxe();

        if (produitDto.getNomsTag() != null && produitDto.getNomsTag().size() > 0) {
            //Suppression Tags produit avant rechargement
            Set<Tag> tagsASupprimer = produit.getTags();
            for (Tag tag : new HashSet<>(tagsASupprimer)) {
                produit.supprimerTag(tag);
            }

            //Modification Tags
            List<Tag> tags = tagRepository.findTagsByNomIn(new HashSet<>(produitDto.getNomsTag()));
            if (tags != null && !tags.isEmpty()) {
                for (Tag tag : tags) {
                    produit.ajoutTag(tag);
                }
            }
        }

        //Affectation image
        if (image != null) {
            produit.setImage(new SerialBlob(image.getBytes()));
            produit.setExtensionImage(image.getContentType()); //On stocke l'extension
        }

        Artisan artisan = artisanService.getArtisan(produitDto.getMailArtisan());

        if (artisan != null) {

            if(!artisan.isEstActif()){
                throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, artisan.getUser().getMail());
            }

            if (produit.getArtisan() != artisan) {
                produit.getArtisan().getProduits().remove(produit);
                artisanService.enregistrerArtisan(produit.getArtisan()); //enleve ancien produit la liste de l'artisan
                artisan.getProduits().add(produit);
                produit.setArtisan(artisan);
                artisanService.enregistrerArtisan(artisan);//ajout liste produits nouvel artisan
            }

            List<Blocage> nouveauxProduits = produit.getBlocages().stream()
                    .filter(p -> p.getTypeBlocage() == TypeBlocage.NOUVEAU_PRODUIT)
                    .collect(Collectors.toList());

            if(nouveauxProduits != null && nouveauxProduits.size() > 0){
                produit.getBlocages().removeAll(nouveauxProduits);
                blocageRepository.delete(nouveauxProduits);
                String logCreated = "Première activation du produit " + produit.getReference();
                log.info(logCreated);
                evenementService.enregistrerEvenement(artisan.getUser(), logCreated);
            }

            produitRepository.save(produit);

            String logCreated = "Produit " + produit.getId() + " de référence " + produit.getReference() + " vient d'être modifié";
            log.info(logCreated);
            evenementService.enregistrerEvenement(artisan.getUser(), logCreated);

        }

        return produit;

    }

    @Override
    public Produit modifierProduitParArtisan(ProduitArtisanDto produitDto, MultipartFile image) throws ProduitInexistantException, IOException, SQLException, UserBloqueException {

        Produit produit = produitRepository.findProduitByReference(produitDto.getReference());

        if (produit == null) {
            throw ProduitInexistantException.creerAvec(HttpStatus.NOT_FOUND, produitDto.getReference());
        }

        if (produitDto.getNom() != null && !produit.getNom().equalsIgnoreCase(produitDto.getNom())) {
            produit.setNom(produitDto.getNom());
        }
        if (produitDto.getDescription() != null && !produit.getDescription().equalsIgnoreCase(produitDto.getDescription())) {
            produit.setDescription(produitDto.getDescription());
        }
        if (produitDto.getPrixHT() != null && !produitDto.getPrixHT().equals(produit.getPrixHT())) {
            produit.setPrixHT(produitDto.getPrixHT());
        }
        if (produitDto.getPoids() != 0.0f && !produit.getPoids().equals(produitDto.getPoids())) {
            produit.setPoids(produitDto.getPoids());
        }
        if (produitDto.getQuantiteStock() != 0 && !produit.getQuantiteStock().equals(produitDto.getQuantiteStock())) {
            produit.setQuantiteStock(produitDto.getQuantiteStock());
        }

        produit.calculTaxe();

        if (produitDto.getNomsTag() != null && produitDto.getNomsTag().size() > 0) {
            //Suppression Tags produit avant rechargement
            Set<Tag> tagsASupprimer = produit.getTags();
            for (Tag tag : new HashSet<>(tagsASupprimer)) {
                produit.supprimerTag(tag);
            }

            //Modification Tags
            List<Tag> tags = tagRepository.findTagsByNomIn(new HashSet<>(produitDto.getNomsTag()));
            if ((tags != null) && !tags.isEmpty()) {
                for (Tag tag : tags) {
                    produit.ajoutTag(tag);
                }
            }
        }

        //Affectation image
        if (image != null) {
            produit.setImage(new SerialBlob(image.getBytes()));
            produit.setExtensionImage(image.getContentType()); //On stocke l'extension
        }

        produitRepository.save(produit);

        String logCreated = "Produit " + produit.getId() + " de référence " + produit.getReference() + " vient d'être modifié";
        log.info(logCreated);
        evenementService.enregistrerEvenement(produit.getArtisan().getUser(), logCreated);

        return produit;

    }

    @Override
    public String supprimerProduit(Principal moi, ProduitSuppressionDto produitSuppressionDto) throws ReferenceIncorrecteSuppressionException, ArtisanNonAutoriseProduitException, UserBloqueException {

        Artisan artisan = artisanService.getArtisan(moi.getName());

        List<String> referencesNonCorrect = new ArrayList<>();

        String artisanNonAutorise = "";
        List<String> nonAutorisesArtisanProduit = new ArrayList<>();

        for (String reference : produitSuppressionDto.getReferenceProduits()) {

            Produit produit = produitRepository.findProduitByReference(reference);

            if (produit == null) {
                referencesNonCorrect.add(reference);
            } else {
                if (artisan != null && artisan != produit.getArtisan()) {

                    if(!artisan.isEstActif()){
                        throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, artisan.getUser().getMail());
                    }

                    artisanNonAutorise = artisan.getUser().getMail();
                    nonAutorisesArtisanProduit.add(reference);
                }

                Set<Tag> tagsASupprimer = produit.getTags();
                for (Tag tag : new HashSet<>(tagsASupprimer)) {
                    produit.supprimerTag(tag);
                }

                produit.getArtisan().getProduits().remove(produit);
                artisanService.enregistrerArtisan(produit.getArtisan());

                //Suppression produit différents paniers
                List<ProduitQuantitePanier> produitsQuantitePanier = produitQuantitePanierRepository.findAllByProduit(produit);
                for (ProduitQuantitePanier produitQuantitePanier : produitsQuantitePanier) {
                    // Suppression panier
                    Panier panierAvecProduitASupprimer = panierRepository.findByProduitsContaining(produitQuantitePanier);
                    if(panierAvecProduitASupprimer != null){
                        panierAvecProduitASupprimer.supprimerProduitsAuPanier(produitQuantitePanierRepository, produitQuantitePanier, null); //Suppression produit panier
                        panierRepository.save(panierAvecProduitASupprimer);
                        String logCreated = "Produit " +produitQuantitePanier.getProduit().getReference() + " supprimé du panier de " + panierAvecProduitASupprimer.getUser().getMail();
                        log.info(logCreated);
                        evenementService.enregistrerEvenement(panierAvecProduitASupprimer.getUser(), logCreated);
                    }

                    // Suppression commande
                    List<Commande> commandesAvecProduitASupprimer = commandeRepository.findAllByProduitsContaining(produitQuantitePanier);
                    for(Commande commande : commandesAvecProduitASupprimer){
                        commande.getProduits().remove(produitQuantitePanier);
                        commandeRepository.save(commande);
                        String logCreated = "Produit " + produitQuantitePanier.getProduit().getReference() + " supprimé de la commande " + commande.getId() + " de " + commande.getUser().getMail();
                        log.info(logCreated);
                        evenementService.enregistrerEvenement(commande.getUser(), logCreated);
                    }

                    produitQuantitePanierRepository.delete(produitQuantitePanier);
                }

                //Suppression produits Colis Mystère
                List<ColisMystere> colisMystereList = colisMystereRepository.findAllByProduitIs(produit);
                for(ColisMystere colisMystere : colisMystereList){
                    colisMystere.getUser().retirerColisMystere(colisMystere);
                    userRepository.save(colisMystere.getUser());

                    colisMystereRepository.delete(colisMystere);
                }

                produitRepository.delete(produit);

                String logCreated = "Produit " + produit.getReference() + " a été supprimé";
                log.info(logCreated);
                evenementService.enregistrerEvenement(null, logCreated);
            }

        }

        if (nonAutorisesArtisanProduit.size() > 0) {
            throw ArtisanNonAutoriseProduitException.creerAvec(HttpStatus.METHOD_NOT_ALLOWED, artisanNonAutorise, nonAutorisesArtisanProduit);
        }

        if (referencesNonCorrect.size() > 0) {
            throw ReferenceIncorrecteSuppressionException.creerAvec(HttpStatus.NOT_FOUND, referencesNonCorrect);
        }

        return "OK";

    }

    @Override
    public Produit affectationColisMystere(User utilisateur, Tag preference) {

        SecureRandom random = new SecureRandom();
        List<Produit> tousLesProduits = this.getProduitsAutorises();
        List<Produit> produitsPreferes = produitRepository.findAllByTagsContaining(preference);

        //Liste de produits achetés dans le mois
        List<Produit> produitsAchetes = new ArrayList<>();
        List<Commande> commandesEffectueesDansLeMois = commandeRepository.findAllByUser(utilisateur);
        for (Commande commande : commandesEffectueesDansLeMois) {
            for (ProduitQuantitePanier produitQuantitePanier : commande.getProduits()) {
                if (!produitsAchetes.contains(produitQuantitePanier.getProduit())) { //Seulement des produits différents
                    produitsAchetes.add(produitQuantitePanier.getProduit());
                }
            }
        }

        if (produitsPreferes == null || produitsPreferes.size() == 0) {
            //Envoi d'un produit aléatoire sans préférence

            List<Produit> tousLesProduitsMoinsDejaAchetes = new ArrayList<>(tousLesProduits);

            tousLesProduitsMoinsDejaAchetes.removeAll(produitsAchetes); //On supprime les produits déjà achetés ce mois-ci
            Produit produitAffecte = null;
            if (tousLesProduitsMoinsDejaAchetes.size() > 0) {
                //Sélection d'un produit aléatoire dans la liste réduite (produit non commandé ce mois-ci)
                produitAffecte = tousLesProduitsMoinsDejaAchetes.get(random.nextInt(tousLesProduitsMoinsDejaAchetes.size()));
            } else {
                //Sélection d'un produit aléatoire dans toute la liste de produit en vente
                produitAffecte = tousLesProduits.get(random.nextInt(tousLesProduits.size()));
            }

            String logCreated = "Produit " + produitAffecte.getReference() + " affecté sans préférence à l'utilisateur " + utilisateur.getMail() + " car aucun produit comportant ces tags";
            log.info(logCreated);
            evenementService.enregistrerEvenement(utilisateur, logCreated);

            return produitAffecte;
        }

        List<Produit> produitsPreferesMoinsDejaAchetes = new ArrayList<>(produitsPreferes);
        produitsPreferesMoinsDejaAchetes.removeAll(produitsAchetes); //On supprime les produits déjà achetés ce mois-ci

        Produit produitAffecteSelonPreference = null;
        if (produitsPreferesMoinsDejaAchetes.size() > 0) {
            //Envoi d'un produit aléatoire selon préférence qui n'a pas été commandé ce mois-ci
            produitAffecteSelonPreference = produitsPreferesMoinsDejaAchetes.get(random.nextInt(produitsPreferesMoinsDejaAchetes.size()));
        } else {
            //Sélection d'un produit aléatoire dans toute la liste de produit en vente
            produitAffecteSelonPreference = tousLesProduits.get(random.nextInt(tousLesProduits.size()));
        }

        String logCreated = "Produit " + produitAffecteSelonPreference.getReference() + " affecté selon préférence de l'utilisateur " + utilisateur.getMail();
        log.info(logCreated);
        evenementService.enregistrerEvenement(utilisateur, logCreated);

        return produitAffecteSelonPreference;

    }

    @Override
    public ProduitView getDetailsProduit(String reference) throws ProduitInexistantException {

        Produit produit = produitRepository.findProduitByReference(reference);

        if(produit == null){
            throw ProduitInexistantException.creerAvec(HttpStatus.NOT_FOUND, reference);
        }

        return ProduitView.creerProduitViewDepuisProduit(produit);
    }

}
