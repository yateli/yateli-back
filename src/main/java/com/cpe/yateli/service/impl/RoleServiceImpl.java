package com.cpe.yateli.service.impl;

import com.cpe.yateli.model.Role;
import com.cpe.yateli.model.User;
import com.cpe.yateli.repository.RoleRepository;
import com.cpe.yateli.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void delierRoleUser(User user) {

        Role role = roleRepository.findRoleByNom(user.getRole().getNom());
        if(role != null){
            role.getUsers().remove(user);
            roleRepository.save(role);
        }

    }

}
