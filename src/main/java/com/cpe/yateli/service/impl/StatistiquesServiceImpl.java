package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.excel.AucuneDonneeExcelException;
import com.cpe.yateli.exception.excel.ErreurGenerationException;
import com.cpe.yateli.exception.user.UserNonTrouveException;
import com.cpe.yateli.model.InfoFichier;
import com.cpe.yateli.model.User;
import com.cpe.yateli.repository.UserRepository;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.StatistiquesService;
import com.cpe.yateli.service.excel.ExcelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.security.Principal;
import java.util.Date;

@Service
public class StatistiquesServiceImpl implements StatistiquesService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EvenementService evenementService;

    @Autowired
    private ExcelService excelService;

    @Override
    public InfoFichier genererExcel(Principal moi, String nomFichier, Long timestampMinimum, String login) throws UserNonTrouveException, IOException, ErreurGenerationException, AucuneDonneeExcelException {

        User admin = userRepository.findUserByMail(moi.getName());

        if(admin == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND, moi.getName());
        }

        MultipartFile excel = excelService.generateExcel(nomFichier, new Date(timestampMinimum), login);

        if(excel == null){
            throw ErreurGenerationException.creerAvec(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        String logCreated = "Récupération d'un fichier Excel avec les données des questions par l'administrateur " + admin.getMail();
        log.info(logCreated);
        evenementService.enregistrerEvenement(admin, logCreated);

        return InfoFichier.builder()
                .fichier(new InputStreamResource(excel.getInputStream()))
                .extension(excel.getContentType())
                .build();

    }

}
