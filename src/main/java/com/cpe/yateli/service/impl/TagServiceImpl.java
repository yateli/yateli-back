package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.tag.TagExisteDejaException;
import com.cpe.yateli.exception.tag.TagExistePasException;
import com.cpe.yateli.model.Produit;
import com.cpe.yateli.model.Tag;
import com.cpe.yateli.model.User;
import com.cpe.yateli.repository.TagRepository;
import com.cpe.yateli.service.EvenementService;
import com.cpe.yateli.service.TagService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TagServiceImpl implements TagService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private EvenementService evenementService;

    @Override
    public Tag creerTag(String nom) throws TagExisteDejaException {

        Tag tag = tagRepository.findTagByNom(nom.toUpperCase());

        if(tag != null){
            throw TagExisteDejaException.creerAvec(HttpStatus.CONFLICT, tag.getNom());
        }

        Tag tagACreer = Tag.builder()
                .nom(nom.toUpperCase())
                .build();

        tagRepository.save(tagACreer);

        String logCreated = "Un nouveau tag d'ID " + tagACreer.getId() + " a été créé : " + tagACreer.getNom();
        log.info(logCreated);
        evenementService.enregistrerEvenement(null, logCreated);

        return tagACreer;

    }

    @Override
    public Tag modifierTag(int id, String nouveauNom) throws TagExistePasException {

        Tag tag = tagRepository.findTagById(id);

        if(tag == null){
            throw TagExistePasException.creerAvec(HttpStatus.PRECONDITION_FAILED, String.valueOf(id));
        }

        tag.setNom(nouveauNom);

        tagRepository.save(tag);

        String logCreated = "Tag d'ID " + tag.getId() + " modifié. Nouveau nom : " + tag.getNom();
        log.info(logCreated);
        evenementService.enregistrerEvenement(null, logCreated);

        return tag;

    }

    @Override
    public String supprimerTag(int id) throws TagExistePasException {

        Tag tag = tagRepository.findTagById(id);

        if(tag == null){
            throw TagExistePasException.creerAvec(HttpStatus.PRECONDITION_FAILED, String.valueOf(id));
        }

        Set<User> users = tag.getUsers();
        Set<Produit> produits = tag.getProduits();

        for(User user : new HashSet<>(users)){
            user.supprimerEnvie(tag);
        }

        for(Produit produit : new HashSet<>(produits)){
            produit.supprimerTag(tag);
        }

        tagRepository.delete(tag);

        String logCreated = "Tag d'ID " + tag.getId() + " (" + tag.getNom() + ") supprimé";
        log.info(logCreated);
        evenementService.enregistrerEvenement(null, logCreated);

        return "OK";

    }

    @Override
    public List<Tag> getTousLesTags() {
        return tagRepository.findAll();
    }

}
