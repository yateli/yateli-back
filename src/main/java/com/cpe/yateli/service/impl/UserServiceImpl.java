package com.cpe.yateli.service.impl;

import com.cpe.yateli.exception.role.RoleExistePasException;
import com.cpe.yateli.exception.user.*;
import com.cpe.yateli.model.*;
import com.cpe.yateli.model.dto.TagDto;
import com.cpe.yateli.model.dto.UserDto;
import com.cpe.yateli.model.dto.UserModificationDto;
import com.cpe.yateli.model.statistiques.InterrogationStat;
import com.cpe.yateli.model.statistiques.Questionnaire;
import com.cpe.yateli.model.statistiques.ReponseStat;
import com.cpe.yateli.model.view.UserView;
import com.cpe.yateli.repository.*;
import com.cpe.yateli.service.*;
import com.cpe.yateli.service.mail.EmailService;
import com.cpe.yateli.service.telephone.TelephoneService;
import com.cpe.yateli.service.usersConnectes.UserConnecte;
import com.cpe.yateli.service.usersConnectes.UserConnecteService;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PanierRepository panierRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ColisMystereService colisMystereService;

    @Autowired
    private BonAchatRepository bonAchatRepository;

    @Autowired
    private FactureService factureService;

    @Autowired
    private ArtisanService artisanService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private TelephoneService telephoneService;

    @Autowired
    private AlerteService alerteService;

    @Autowired
    private UserConnecteService userConnecteService;

    @Autowired
    private CommandeService commandeService;

    @Autowired
    private ReponseRepository reponseRepository;

    @Autowired
    private QuestionnaireRepository questionnaireRepository;

    @Autowired
    private InterrogationRepository interrogationRepository;

    @Autowired
    private EvenementService evenementService;

    @Override
    public String getTokenWebsocket(Principal moi) throws UserNonTrouveException, UserBloqueException {

        User user = userRepository.findUserByMail(moi.getName());

        if(user == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!user.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, user.getMail());
        }

        return userConnecteService.genererTokenWS(user);

    }

    @Override
    public List<UserConnecte> getConnectedUsers(Principal moi,String role) throws UserNonTrouveException {

        User admin = userRepository.findUserByMail(moi.getName());

        if(admin == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        List<UserConnecte> usersGeneres = userConnecteService.getUsersConnectes().stream()
                .filter(p -> p.isWebSocketActif() && p.getRole().equals(role))
                .collect(Collectors.toList());

        String logCreated = "Récupération des Users connectes par " + admin.getMail() + " . Nombre de Users en ligne : " + String.valueOf(usersGeneres.size());
        log.info(logCreated);
        evenementService.enregistrerEvenement(admin, logCreated);

        return usersGeneres;

    }

    @Override
    public String logoutSpecificUser(String login, String raison) throws UserNonTrouveException, UserNonConnecteException {

        User userConnecte = userRepository.findUserByMail(login);

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,login);
        }

        if(!userConnecteService.userEstConnecte(userConnecte.getMail())){
            throw UserNonConnecteException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        String date = new SimpleDateFormat("HH:mm").format(new Date());
        String texteEnvoye = "Un administrateur vous a déconnecté à " + date + ".";

        if(raison != null && !raison.isEmpty()){
            texteEnvoye += " Raison : " + raison;
        }

        alerteService.creerNotification(userConnecte, SocketEventType.DECONNEXION, texteEnvoye);

        String logCreated = "Un administrateur a déconnecté le User " + userConnecte.getMail() + " à " + date;
        log.info(logCreated);
        evenementService.enregistrerEvenement(userConnecte, logCreated);

        return "User " + userConnecte.getMail() + " déconnecté avec message : " + texteEnvoye;

    }

    @Override
    public User creerUser(UserDto userJson) throws UserExisteDejaException, PhoneFormatIncorrectException, MailNonCorrectException, FormatMotDePasseIncorrecteException {

        // Vérification si le User existe déjà
        User userExistant = userRepository.findUserByMail(userJson.getMail());

        if(userExistant != null){
            // Comme le User existe déjà, je lève une exception qui va être renvoyée au front
            throw UserExisteDejaException.creerAvec(HttpStatus.CONFLICT,userJson.getMail());
        }

        if(!UserServiceImpl.mailEstCorrect(userJson.getMail())){
            throw MailNonCorrectException.creerAvec(HttpStatus.PRECONDITION_FAILED, userJson.getMail());
        }

        if(!UserServiceImpl.motDePasseEstCorrect(userJson.getPassword())){
            throw FormatMotDePasseIncorrecteException.creerAvec(HttpStatus.PRECONDITION_FAILED);
        }

        //Création du User
        User nouveauUser = User.builder()
                .prenom(userJson.getPrenom())
                .mail(userJson.getMail())
                .password(passwordEncoder.encode(userJson.getPassword()))
                .role(null)
                .dateCreation(new Timestamp(new Date().getTime()))
                .pointsFidelite(0)
                .bonAchats(new ArrayList<>())
                .listeEnvies(new HashSet<>())
                .estAbonne(false)
                .build();

        if(userJson.getTelephone() != null && !userJson.getTelephone().isEmpty()){
            nouveauUser.setTelephone(userJson.getTelephone());
            telephoneService.ajouterNumeroAListeReception(nouveauUser.getMail(),userJson.getTelephone());
        }

        // Récupération du rôle du User dans le Json
        Role roleUser = null;
        if(userJson.getRole() != null && !userJson.getRole().getNom().isEmpty()){
            roleUser = roleRepository.findRoleByNom(userJson.getRole().getNom());
            if(roleUser != null) {
                roleUser.getUsers().add(nouveauUser);
                nouveauUser.setRole(roleUser);
            }
        }else{
            String logCreated = "Le User " + userJson.getMail() + " n'a pas de rôle !";
            log.error(logCreated);
            User user = userRepository.findUserByMail(userJson.getMail());
            evenementService.enregistrerEvenement(user, logCreated);
        }

        // Récupération de la liste d'envies du User à partir du Json
    /*    List<TagDto> listeEnviesJson = userJson.getListeEnvies();
        if(listeEnviesJson != null && listeEnviesJson.size() > 0){
            for(TagDto envieJson : listeEnviesJson){
                Tag envie = tagRepository.findTagByNom(envieJson.getNom());
                if(envie != null) nouveauUser.ajoutEnvie(envie); // Enregistrement de l'envie à notre User
            }
        }else{
            log.debug("Pas de liste d'envies pour le User {}", userJson.getMail());
        } */

        List<InterrogationStat> interrogationStats = new ArrayList<>();
        List<Reponse> reponsesUser = reponseRepository.findAllByIdIn(userJson.getReponses());
        if(reponsesUser != null && reponsesUser.size() > 0){
            for(Reponse reponse : reponsesUser){
                List<String> tagsString = new ArrayList<>();

                for(Tag tag : reponse.getTags()){
                    tagsString.add(tag.getNom());
                    nouveauUser.ajoutEnvie(tag);
                }

                ReponseStat reponseStat = ReponseStat.builder()
                        .reponse(reponse.getTexte())
                        .tags(tagsString)
                        .build();

                InterrogationStat interrogationStat = InterrogationStat.builder()
                        .question(reponse.getInterrogation().getQuestion())
                        .reponse(reponseStat)
                        .build();

                interrogationStats.add(interrogationStat);
            }
            userRepository.save(nouveauUser);

            //Enregistrement Reporting
            Questionnaire questionnaire = Questionnaire.builder()
                    .mail(nouveauUser.getMail())
                    .date(Instant.now().plus(1, ChronoUnit.HOURS))
                    .interrogations(interrogationStats)
                    .build();
            questionnaireRepository.save(questionnaire);
        }

        // Création panier du User
        Panier panier = Panier.builder()
                .date_creation(new Timestamp(new Date().getTime()))
                .user(nouveauUser)
                .produits(new ArrayList<>())
                .poidsPanier(0f)
                .pointsFidelitePanier(0)
                .build();

        //Affectation Panier au User
        nouveauUser.setPanier(panier);
        Panier panierSaved = panierRepository.save(panier);
        String logCreated = "Le User " + nouveauUser.getMail() + " a un Panier d'id : " + panierSaved.getId();
        log.info(logCreated);
        evenementService.enregistrerEvenement(nouveauUser, logCreated);

        userRepository.save(nouveauUser);

        Thread envoiMail = new Thread(() -> {
            Map<String,Object> variablesMail = new HashMap<>();
            variablesMail.put("prenom", nouveauUser.getPrenom());
            variablesMail.put("login", nouveauUser.getMail());
            variablesMail.put("urlActivation", Client.host + "/#!/activerCompte?login=" + nouveauUser.getMail());
            Map<String,String> imagesAAjouter = new HashMap<>();
            imagesAAjouter.put("logoMBALInteractif","./static/mbalopening.gif");
            emailService.sendMail(nouveauUser.getMail(),"Yateli : Activer mon compte",variablesMail,imagesAAjouter,null,"mailActiverCompte.ftl");

            String logCreatedBis = "Mail d'activation de compte envoyé à " + nouveauUser.getMail();
            log.info(logCreatedBis);
            evenementService.enregistrerEvenement(nouveauUser, logCreatedBis);
        });
        envoiMail.start();

        return nouveauUser;

    }

    @Override
    public User modifierUser(Principal moi, UserModificationDto userDto) throws UserNonTrouveException, UserBloqueException {

        User userModifie = userRepository.findUserByMail(moi.getName());

        if(userModifie == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userModifie.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userModifie.getMail());
        }

        if(userModifie.getRole() == roleRepository.findRoleByNom("ADMIN")){
            userModifie = userRepository.findUserByMail(userDto.getMail());

            if(userModifie == null){
                throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,userDto.getMail());
            }
        }

        if(userDto.getPassword() != null && !userDto.getPassword().isEmpty()){
            userModifie.setPassword(passwordEncoder.encode(userDto.getPassword()));
        }

        if(userDto.getPrenom() != null){
            userModifie.setPrenom(userDto.getPrenom());
        }

        if(userDto.getTelephone() != null){
            userModifie.setTelephone(userDto.getTelephone());
        }

        if(userDto.getListeEnvies() != null){
            Set<Tag> tagsASupprimer = userModifie.getListeEnvies();

            for(Tag tag : new HashSet<>(tagsASupprimer)){
                userModifie.supprimerEnvie(tag);
            }

            for(TagDto tagAajouterNom : userDto.getListeEnvies()){
                Tag tagAajouter = tagRepository.findTagByNom(tagAajouterNom.getNom());
                if(tagAajouter != null){
                    userModifie.ajoutEnvie(tagAajouter);
                }
            }
        }

        userRepository.save(userModifie);

        String logCreated = "User " + userModifie.getMail() + " modifié à " + new Date().getTime();
        log.info(logCreated);
        evenementService.enregistrerEvenement(userModifie, logCreated);

        return userModifie;

    }

    @Override
    public User activerUser(String login, String password) throws UserNonTrouveException, MotDePasseIncorrectException, UserDejaActiveException {

        User userConnecte = userRepository.findUserByMail(login);

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,login);
        }

        if(userConnecte.isEstActive()){
            throw UserDejaActiveException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        if(!passwordEncoder.matches(password,userConnecte.getPassword())){
            throw MotDePasseIncorrectException.creerAvec(HttpStatus.PRECONDITION_FAILED);
        }

        userConnecte.setEstActive(true);

        userRepository.save(userConnecte);

        String logCreated = "User " + userConnecte.getMail() + " est activé, il peut désormais se connecter";
        log.info(logCreated);
        evenementService.enregistrerEvenement(userConnecte, logCreated);

        Thread userActiveMail = new Thread(() -> {
            Map<String,Object> variablesMail = new HashMap<>();
            variablesMail.put("prenom", userConnecte.getPrenom());
            variablesMail.put("login", userConnecte.getMail());
            variablesMail.put("allerSurSite", Client.host);
            Map<String,String> imagesAAjouter = new HashMap<>();
            imagesAAjouter.put("compteValide","./static/checksuccess.gif");
            emailService.sendMail(userConnecte.getMail(),"Yateli : Votre compte est activé",variablesMail,imagesAAjouter,null,"mailCompteActive.ftl");

            String logCreatedBis = "Mail d'information de compte activé envoyé à " + userConnecte.getMail();
            log.info(logCreatedBis);
            evenementService.enregistrerEvenement(userConnecte, logCreatedBis);
        });
        userActiveMail.start();

        return userConnecte;

    }

    @Override
    public String desactiverUser(String login) throws UserNonTrouveException {

        User userConnecte = userRepository.findUserByMail(login);

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,login);
        }

        userConnecte.setEstActive(false);

        userRepository.save(userConnecte);

        alerteService.creerNotification(userConnecte, SocketEventType.COMPTE_DESACTIVE, "Votre compte a été désactivé");

        return "OK";

    }

    @Override
    public String reactiverUser(String login) throws UserNonTrouveException {

        User userConnecte = userRepository.findUserByMail(login);

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,login);
        }

        userConnecte.setEstActive(true);

        userRepository.save(userConnecte);

        String logCreated = "Compte utilisateur " + userConnecte.getMail() + " réactivé";
        log.info(logCreated);
        evenementService.enregistrerEvenement(userConnecte, logCreated);

        return "OK";

    }

    @Override
    public String motDePasseOublie(String login) throws UserNonTrouveException, UserBloqueException {

        User userConnecte = userRepository.findUserByMail(login);

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,login);
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        String code = RandomStringUtils.random(5,false,true);
        if(userConnecte.getTelephone() != null && !userConnecte.getTelephone().isEmpty()){
            telephoneService.envoyerSms(userConnecte.getTelephone(),"YATELI : Votre code de récupération de compte est le : " + code);
        }else{
            Map<String,Object> variablesMail = new HashMap<>();
            variablesMail.put("login", userConnecte.getMail());
            variablesMail.put("allerSurSite", Client.host);
            variablesMail.put("code", code);
            Map<String,String> imagesAAjouter = new HashMap<>();
            imagesAAjouter.put("forget","./static/willsmithforget.gif");

            String logCreated = "Mail de mot de passe oublié envoyé à " + userConnecte.getMail() + " avec le code de récupération " + code;
            log.info(logCreated);
            evenementService.enregistrerEvenement(userConnecte, logCreated);
        }

        userConnecte.setCodeRecuperation(code);
        userRepository.save(userConnecte);

        return "OK";

    }

    @Override
    public String verificationCodeRecuperation(String login, String code) throws UserNonTrouveException, CodeVerificationIncorrectException, UserBloqueException {

        User userConnecte = userRepository.findUserByMail(login);

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,login);
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        if(!userConnecte.getCodeRecuperation().equals(code)){
            throw CodeVerificationIncorrectException.creerAvec(HttpStatus.BAD_REQUEST,code);
        }

        return "OK";

    }

    @Override
    public String nouveauPasswordAvecCode(String code, String nouveauPassword) throws UserNonTrouveException, UserBloqueException {

        User userConnecte = userRepository.findUserByCodeRecuperation(code);

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,"");
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        userConnecte.setPassword(passwordEncoder.encode(nouveauPassword));
        userConnecte.setCodeRecuperation(null);
        userRepository.save(userConnecte);

        return "OK";

    }

    @Override
    public String supprimerUser(Principal moi) throws UserNonTrouveException, UserBloqueException {

        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        List<BonAchat> bonAchats = bonAchatRepository.findAllByUser(userConnecte);
        if(bonAchats != null){ bonAchatRepository.delete(bonAchats); }

        List<ColisMystere> colisMystereList = userConnecte.getColisMystereListe();
        if(colisMystereList != null){ colisMystereService.supprimerTousColisMystere(colisMystereList); }

        factureService.supprimerFacturesCommandesUser(userConnecte);

        artisanService.supprimerArtisanSelonUser(userConnecte);

        roleService.delierRoleUser(userConnecte);

        commandeService.supprimerToutesCommandesUser(userConnecte);

        userRepository.delete(userConnecte);

        String logCreated = "Le User " + moi.getName() + " a été supprimé du site Yateli";
        log.info(logCreated);
        evenementService.enregistrerEvenement(userConnecte, logCreated);

        return "OK";

    }

    @Override
    public String supprimerSpecificUser(Principal moi, String loginUser) throws UserNonTrouveException {

        User admin = userRepository.findUserByMail(moi.getName());

        if(admin == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        User userASupprimer = userRepository.findUserByMail(loginUser);

        if(userASupprimer == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,loginUser);
        }

        List<BonAchat> bonAchats = bonAchatRepository.findAllByUser(userASupprimer);
        if(bonAchats != null){ bonAchatRepository.delete(bonAchats); }

        List<ColisMystere> colisMystereList = userASupprimer.getColisMystereListe();
        if(colisMystereList != null){ colisMystereService.supprimerTousColisMystere(colisMystereList); }

        factureService.supprimerFacturesCommandesUser(userASupprimer);

        artisanService.supprimerArtisanSelonUser(userASupprimer);

        roleService.delierRoleUser(userASupprimer);

        commandeService.supprimerToutesCommandesUser(userASupprimer);

        userRepository.delete(userASupprimer);

        String logCreated = "Le User " + loginUser + " a été supprimé du site Yateli par l'admin " + moi.getName();
        log.info(logCreated);
        evenementService.enregistrerEvenement(userASupprimer, logCreated);

        return "OK";
    }

    @Override
    public UserView getUser(Principal moi) throws UserNonTrouveException, UserBloqueException {

        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        UserView userView = UserView.builder()
                .id(userConnecte.getId())
                .prenom(userConnecte.getPrenom())
                .mail(userConnecte.getMail())
                .telephone(userConnecte.getTelephone())
                .panier(userConnecte.getPanier())
                .dateCreation(userConnecte.getDateCreation())
                .pointsFidelite(userConnecte.getPointsFidelite())
                .colisMystereListe(userConnecte.getColisMystereListe())
                .listeEnvies(userConnecte.getListeEnvies())
                .estAbonne(userConnecte.getEstAbonne())
                .role(userConnecte.getRole().getNom())
                .build();

        List<BonAchat> listeBonAchatsEncoreActif = new ArrayList<>();
        for(BonAchat bonAchat : userConnecte.getBonAchats()){
            if(!bonAchat.isDejaUtilise()){
                listeBonAchatsEncoreActif.add(bonAchat);
            }
        }
        userView.setBonAchats(listeBonAchatsEncoreActif);

        return userView;

    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<User> getUsersSelonRole(String role) throws RoleExistePasException {

        Role entiteRole = roleRepository.findRoleByNom(role.toUpperCase());

        if(entiteRole == null){
            throw RoleExistePasException.creerAvec(HttpStatus.NOT_FOUND,role);
        }

        return userRepository.findUsersByRole(entiteRole);

    }

    @Override
    public User abonnerUser(Principal moi) throws UserNonTrouveException, EstDejaAbonneException, UserBloqueException {

        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        if(userConnecte.getEstAbonne()){
            throw EstDejaAbonneException.creerAvec(HttpStatus.PRECONDITION_FAILED);
        }

        userConnecte.setEstAbonne(true);

        userRepository.save(userConnecte);

        //Création colis mystere pour User
        colisMystereService.creerColisMystere(moi);

        String logCreated = "User " + userConnecte.getMail() + " est désormais abonné au colis mystère";
        log.info(logCreated);
        evenementService.enregistrerEvenement(userConnecte, logCreated);

        return userConnecte;

    }

    @Override
    public User desabonnerUser(Principal moi) throws UserNonTrouveException, UserBloqueException {

        User userConnecte = userRepository.findUserByMail(moi.getName());

        if(userConnecte == null){
            throw UserNonTrouveException.creerAvec(HttpStatus.NOT_FOUND,moi.getName());
        }

        if(!userConnecte.isEstActive()){
            throw UserBloqueException.creerAvec(HttpStatus.PRECONDITION_FAILED, userConnecte.getMail());
        }

        List<ColisMystere> colisMystereASupprimer = userConnecte.viderTousColisMystere();

        userConnecte.setEstAbonne(false);

        userRepository.save(userConnecte);

        colisMystereService.supprimerTousColisMystere(colisMystereASupprimer);

        return userConnecte;

    }

    @Scheduled(cron = "0 0 0 * * ?")
    private void supprimerUserNonActive(){
        LocalDate maintenant = LocalDate.now();
        LocalDate troisJoursAvant = maintenant.minusDays(3).atTime(0,0).toLocalDate();
        Date troisJoursAvantDate = Date.from(troisJoursAvant.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
        LocalDate quatreJoursAvant = maintenant.minusDays(4).atTime(0,0).toLocalDate();
        Date quatreJoursAvantDate = Date.from(quatreJoursAvant.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());

        List<User> usersInactif = userRepository.findAllByDateCreationBeforeAndEstActiveIsFalseAndRoleIs(new Timestamp(troisJoursAvantDate.getTime()),roleRepository.findRoleByNom("USER"));

       /*-----------------------------Test------------------------------------*/
       /* Instant maintenant = Instant.now();
        Instant test30SecondesAvant = maintenant.minus(30000, ChronoUnit.MILLIS);
        Date test30SecondesAvantDate = Date.from(test30SecondesAvant.atZone(ZoneId.systemDefault()).toInstant());
        Instant uneMinuteAvant = maintenant.minus(60000,ChronoUnit.MILLIS);
        Date uneMinuteAvantDate = Date.from(uneMinuteAvant.atZone(ZoneId.systemDefault()).toInstant());

        List<User> usersInactif = userRepository.findAllByDateCreationBeforeAndEstActiveIsFalseAndRoleIs(new Timestamp(test30SecondesAvantDate.getTime()),roleRepository.findRoleByNom("USER"));*/
        /*---------------------------------------------------------------------*/

        for(User userInactif : usersInactif){
            if(userInactif.getDateCreation().getTime() >= quatreJoursAvantDate.getTime()){
                if(userInactif.getTelephone() != null && !userInactif.getTelephone().isEmpty()){
                    telephoneService.envoyerSms(userInactif.getTelephone(),"YATELI : Activez votre compte avant demain via le mail envoyé lors de l'inscription");
                }else{
                    Map<String,Object> variablesMail = new HashMap<>();
                    Date date = new Date(userInactif.getDateCreation().getTime());
                    variablesMail.put("dateInscription", new SimpleDateFormat("dd MMMMM yyyy").format(date));
                    variablesMail.put("heureInscription", new SimpleDateFormat("HH:mm").format(date));
                    variablesMail.put("prenom", userInactif.getPrenom());
                    variablesMail.put("login", userInactif.getMail());
                    variablesMail.put("activationSite", Client.host + "/activer-compte");
                    Map<String,String> imagesAAjouter = new HashMap<>();
                    imagesAAjouter.put("warning","./static/caterror.gif");
                    emailService.sendMail(userInactif.getMail(),"Yateli : Attention compte non validé",variablesMail,imagesAAjouter,null,"mailCompteInactif.ftl");

                    String logCreated = "Mail d'avertissement de compte inactif envoyé à " + userInactif.getMail();
                    log.info(logCreated);
                    evenementService.enregistrerEvenement(userInactif, logCreated);
                }
            }else{
                List<BonAchat> bonAchats = bonAchatRepository.findAllByUser(userInactif);
                if(bonAchats != null){ bonAchatRepository.delete(bonAchats); }

                List<ColisMystere> colisMystereList = userInactif.getColisMystereListe();
                if(colisMystereList != null){ colisMystereService.supprimerTousColisMystere(colisMystereList); }

                roleService.delierRoleUser(userInactif);

                userRepository.delete(userInactif);
                String logCreated = "User " + userInactif.getMail() + " supprimé car le compte n'a pas été activé depuis le " + userInactif.getDateCreation().getTime();
                log.info(logCreated);
                evenementService.enregistrerEvenement(userInactif, logCreated);
            }
        }

    }

    private static boolean mailEstCorrect(String mail){
        EmailValidator validator = EmailValidator.getInstance();
        return validator.isValid(mail);
    }

    private static boolean motDePasseEstCorrect(String password){
        return !password.isEmpty() && password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$");
    }

}
