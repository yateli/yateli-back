package com.cpe.yateli.service.mail;

import com.cpe.yateli.model.Mail;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.util.ResourceUtils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Service
public class EmailService {

    @Qualifier("freeMarkerConfiguration")
    @Autowired
    private Configuration freemarkerConfig;

    @Autowired
    private ServletContext servletContext;


    private void sendSimpleMessage(Mail mail, String template) throws MessagingException, IOException, TemplateException {
        final String username = "yateli.ecoculture@gmail.com";
        final String password = "yateliCPE";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "465");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                }
        );

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress(mail.getFrom()));
        InternetAddress[] toAddresses = { new InternetAddress(mail.getTo()) };
        message.setRecipients(Message.RecipientType.TO, toAddresses);
        message.setSubject(mail.getSubject(),"UTF-8");
        message.setSentDate(new Date());

        MimeMultipart mimeMultipart = new MimeMultipart("related");

        BodyPart messageBodyPart = new MimeBodyPart();
        Template t = freemarkerConfig.getTemplate(template);
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mail.getModel());
        messageBodyPart.setContent(html,"text/html; charset=\"UTF-8\"");

        mimeMultipart.addBodyPart(messageBodyPart);

        if (mail.getImagesAAjouer() != null && mail.getImagesAAjouer().size() > 0) {
            Set<String> setImageID = mail.getImagesAAjouer().keySet();
            for (String contentId : setImageID) {
                MimeBodyPart imagePart = new MimeBodyPart();
                imagePart.setHeader("Content-ID", "<" + contentId + ">");
                File fichierImage = new ClassPathResource(mail.getImagesAAjouer().get(contentId)).getFile();
                DataSource fds = new FileDataSource(fichierImage);
                imagePart.setDataHandler(new DataHandler(fds));
                mimeMultipart.addBodyPart(imagePart);
            }
        }

        //Ajout pièces jointes
        if(mail.getPiecesJointes() != null && !mail.getPiecesJointes().isEmpty()){
            MimeBodyPart mimeBodyPart = new MimeBodyPart();
            for(File piece : mail.getPiecesJointes()){
                mimeBodyPart.attachFile(piece);
            }
            mimeMultipart.addBodyPart(mimeBodyPart);
        }

        message.setContent(mimeMultipart);

        Transport.send(message);

    }

    public void sendMail(String to, String subject, Map<String,Object> variables, Map<String,String> imagesAAjouter, List<File> piecesJointes, String template){
        try {
            Mail mail = new Mail();
            mail.setFrom("Yateli");
            mail.setTo(to);
            mail.setSubject(subject);
            mail.setModel(variables);
            mail.setImagesAAjouer(imagesAAjouter);
            mail.setPiecesJointes(piecesJointes);

            this.sendSimpleMessage(mail, template);
        } catch (TemplateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}
