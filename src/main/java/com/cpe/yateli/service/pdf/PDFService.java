package com.cpe.yateli.service.pdf;

import com.cpe.yateli.model.Facture;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class PDFService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TemplateEngine templateEngine;

    public MultipartFile creerPdfFacture(Facture facture) throws Exception {
        String date = new SimpleDateFormat("dd MMMMM yyyy").format(facture.getDate_creation());
        String dateFactureTitre = new SimpleDateFormat("ddMMyyyy").format(facture.getDate_creation());

        Context ctx = new Context();
        ctx.setVariable("facture", facture);
        ctx.setVariable("codePostalVille", facture.getCommande().getAdresseLivraison().getCodePostal() + ", " + facture.getCommande().getAdresseLivraison().getVille());
        ctx.setVariable("date", date);

        String processedHtml = templateEngine.process("facture", ctx);
        FileOutputStream os = null;
        String fileName = "Facture" + dateFactureTitre;
        MultipartFile multipartFile = null;
        try {
            final File outputFile = File.createTempFile(fileName, ".pdf");
            fileName = outputFile.getName();
            os = new FileOutputStream(outputFile);

            ITextRenderer renderer = new ITextRenderer();
            renderer.setDocumentFromString(processedHtml);
            renderer.layout();
            renderer.createPDF(os, false);
            renderer.finishPDF();

            multipartFile = new MockMultipartFile("file",outputFile.getName(), "application/pdf", IOUtils.toByteArray(new FileInputStream(outputFile)));
            log.debug("PDF " + fileName + " cree avec succes");
        } finally {
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) { /*ignore*/ }
            }
        }
        return multipartFile;
    }

}
