package com.cpe.yateli.service.telephone;

import com.cpe.yateli.exception.user.PhoneFormatIncorrectException;
import com.cpe.yateli.model.User;
import com.cpe.yateli.service.EvenementService;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.ValidationRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class TelephoneService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    //Adresse CPE
    //Password Twilio : yatelibackirc69

    private static final String ACCOUNT_SID = "AC5c3eb0586599c8050e8980eeb9807186";
    private static final String AUTH_TOKEN = "0d2c14cc425999b375b33bf5768b6a49";
    private static final String PHONE_NUMBER_TWILIO = "+33756796160";

    @Autowired
    private EvenementService evenementService;

    public TelephoneService(){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    }

    public void ajouterNumeroAListeReception(String login, String telephone) throws PhoneFormatIncorrectException {
        if(!telephone.startsWith("+")){
            throw PhoneFormatIncorrectException.creerAvec(HttpStatus.PRECONDITION_FAILED, telephone);
        }

        ValidationRequest validationRequest = ValidationRequest.creator(
                new com.twilio.type.PhoneNumber(telephone))
                .setFriendlyName(login)
                .create();

        String logCreated = "Numéro " + telephone + " ajouté pour " + validationRequest.getFriendlyName() + " avec code de validation " + validationRequest.getValidationCode() + " à la liste de confiance Twilio";
        log.info(logCreated);
        evenementService.enregistrerEvenement(null, logCreated);
    }

    public void envoyerSms(String telephone, String message){
        Message.creator(
                new com.twilio.type.PhoneNumber(telephone),
                new com.twilio.type.PhoneNumber(PHONE_NUMBER_TWILIO),
                message)
                .create();

        String logCreated = "Sms envoyé avec message(" + message + ") à " + telephone;
        log.info(logCreated);
        evenementService.enregistrerEvenement(null, logCreated);
    }

}
