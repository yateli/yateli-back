package com.cpe.yateli.service.usersConnectes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@Data
@AllArgsConstructor
@Builder
@RedisHash("UserConnecte")
public class UserConnecte {

    @Id
    private String login;

    private String role;

    private long timestampConnexion;

    @JsonIgnore
    private String tokenWebsocket;

    private boolean webSocketActif;

}
