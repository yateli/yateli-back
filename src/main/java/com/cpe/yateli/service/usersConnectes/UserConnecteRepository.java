package com.cpe.yateli.service.usersConnectes;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserConnecteRepository extends CrudRepository<UserConnecte,String> {}
