package com.cpe.yateli.service.usersConnectes;

import com.cpe.yateli.model.User;
import com.cpe.yateli.repository.UserRepository;
import com.cpe.yateli.service.EvenementService;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.security.Principal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class UserConnecteService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserConnecteRepository userConnecteRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EvenementService evenementService;

    public boolean userEstConnecte(String login){
        List<UserConnecte> userConnectes = getUsersConnectes();
        List<String> loginsConnecte = userConnectes.stream()
                .map(UserConnecte::getLogin).collect(Collectors.toList());
        return loginsConnecte.contains(login);
    }

    public String genererTokenWS(User user){

        String tokenGenere = RandomStringUtils.random(15,true,true);

        UserConnecte userConnecte = UserConnecte.builder()
                .login(user.getMail())
                .role(user.getRole().getNom())
                .tokenWebsocket(tokenGenere)
                .timestampConnexion(new Date().getTime())
                .build();

        userConnecteRepository.save(userConnecte);

        return tokenGenere;

    }

    public boolean estConnecteSelonLoginEtTokenWS(String login, String token){

        UserConnecte userConnecte = userConnecteRepository.findOne(login);

        return userConnecte != null && userConnecte.getTokenWebsocket() != null && userConnecte.getTokenWebsocket().equals(token);

    }

    public void supprimerUserConnecte(String login){

        UserConnecte userConnecte = userConnecteRepository.findOne(login);

        if(userConnecte != null){
            userConnecteRepository.delete(userConnecte);
        }

    }

    public List<UserConnecte> getUsersConnectes(){
        return Lists.newArrayList(userConnecteRepository.findAll()).stream()
                .filter(UserConnecte::isWebSocketActif)
                .collect(Collectors.toList());
    }

    public List<UserConnecte> getUsersNonConnectes(){
        return Lists.newArrayList(userConnecteRepository.findAll()).stream()
                .filter(p -> !p.isWebSocketActif())
                .collect(Collectors.toList());
    }

    @Scheduled(cron = "* * */4 * * ?")
    private void viderClesExpirees(){

        Instant maintenant = Instant.now();
        Instant uneHeureAvant = maintenant.minus(30, ChronoUnit.SECONDS);
        Date uneHeureAvantDate = Date.from(uneHeureAvant.atZone(ZoneId.systemDefault()).toInstant());

        List<UserConnecte> userConnectes = getUsersNonConnectes();

        for(UserConnecte userConnecte : userConnectes){
            if(userConnecte.getTimestampConnexion() <= uneHeureAvantDate.getTime()){
                userConnecteRepository.delete(userConnecte);
                String logCreated = "User déconnecté après expiration de sa session WebSocket : " + userConnecte.getLogin();
                log.info(logCreated);
                User user = userRepository.findUserByMail(userConnecte.getLogin());
                evenementService.enregistrerEvenement(user, logCreated);
            }
        }

    }


}
