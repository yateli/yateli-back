package com.cpe.yateli.service.websocket;

import com.cpe.yateli.configuration.websocket.SocketData;
import com.cpe.yateli.model.Alerte;
import com.cpe.yateli.model.SocketEventType;
import com.cpe.yateli.service.usersConnectes.UserConnecte;
import com.cpe.yateli.service.usersConnectes.UserConnecteService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WebSocketService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private SimpMessagingTemplate socket;

    @Autowired
    private UserConnecteService userConnecteService;

    public void envoyerUserSpecifique(String login, Alerte alerte){
        if(userConnecteService.userEstConnecte(login)){
            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            SocketData socketData = new SocketData(alerte);
            socket.convertAndSendToUser(login,"/event", gson.toJson(socketData));
        }
    }

    public void envoyerListeUserSpecifique(List<String> logins, Alerte alerte){
        for(String login : logins){
            envoyerUserSpecifique(login,alerte);
        }
    }

    public void envoyerRoleSpecifique(String role, Alerte alerte){
        List<UserConnecte> userConnectes = userConnecteService.getUsersConnectes();
        List<String> logins = userConnectes.stream().filter(p -> p.getRole().equals(role)).map(UserConnecte::getLogin).collect(Collectors.toList());
        envoyerListeUserSpecifique(logins,alerte);
    }

    /**
     * Alerte directe sans enregistrement en BDD de notification (Seulement Users connectés)
     * @param socketEventType : Type de message
     * @param message : Objet à envoyer dans la websocket
     */
    public void envoyerTouslesUsers(SocketEventType socketEventType, Object message){
        List<UserConnecte> userConnectes = userConnecteService.getUsersConnectes();
        List<String> logins = userConnectes.stream().map(UserConnecte::getLogin).collect(Collectors.toList());
        Alerte alerte = new Alerte();
        alerte.setType(socketEventType);
        alerte.setMessage(message);
        envoyerListeUserSpecifique(logins, alerte);
    }

}
